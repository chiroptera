module chievents /*is aliced*/;

import iv.alice;
import iv.egra;


// ////////////////////////////////////////////////////////////////////////// //
public __gshared int guiGroupListWidth = 140*2;
public __gshared int guiThreadListHeight = 250*2-100 + 80;
public __gshared int guiMessageTextLPad = 6;
public __gshared int guiMessageTextVPad = 4;
public __gshared int guiMessageTextInterline = 4;
public __gshared int guiScrollbarWidth = 8;


// ////////////////////////////////////////////////////////////////////////// //
public class UpdatingAccountEvent {
  uint accid;

  this (uint aid) nothrow @safe @nogc { accid = aid; }
}

public class UpdatingAccountCompleteEvent {
  uint accid;

  this (uint aid) nothrow @safe @nogc { accid = aid; }
}

public class UpdatingCompleteEvent {}

public class RecalcAllTwitsEvent {}

public class TagThreadsUpdatedEvent {
  uint tagid;

  this (uint tgid) nothrow @safe @nogc { tagid = tgid; }
}
