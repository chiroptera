/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module dialogs /*is aliced*/;
private:

import core.time;

import arsd.simpledisplay;

import iv.egra;

import iv.alice;
import iv.cmdcon;
import iv.strex;
import iv.sq3;
import iv.utfutil;
import iv.vfs;

import chibackend /*: DynStr*/;
import receiver /*: DynStr*/;


// ////////////////////////////////////////////////////////////////////////// //
public final class HintWindow : SubWindow {
  private DynStr mMessage;

  this (const(char)[] amessage) {
    mMessage = amessage;

    int xwdt = gxTextWidthUtf(amessage)+6;
    int xhgt = gxTextHeightUtf+4;
    super(null, GxSize(xwdt, xhgt));
    x0 = screenWidth-width;
    y0 = 0;
    mType = Type.OnTop;
    add();
  }

  override @property int decorationSizeX () const nothrow @safe @nogc { return 3*2; }
  override @property int decorationSizeY () const nothrow @safe @nogc { return 2*2; }

  override @property int clientOffsetX () const nothrow @safe @nogc { return decorationSizeX/2; }
  override @property int clientOffsetY () const nothrow @safe @nogc { return decorationSizeY/2; }

  @property void message (DynStr v) {
    if (mMessage != v) {
      mMessage = v;
      if (x0 == screenWidth-width) {
        width = gxTextWidthUtf(v)+6;
        x0 = screenWidth-width;
      } else {
        width = gxTextWidthUtf(v)+6;
        if (x0+width > screenWidth) x0 = screenWidth-width;
      }
      postScreenRebuild();
    }
  }

  override void onPaint () {
    if (closed) return;
    {
      immutable bool oldNoTitle = mNoTitle;
      scope(exit) mNoTitle = oldNoTitle;
      if (!minimised) mNoTitle = true;
      super.onPaint();
    }
    if (!minimised && mMessage.length) {
      gxWithSavedClip {
        setupClip();
        gxClipRect.shrinkBy(3, 2);
        gxDrawTextUtf(x0+3, y0+2, mMessage, getColor("text"));
      };
    }
  }

  // prevent any default keyboard handling
  override bool onKeySink (KeyEvent event) {
    return true;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public final class MessageWindow : SubWindow {
  private DynStr mMessage;
  private uint lastBarWidth;

  this (const(char)[] amessage) {
    mMessage = amessage;

    int xwdt = gxTextWidthUtf(amessage)+decorationSizeX;
    int xhgt = gxTextHeightUtf+decorationSizeY;
    super(null, GxSize(xwdt, xhgt));
    centerWindow();
    mType = Type.OnTop;
    add();
  }

  override @property int decorationSizeX () const nothrow @safe @nogc { return 24; }
  override @property int decorationSizeY () const nothrow @safe @nogc { return 16; }

  override @property int clientOffsetX () const nothrow @safe @nogc { return decorationSizeX/2; }
  override @property int clientOffsetY () const nothrow @safe @nogc { return decorationSizeY/2; }

  // returns `true` if need redraw
  bool setProgress (const(char)[] v, uint curr, uint total) {
    bool res = false;
    // fix message
    if (mMessage != v) {
      mMessage = v;
      int xwdt = gxTextWidthUtf(v)+decorationSizeX;
      int xhgt = gxTextHeightUtf+decorationSizeY;
      if (xwdt > width || xhgt > height) {
        setSize(xwdt, xhgt);
        centerWindow();
        res = true;
      }
    }
    // bix bar width
    uint barWidth = 0;
    if (total) barWidth = curr*cast(uint)(width-2)/total;
    if (barWidth != lastBarWidth) res = true;
    lastBarWidth = barWidth;
    return res;
  }

  override void onPaint () {
    if (closed) return;
    {
      immutable bool oldNoTitle = mNoTitle;
      scope(exit) mNoTitle = oldNoTitle;
      if (!minimised) mNoTitle = true;
      super.onPaint();
    }
    if (!minimised && mMessage.length) {
      gxWithSavedClip {
        setupClip();
        if (lastBarWidth > 0) {
          auto rc = GxRect(x0+1, y0+1, lastBarWidth, height-2);
          gxFillRect(rc, getColor("bar-back"));
        }
        setupClientClip();
        //gxClipRect.shrinkBy(3, 2);
        immutable tw = gxTextWidthUtf(mMessage);
        immutable th = gxTextHeightUtf();
        gxDrawTextUtf(x0+(width-tw)/2, y0+(height-th)/2, mMessage, getColor("text"));
      };
    }
  }

  // prevent any default keyboard handling
  override bool onKeySink (KeyEvent event) {
    return true;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public final class ProgressWindow : SubWindow {
  ProgressBarWidget pbar;
  private enum PadX = 24;
  private enum PadY = 8;

  this (const(char)[] amessage) {
    int xwdt = gxTextWidthUtf(amessage)+PadX*2;
    int xhgt = gxTextHeightUtf+PadY*2;
    super(null, GxSize(xwdt, xhgt));
    centerWindow();

    pbar = new ProgressBarWidget(rootWidget, amessage);
    pbar.size = rootWidget.size;

    mType = Type.OnTop;
    add();
  }

  override @property int decorationSizeX () const nothrow @safe @nogc { return 0; }
  override @property int decorationSizeY () const nothrow @safe @nogc { return 0; }

  override @property int clientOffsetX () const nothrow @safe @nogc { return 0; }
  override @property int clientOffsetY () const nothrow @safe @nogc { return 0; }

  // returns `true` if need redraw
  bool setProgress (const(char)[] v, uint curr, uint total) {
    bool res = false;
    // fix message
    if (pbar.text != v) {
      pbar.text = v;
      int xwdt = gxTextWidthUtf(v)+PadX*2;
      int xhgt = gxTextHeightUtf+PadY*2;
      if (xwdt > width || xhgt > height) {
        if (xwdt < width) xwdt = width;
        if (xhgt < height) xhgt = height;
        setSize(xwdt, xhgt);
        rootWidget.size = GxSize(xwdt, xhgt);
        pbar.size = rootWidget.size;
        centerWindow();
        res = true;
      }
    }
    if (pbar.setCurrentTotal(cast(int)curr, cast(int)total)) res = true;
    return res;
  }

  // prevent any default keyboard handling
  override bool onKeySink (KeyEvent event) {
    return true;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public class SelectPopBoxWindow : SubWindow {
public:
  void delegate (uint accid) onSelected;

public:
  this (dynstring defacc) {
    createRoot();

    auto lb = new SimpleListBoxUDataWidget!uint(rootWidget);
    lb.id = "listbox";

    static auto stat = LazyStatement!"Conf"(`
      SELECT accid AS accid, name AS name, realname AS realname, email AS email
      FROM accounts
      WHERE sendserver<>'' AND email<>'' and nntpgroup=''
      ORDER BY accid
    ;`);

    int xhgt = 0;
    int xwdt = 0;
    bool accFound = false;
    //int defAccIdx = -1;

    foreach (auto row; stat.st.range) {
      dynstring s = row.realname!SQ3Text;
      s ~= " <";
      s ~= row.email!SQ3Text;
      s ~= ">";
      //conwriteln(s);
      lb.appendItemWithData(s, row.accid!uint);
      int w = gxTextWidthUtf(s)+2;
      if (xwdt < w) xwdt = w;
      if (defacc == row.name!SQ3Text) {
        lb.curidx = lb.length-1;
        accFound = true;
      }
      //if (acc is defaultAcc) defAccIdx = lb.length-1;
      xhgt += gxTextHeightUtf;
    }

    if (lb.length > 0) {
      if (xhgt == 0) { super(); return; }
      if (xhgt > screenHeight) xhgt = screenHeight-decorationSizeY;

      //if (!accFound && defAccIdx >= 0) lb.curidx = defAccIdx;
      if (xwdt > screenWidth-decorationSizeX) xwdt = screenWidth-decorationSizeX;

      super("Select Account", GxSize(xwdt+decorationSizeX, xhgt+decorationSizeY));
      lb.width = clientWidth;
      lb.height = clientHeight;

      lb.onAction = delegate (self) {
        if (auto acc = lb.itemData(lb.curidx)) {
          close();
          if (onSelected !is null) onSelected(acc); else vbwin.beep();
        } else {
          vbwin.beep();
        }
      };

      addModal();
    } else {
      super();
      close();
    }
  }

  override bool onKeyBubble (KeyEvent event) {
    if (event.pressed) {
      if (event == "Escape") { close(); return true; }
      if (event == "Enter") {
        if (auto lb = querySelector!Widget("#listbox")) {
          lb.doAction();
          return true;
        }
      }
    }
    return super.onKeyBubble(event);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public class SelectAddressBookWindow : SubWindow {
  static struct Entry {
    dynstring nick;
    dynstring name;
    dynstring email;
  }

  void delegate (dynstring nick, dynstring name, dynstring email) onSelected;

  SimpleListBoxUDataWidget!Entry lb;

  this (const(char)[] prefix) {
    static auto stat = LazyStatement!"Conf"(`
      SELECT nick AS nick, name AS name, email AS email
      FROM addressbook
      WHERE nick<>'' AND email<>''
      ORDER BY nick
    ;`);

    createRoot();

    lb = new SimpleListBoxUDataWidget!Entry(rootWidget);
    lb.id = "listbox";

    int xhgt = 0;
    int xwdt = 0;

    foreach (auto row; stat.st.range) {
      //conwriteln("! <", row.nick!SQ3Text, "> : <", row.name!SQ3Text, "> : <", row.email!SQ3Text, ">");
      if (prefix.length) {
        if (!startsWithCI(row.nick!SQ3Text, prefix)) continue;
      }
      dynstring it;
      if (row.name!SQ3Text.length) {
        it ~= row.name!SQ3Text;
        it ~= " <";
        it ~= row.email!SQ3Text;
        it ~= ">";
      } else {
        it = row.email!SQ3Text;
      }
      Entry e;
      e.nick = row.nick!SQ3Text;
      e.name = row.name!SQ3Text;
      e.email = row.email!SQ3Text;
      lb.appendItemWithData(it, e);

      int w = gxTextWidthUtf(it)+2;
      if (xwdt < w) xwdt = w;
      //if (ae is defae) lb.curidx = lb.length-1;
      xhgt += gxTextHeightUtf;
    }

    if (xhgt == 0) { super(); return; }
    if (xhgt > screenHeight) xhgt = screenHeight-decorationSizeY;

    if (xwdt > screenWidth-decorationSizeX) xwdt = screenWidth-decorationSizeX;

    super("Select Recepient", GxSize(xwdt+decorationSizeX, xhgt+decorationSizeY));
    lb.width = clientWidth;
    lb.height = clientHeight;

    lb.onAction = delegate (self) {
      Entry ae = lb.itemData(lb.curidx);
      if (ae.email.length) {
        close();
        if (onSelected !is null) onSelected(ae.nick, ae.name, ae.email); else vbwin.beep();
      } else {
       vbwin.beep();
      }
    };

    addModal();
  }

  override bool onKeyBubble (KeyEvent event) {
    if (event.pressed) {
      if (event == "Escape") { close(); return true; }
      if (event == "Enter") { lb.onAction(lb); return true; }
    }
    return super.onKeyBubble(event);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public class PostWindow : SubWindow {
  LineEditWidget from;
  LineEditWidget to;
  LineEditWidget subj;
  EditorWidget ed;
  //dynstring replyto;
  dynstring references; // of replyto article
  dynstring accname;
  dynstring desttag;
  bool allowAccountChange = true;
  bool nntp = false;

  void setFrom (const(char)[] s) {
    from.readonly = false;
    from.str = s;
    from.readonly = true;
  }

  void setFrom (const(char)[] name, const(char)[] email) {
    dynstring s;
    if (name.length) {
      s = name;
      s ~= " <";
      s ~= email;
      s ~= ">";
    } else {
      s = email;
    }
    setFrom(s.getData);
  }

  bool trySend () {
    static bool checkString (const(char)[] s) nothrow @trusted @nogc {
      if (s.length == 0) return false;
      if (s.utflen > 255) return false;
      return true;
    }

    if (!checkString(from.str)) { from.focus(); return false; }
    if (!nntp && !checkString(to.str)) { to.focus(); return false; }
    if (!checkString(subj.str)) { subj.focus(); return false; }
    if (ed.editor[].length == 0) { ed.focus(); return false; }

    static const(char)[] extractMail (const(char)[] s) nothrow @trusted @nogc {
      s = s.xstrip;
      if (s.length == 0 || s[$-1] != '>') return s;
      auto spp = s.lastIndexOf('<');
      if (spp < 0) return s;
      s = s[spp+1..$-1].xstrip;
      return s;
    }

    static const(char)[] extractName (const(char)[] s) nothrow @trusted @nogc {
      s = s.xstrip;
      if (s.length == 0 || s[$-1] != '>') return null;
      auto spp = s.lastIndexOf('<');
      if (spp < 0) return null;
      s = s[0..spp];
      return s.xstrip;
    }

    AccountInfo acc;
    if (!chiroGetAccountInfo(accname.getData, out acc)) return false;

    if (nntp != (acc.nntpgroup.length != 0)) return false;

    /*
    conwriteln("ACCOUNT ID: ", acc.accid);
    conwriteln("ACCOUNT NAME: ", acc.name);
    conwriteln("ACCOUNT REAL NAME: ", acc.realname);
    conwriteln("ACCOUNT EMAIL: ", acc.email);
    conwriteln("ACCOUNT NNTP GROUP: ", acc.nntpgroup);
    */

    dynstring fromname = extractName(from.str);
    dynstring frommail = extractMail(from.str);

    dynstring toname = extractName(to.str);
    dynstring tomail = extractMail(to.str);

    conwriteln("FROM: name=<", fromname, ">:<", strEncodeQ(fromname), "> : mail=<", frommail, ">");
    conwriteln("TO: name=<", toname, ">:<", strEncodeQ(toname), "> : mail=<", tomail, ">");

    if (!isGoodEmail(frommail)) { from.focus(); return false; }
    if (!nntp && !isGoodEmail(tomail)) { to.focus(); return false; }

    // build reply article and add it to send queue
    // check attaches
    ed.editor.clearAndDisableUndo(); // so removing attaches will not add 'em to undo, lol
    dynstring[] attnames = ed.extractAttaches();
    if (attnames.length) foreach (dynstring ss; attnames) conwriteln("ATTACH: ", ss);

    MessageBuilder msg;
    msg.setFromName(fromname);
    msg.setFromMail(frommail);
    msg.setToName(toname);
    msg.setToMail(tomail);
    if (acc.nntpgroup.length) msg.setNewsgroup(acc.nntpgroup);
    msg.setSubj(subj.str);
    msg.setBody(ed.getText);

    //if (replyto.length) msg.appendReference(replyto);

    const(char)[] refs = references.xstrip;
    while (refs.length) {
      usize spp = 0;
      while (spp < refs.length && refs[spp] > ' ') ++spp;
      msg.appendReference(refs[0..spp]);
      refs = refs[spp..$].xstrip;
    }

    foreach (dynstring ss; attnames) {
      try {
        msg.attachFile(ss);
      } catch (Exception e) {
        conwriteln("ERROR: cannot attach file '", ss, "'!");
      }
    }

    // clear editor, so it will free used memory
    ed.editor.clearAndDisableUndo();
    ed.editor.clear();

    bool skipFilters = true;
    // build tags
    dynstring tags;
    // account
    if (!nntp) {
      tags ~= "account:";
      tags ~= acc.name;
      if (desttag.length) {
        tags ~= "|";
        tags ~= desttag;
      } else if (acc.inbox.length) {
        // put to inbox
        tags ~= "|";
        tags ~= acc.inbox;
      } else {
        skipFilters = false;
      }
    }

    //conwriteln("TAGS: ", tags);
    //assert(0);

    dynstring mdata = msg.getPrepared;

    // for NNTP, we will do the trick: insert deleted message into the storage
    // this is because next NNTP check will receive it
/+
      uid INTEGER PRIMARY KEY  /* the same as in the storage, not automatic */
    , accid INTEGER            /* account from which this message should be sent */
    , from_pop3 TEXT           /* "from" for POP3 */
    , to_pop3 TEXT             /* "to" for POP3 */
    , data TEXT                /* PACKED data to send */
    , sendtime INTEGER DEFAULT 0      /* 0: not yet; unixtime */
    , lastsendtime INTEGER DEFAULT 0  /* when we last tried to send it? 0 means "not yet" */
+/

    // insert into main store
    uint uid;
    transacted!"Store"{
      foreach (auto row; dbStore.statement(`
          INSERT INTO messages(tags, data) VALUES(:tags, ChiroPack(:data)) RETURNING uid;`)
            .bindConstText(":tags", tags.getData)
            .bindConstBlob(":data", mdata.getData)
            .range)
      {
        uid = row.uid!uint;
      }
    };

    if (nntp) tomail.clear();

    // insert into unsent queue
    transacted!"View"{
      dbView.statement(`
        INSERT INTO unsent
              ( uid, accid, from_pop3, to_pop3, data)
        VALUES(:uid,:accid,:from_pop3,:to_pop3,ChiroPack(:data))
      ;`)
        .bind(":uid", uid)
        .bind(":accid", acc.accid)
        .bind(":accid", acc.accid)
        .bindConstText(":from_pop3", frommail.getData)
        .bindConstText(":to_pop3", tomail.getData)
        .bindConstBlob(":data", mdata.getData)
        .doAll();
    };

    conwriteln("message inserted");
    if (!nntp) {
      updateViewDB(skipFilters:skipFilters);
    }

    /*
    conwriteln("=======================");
    conwrite(msg.getPrepared);
    conwriteln("-----------------------");
    */

    return true;
  }

  override void createWidgets () {
    (new VBoxWidget).enter{
      (new HBoxWidget).enter{
        with (new HotLabelWidget("&From:", LabelWidget.HAlign.Right)) { width = width+2; hsizeId = "editors"; }
        new SpacerWidget(4);
        from = new LineEditWidget();
        from.flex = 1;
        from.readonly = true;
      }.flex = 1;

      new SpacerWidget(1);
      (new HBoxWidget).enter{
        with (new HotLabelWidget("&To:", LabelWidget.HAlign.Right)) { width = width+2; hsizeId = "editors"; }
        new SpacerWidget(4);
        to = new LineEditWidget();
        to.flex = 1;
      }.flex = 1;

      new SpacerWidget(1);
      (new HBoxWidget).enter{
        with (new HotLabelWidget("&Subj:", LabelWidget.HAlign.Right)) { width = width+2; hsizeId = "editors"; }
        new SpacerWidget(4);
        subj = new LineEditWidget();
        subj.flex = 1;
      }.flex = 1;
    };

    (new VBoxWidget).enter{
      ed = new EditorWidget();
      ed.flex = 1;
    }.flex = 1;

    new SpacerWidget(4);
    (new HBoxWidget).enter{
      new SpacerWidget(12);
      //new SpringWidget(1);
      with (new ButtonWidget(" Send ")) {
        hsizeId = "okcancel";
        //deftype = Default.Accept;
        onAction = delegate (self) {
          if (trySend) close(); else vbwin.beep();
        };
        flex = 1;
      }
      new SpacerWidget(12);
      with (new ButtonWidget(" Cancel ")) {
        hsizeId = "okcancel";
        //deftype = Default.Cancel;
        onAction = delegate (self) {
          close();
        };
        flex = 1;
      }
      //new SpringWidget(1);
      new SpacerWidget(12);
    };
    new SpacerWidget(4);

    relayout(); // don't resize window
    centerWindow();
  }

  this () {
    int wanthgt = screenHeight-42*2;
    if (wanthgt < 80) wanthgt = 80;
    int wantwdt = screenWidth-64*2;
    if (wantwdt < 506) wantwdt = 506;
    super("Compose Mail", GxSize(wantwdt, wanthgt));
    //if (hasWindowClass(this)) return;
  }

  override bool onKeySink (KeyEvent event) {
    if (event.pressed) {
      if (event == "Escape" && !ed.editor.textChanged) { close(); return true; }
      if (event == "C-G" || event == "C-C") {
        if (ed.editor.textChanged) {
          auto qww = new YesNoWindow("Close?", "Do you really want to close the editor?", true);
          qww.onYes = () { close(); };
          //qww.addModal();
        } else {
          close();
        }
        return true;
      }

      /+
      if (event == "M-Tab" && focusedWidget is to) {
        auto ae = abookFindByNick(to.str);
        //if (ae is null) ae = abookFindByMail(to.str);
        if (ae !is null) {
          if (ae.realname.length) to.str = ae.realname~" <"~ae.mail~">"; else to.str = ae.mail;
        } else {
          vbwin.beep();
        }
        return true;
      }
      +/

      // select destination from the address book
      if (event == "C-Space" && focusedWidget is to) {
        auto wae = new SelectAddressBookWindow(to.str);
        if (wae.lb.length != 0) {
          wae.onSelected = delegate (dynstring nick, dynstring name, dynstring email) {
            if (name.length) to.str = name~" <"~email~">"; else to.str = email;
          };
        } else {
          wae.close();
        }
        return true;
      }

      // select from account from the address book
      if (event == "C-Space" && focusedWidget is from) {
        if (allowAccountChange) {
          auto wacc = new SelectPopBoxWindow(accname);
          wacc.onSelected = delegate (uint accid) {
            AccountInfo acc;
            if (chiroGetAccountInfo(accid, out acc)) {
              accname = acc.name;
              /*
              conwriteln("ACCOUNT ID: ", acc.accid);
              conwriteln("ACCOUNT NAME: ", acc.name);
              conwriteln("ACCOUNT REAL NAME: ", acc.realname);
              conwriteln("ACCOUNT EMAIL: ", acc.email);
              conwriteln("ACCOUNT NNTP GROUP: ", acc.nntpgroup);
              */
              setFrom(acc.realname~" <"~acc.email~">");
            }
          };
        }
        return true;
      }

      if (event == "C-Enter") {
        auto qww = new YesNoWindow("Send?", "Do you really want to send the message?", true);
        qww.onYes = {
          if (trySend) close(); else vbwin.beep();
        };
        return true;
      }
    }
    return super.onKeyBubble(event);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public class TitlerWindow : SubWindow {
  LineEditWidget edtTitle;
  LineEditWidget fromName;
  LineEditWidget fromMail;
  //LineEditWidget fromTag;
  DynStr name;
  DynStr mail;
  DynStr folder;
  DynStr title;

  bool delegate (const(char)[] name, const(char)[] mail, const(char)[] folder, const(char)[] title) onSelected;

  override void createWidgets () {
    new SpacerWidget(rootWidget, 2);

    version(none) {
      fromName = new LineEditWidget(rootWidget, "Name:");
      fromName.flex = 1;

      new SpacerWidget(rootWidget, 2);
      fromMail = new LineEditWidget(rootWidget, "Mail:");
      fromMail.flex = 1;

      new SpacerWidget(rootWidget, 2);
      edtTitle = new LineEditWidget(rootWidget, "Title:");
      edtTitle.flex = 1;
    } else {
      (new HBoxWidget).enter{
        with (new HotLabelWidget("&Name:", LabelWidget.HAlign.Right)) { width = width+2; hsizeId = "editors"; }
        new SpacerWidget(4);
        fromName = new LineEditWidget();
        fromName.flex = 1;
      }.flex = 1;

      new SpacerWidget(1);
      (new HBoxWidget).enter{
        with (new HotLabelWidget("&Mail:", LabelWidget.HAlign.Right)) { width = width+2; hsizeId = "editors"; }
        new SpacerWidget(4);
        fromMail = new LineEditWidget();
        fromMail.flex = 1;
      }.flex = 1;

      new SpacerWidget(1);
      (new HBoxWidget).enter{
        with (new HotLabelWidget("&Title:", LabelWidget.HAlign.Right)) { width = width+2; hsizeId = "editors"; }
        new SpacerWidget(4);
        edtTitle = new LineEditWidget();
        edtTitle.flex = 1;
      }.flex = 1;
    }

    new SpacerWidget(4);
    (new HBoxWidget).enter{
      new SpacerWidget(2);
      new SpringWidget(1);
      with (new ButtonWidget(" O&k ")) {
        hsizeId = "okcancel";
        deftype = Default.Accept;
        onAction = delegate (self) {
          if (onSelected !is null) {
            if (!onSelected(fromName.str, fromMail.str, folder, edtTitle.str)) return;
          }
          close();
        };
      }
      new SpacerWidget(2);
      with (new ButtonWidget(" Cancel ")) {
        hsizeId = "okcancel";
        deftype = Default.Cancel;
        onAction = delegate (self) {
          close();
        };
      }
      new SpringWidget(1);
      new SpacerWidget(2);
    };
    new SpacerWidget(4);

    fromName.str = name;
    fromMail.str = mail;
    edtTitle.str = title;

    minWinSize.w = 320;

    edtTitle.focus();

    relayoutResize();
    centerWindow();
  }

  this (const(char)[] aname, const(char)[] amail, const(char)[] afolder, const(char)[] atitle) {
    name = aname;
    mail = amail;
    title = atitle;
    folder = afolder;

    DynStr caption = "Title for ";
    caption ~= aname;
    caption ~= " <";
    caption ~= amail;
    caption ~= ">";
    super(caption.getData);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public class TagOptionsWindow : SubWindow {
  LabelWidget optPath; // real path
  LineEditWidget optMonthes;
  CheckboxWidget optThreaded;
  CheckboxWidget optAttaches;
  DynStr tagname;

  void delegate (const(char)[] tagname) onUpdated;

  bool onAccept () {
    int mv = -666;
    try {
      auto vv = optMonthes.str.xstrip;
      if (vv.length == 0) {
        mv = -2;
      } else {
        mv = vv.toInt;
        if (mv < -1) mv = -666; else if (mv == 0) mv = -1;
      }
    } catch (Exception) {
      mv = -666;
    }
    if (mv < -2) return false;
    import core.stdc.stdio : snprintf;
    char[1024] xname = void;
    const(char)[] tn = tagname.getData;
    auto xlen = snprintf(xname.ptr, xname.sizeof, "/mainpane/msgview/monthlimit%s%.*s",
      (tn.length && tn[0] != '/' ? "/".ptr : "".ptr), cast(uint)tn.length, tn.ptr);
    if (mv == -2) {
      // delete
      if (xname[0..xlen] != "/mainpane/msgview/monthlimit") {
        chiroDeleteOption(xname[0..xlen]);
      }
    } else {
      // set
      chiroSetOption(xname[0..xlen], mv);
    }
    // fix threading and attaches
    if (optThreaded.enabled) {
      dbView.statement(`
        UPDATE tagnames
        SET threading=:trd, noattaches=:noatt
        WHERE tag=:name
      ;`)
        .bindConstText(":name", tagname.getData())
        .bind(":trd", (optThreaded.checked ? 1 : 0))
        .bind(":noatt", (optAttaches.checked ? 0 : 1))
        .doAll();
    }
    if (onUpdated !is null) onUpdated(tagname.getData);
    return true;
  }

  override void createWidgets () {
    optPath = new LabelWidget(rootWidget, "", LabelWidget.HAlign.Center);
    optPath.flex = 1;

    version(none) {
      optMonthes = new LineEditWidget(rootWidget, "Monthes:");
      optMonthes.flex = 1;
      optMonthes.width = optMonthes.titwdt+64;
    } else {
      (new HBoxWidget).enter{
        with (new HotLabelWidget("&Monthes:", LabelWidget.HAlign.Right)) { width = width+2; /*hsizeId = "editors";*/ }
        new SpacerWidget(4);
        optMonthes = new LineEditWidget();
        //optMonthes.flex = 1;
        optMonthes.width = gxTextWidthUtf("96669");
        //new SpringWidget(1);
      }.flex = 1;
    }

    optThreaded = new CheckboxWidget(rootWidget, "&Threaded");
    optThreaded.flex = 1;

    optAttaches = new CheckboxWidget(rootWidget, "&Attaches");
    optAttaches.flex = 1;

    new SpacerWidget(4);
    (new HBoxWidget).enter{
      new SpacerWidget(2);
      new SpringWidget(1);
      with (new ButtonWidget(" O&k ")) {
        hsizeId = "okcancel";
        deftype = Default.Accept;
        onAction = delegate (self) {
          if (onAccept()) close();
        };
      }
      new SpacerWidget(2);
      with (new ButtonWidget(" Cancel ")) {
        hsizeId = "okcancel";
        deftype = Default.Cancel;
        onAction = delegate (self) {
          close();
        };
      }
      new SpringWidget(1);
      new SpacerWidget(2);
    };
    new SpacerWidget(4);

    optMonthes.focus();

    immutable bool goodTag = (chiroGetTagUid(tagname.getData) != 0);
    {
      import std.conv : to;
      int val;
      DynStr path = chiroGetTagMonthLimitEx(tagname.getData, out val, defval:6);
      //conwriteln("TAGNAME=<", tagname.getData, ">; val=", val, "; path=<", path.getData, ">");
      optPath.text = path.getData;
      optMonthes.str = val.to!string;
      optPath.width = gxTextWidthUtf(optPath.text)+4;
    }

    if (goodTag) {
      foreach (auto row; dbView.statement(`
          SELECT threading AS trd, noattaches AS noatt FROM tagnames WHERE tag=:tag LIMIT 1
        ;`).bindConstText(":tag", tagname.getData).range)
      {
        optThreaded.checked = (row.trd!int == 1);
        optAttaches.checked = (row.noatt!int == 0);
      }
    } else {
      optThreaded.enabled = false;
      optAttaches.enabled = false;
    }

    optMonthes.killTextOnChar = true;

    minWinSize.w = optPath.width;
    relayoutResize();

    centerWindow();
  }

  this (const(char)[] atagname) {
    tagname = atagname;

    DynStr caption = "options for '";
    caption ~= atagname;
    caption ~= "'";
    super(caption.getData);
  }
}
