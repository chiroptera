import iv.vfs.io;

//font6x8
mixin(import("koifont.d"));


bool hasPixelAtX (uint ch, int x) {
  if (x < 0 || x > 7) return false;
  immutable ubyte mask = 0x80>>x;
  foreach (immutable ubyte b; vlFont6[ch*8..ch*8+8]) if (b&mask) return true;
  return false;
}

bool doShift (uint ch) {
  return
    (ch >= 32 && ch <= 127) ||
    (ch >= 143 && ch <= 144) ||
    (ch >= 166 && ch <= 167) ||
    (ch >= 192 && ch <= 255);
}


// hi nibble: lshift; lo nibble: width
void processChar (VFile fo, uint ch) {
  if (doShift(ch) && !(ch >= '0' && ch <= '9')) {
    foreach (immutable _; 0..8) {
      if (hasPixelAtX(ch, 0)) break;
      foreach (ref ubyte b; vlFont6[ch*8..ch*8+8]) b <<= 1;
    }
  }
  int wdt = 7;
  while (wdt >= 0 && !hasPixelAtX(ch, wdt)) --wdt;
  ++wdt;
  switch (ch) {
    case 0: wdt = 8; break; // 8px space
    case 32: wdt = 4; break; // 4px space
    case  17: .. case  27: wdt = 8; break; // single frames
    case  48: .. case  57: wdt = 5; break; // digits are monospaced
    case 128: .. case 142: wdt = 8; break; // filled frames
    case 145: .. case 151: wdt = 8; break; // filled frames
    case 155: .. case 159: wdt = 8; break; // filled frames
    //case 195: case 196: --wdt; break; // lowercase ce, lowercase de
    default:
  }
  //writeln(ch, " : ", wdt);
  fo.writeNum!ubyte(cast(ubyte)wdt);
  fo.rawWriteExact(vlFont6[ch*8..ch*8+8]);
}


void main () {
  auto fo = VFile("zxpfontkoi.fnt", "w");
  foreach (immutable ch; 0..256) processChar(fo, ch);
}
