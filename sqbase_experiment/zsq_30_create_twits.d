/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// WARNING! this doesn't perform any sanity checks on "accounts.rc"!
module zsq_create_twits is aliced;

import chibackend;

import iv.sq3;
import iv.strex;
import iv.timer;
import iv.utfutil;
import iv.vfs;
import iv.vfs.io;
import iv.vfs.util;


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  ChiroTimerEnabled = true;
  chiroParseCommonCLIArgs(args);

  // here, i don't have any threads at all
  if (sqlite3_config(SQLITE_CONFIG_SINGLETHREAD) != SQLITE_OK) throw new Exception("cannot switch SQLite to multi-threaded mode");

  bool allowed = false;
  foreach (string s; args[1..$]) {
    if (s == "force") allowed = true;
  }
  if (!allowed) throw new Exception("use \"force\" to rebuild");

  chiroOpenConfDB();

  // we will re-import them
  dbConf.execute(`
    DELETE FROM emailtwits;
    DELETE FROM msgidtwits;
  `);

  auto stInsTwitEMail = dbConf.persistentStatement(`
    INSERT INTO emailtwits
            ( tagglob, email, name, title, notes)
      VALUES(:tagglob,:email,:name,:title,:notes)
  ;`);

  auto stInsTwitMsgid = dbConf.persistentStatement(`
    INSERT INTO msgidtwits
            ( etwitid, tagglob, msgid, automatic)
      VALUES(:etwitid,:tagglob,:msgid, 0)
  ;`);

  uint hasTwitEMail (const(char)[] email, const(char)[] name) {
    if (name.length == 0) {
      if (email.length == 0) return 0;
      // email only
      foreach (auto row; dbConf.statement(`SELECT rowid AS twitid FROM emailtwits WHERE email=:email;`).bindConstText(":email", email).range) return row.twitid!uint;
    } else if (email.length == 0) {
      // name only
      foreach (auto row; dbConf.statement(`SELECT rowid AS twitid FROM emailtwits WHERE name=:name;`).bindConstText(":name", name).range) return row.twitid!uint;
    } else {
      // both name and email
      foreach (auto row; dbConf.statement(`SELECT rowid AS twitid FROM emailtwits WHERE email=:email AND name=:name;`)
                               .bindConstText(":email", email).bindConstText(":name", name).range) return row.twitid!uint;
    }
    return false;
  }

  /*
  bool hasTwitMsgid (const(char)[] msgid) {
    if (msgid.length == 0) return false;
    foreach (auto row; dbConf.statement(`SELECT rowid FROM msgidtwits WHERE msgid=:msgid;`).bindConstText(":msgid", msgid).range) return true;
    return false;
  }
  */

  dbConf.execute("BEGIN TRANSACTION;");
  scope(failure) dbConf.execute("ROLLBACK TRANSACTION;");

  void insertTwit (string[] argv) {
    string tagglob = null;
    string msgid = null;
    string name = null;
    string email = null;
    string title = null;
    string notes = null; /* url */

    if (argv[0] != "twit_set" && argv[0] != "twit_thread") throw new Exception("expected \"twit_set\", but got \""~argv[0]~"\"");
    if (argv.length < 2) throw new Exception("\"twit_set\" don't have any arguments");

    for (usize aidx = 1; aidx < argv.length; ) {
      string aname = argv[aidx++];
      switch (aname) {
        case "foldermask":
        case "folder_mask":
          if (tagglob !is null) throw new Exception("duplicate tag glob, old is \""~tagglob~"\", new is \""~argv[aidx+1]~"\"");
          tagglob = argv[aidx++];
          if (tagglob.length && tagglob[0].isalpha()) tagglob = "/"~tagglob;
          break;
        case "msgid":
        case "message":
        case "messageid":
        case "message_id":
          if (msgid !is null) throw new Exception("duplicate message id, old is \""~msgid~"\", new is \""~argv[aidx+1]~"\"");
          msgid = argv[aidx++];
          if (msgid.length >= 2 && msgid[0] == '<' && msgid[$-1] == '>') msgid = msgid[1..$-1].xstrip;
          break;
        case "mail":
        case "email":
        case "e-mail":
          if (email !is null) throw new Exception("duplicate email, old is \""~email~"\", new is \""~argv[aidx+1]~"\"");
          email = argv[aidx++].toLowerStr;
          if (!isGoodEmail(email) && email.indexOf('*') < 0) throw new Exception("invalid email \""~email~"\"");
          break;
        case "name":
          if (name !is null) throw new Exception("duplicate name, old is \""~name~"\", new is \""~argv[aidx+1]~"\"");
          name = argv[aidx++];
          break;
        case "title":
          if (title !is null) throw new Exception("duplicate title, old is \""~title~"\", new is \""~argv[aidx+1]~"\"");
          title = argv[aidx++];
          break;
        case "url":
        case "href":
          if (notes !is null) throw new Exception("duplicate notes, old is \""~notes~"\", new is \""~argv[aidx+1]~"\"");
          notes = argv[aidx++];
          break;
        default:
          throw new Exception("unknown option \""~aname~"\"");
      }
    }

    while (tagglob.length && tagglob[$-1] == '/') tagglob = tagglob[0..$-1];
    if (tagglob.length && isalnum(tagglob[0])) tagglob = "/"~tagglob;

    if (tagglob.length == 0) throw new Exception("missing tag glob");
    if (email.length == 0 && name.length == 0 && msgid.length == 0) {
      throw new Exception("missing email, name or msgid");
    }

    uint etid = hasTwitEMail(email, name);
    /*
    if (hasTwitEMail(email, name)) {
      writeln("duplicate twit for: email=\"", email, "\"; name=\"", name, "\"");
    } else
    */
    if (email.length || name.length) {
      stInsTwitEMail
        .bindConstText(":tagglob", tagglob)
        .bindConstText(":email", email)
        .bindConstText(":name", name)
        .bindConstText(":title", title, allowNull:true)
        .bindConstText(":notes", notes, allowNull:true)
        .doAll();
      etid = cast(uint)dbConf.lastRowId;
    }

    if (msgid.length) {
      stInsTwitMsgid
        .bind(":etwitid", etid)
        .bindConstText(":tagglob", tagglob)
        .bindConstText(":msgid", msgid, allowNull:true)
        .doAll();
    }
  }

  uint count = 0;
  foreach (string[] argv; loadRCFile("twits.rc")) { insertTwit(argv); ++count; }
  foreach (string[] argv; loadRCFile("twit_threads.rc")) { insertTwit(argv); ++count; }
  foreach (string[] argv; loadRCFile("auto_twits.rc")) { insertTwit(argv); ++count; }
  foreach (string[] argv; loadRCFile("auto_twit_threads.rc")) { insertTwit(argv); ++count; }
  dbConf.execute("COMMIT TRANSACTION;");

  writeln(count, " twit entries added.");

  writeln("closing the db");
  dbConf.execute("ANALYZE;");
  dbConf.close();
}
