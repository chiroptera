/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// this rebuilds FTS5 tables in SQLite supplementary mail database.
module zsq_rebuild_support is aliced;

import iv.encoding;
import iv.sq3;
import iv.strex;
import iv.timer;
import iv.vfs;
import iv.vfs.io;

import chibackend;


// ////////////////////////////////////////////////////////////////////////// //
void printReply (SQ3Text infield, SQ3Text reply) {
  if (reply.length == 0 || reply.indexOf('\x02') < 0) return;
  write("-- IN ", infield, " --: ");
  reply = reply.recodeToKOI8;
  char[] res;
  scope(exit) delete res;
  res.reserve(reply.length+64);
  foreach (char ch; reply) {
    if (ch >= 32) { res ~= ch; continue; }
    if (ch == 1) { res ~= "\x1b[1m...\x1b[0m"; continue; }
    if (ch == 2) { res ~= "\x1b[4m"; continue; }
    if (ch == 3) { res ~= "\x1b[0m"; continue; }
    if (ch == 26) { res ~= "|"; continue; }
    res ~= ' ';
  }
  writeln(res);
}


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  ChiroTimerEnabled = true;
  chiroParseCommonCLIArgs(args);

  // here, i don't have any threads at all
  if (sqlite3_config(SQLITE_CONFIG_SINGLETHREAD) != SQLITE_OK) throw new Exception("cannot switch SQLite to multi-threaded mode");

  writeln("opening message support db...");
  chiroOpenViewDB();

  /* sender, subj, text, html */
  //dbView.execute(`INSERT INTO fts5_messages(fts5_messages, rank) VALUES('rank', 'bm25(1.0, 3.0, 10.0, 6.0)');`);

  //writeln("selecting max recorded message...");
  //foreach (auto row; dbView.statement(`SELECT MAX(uid) AS cnt FROM fts5_messages;`).range) writeln(row.cnt!uint);

  auto stSelFTS5 = dbView.persistentStatement(`
    SELECT
        snippet(fts5_messages, 0, CHAR(2), CHAR(3), CHAR(1), 6) AS sender
      , snippet(fts5_messages, 1, CHAR(2), CHAR(3), CHAR(1), 6) AS subj
      , snippet(fts5_messages, 2, CHAR(2), CHAR(3), CHAR(1), 16) AS text
      , snippet(fts5_messages, 3, CHAR(2), CHAR(3), CHAR(1), 16) AS html
      , rowid as uid
    FROM fts5_messages
    WHERE fts5_messages MATCH :query
    ORDER BY rank;
  ;`);

  foreach (string q; args[1..$]) {
    q = q.xstrip;
    if (q.length == 0) continue;
    writeln("================================");
    writeln(q);
    writeln("--------------------------------");
    q = q.recode("utf-8", "koi8-u");
    Timer ctm = Timer(true);
    foreach (auto frow; stSelFTS5.bindConstText(":query", q).range) {
      auto sender = frow.sender!SQ3Text;
      auto subj = frow.subj!SQ3Text;
      auto text = frow.text!SQ3Text;
      auto html = frow.html!SQ3Text;
      writeln("  * * * * * * * * * * * * * * * * * * * UID:", frow.uid!uint);
      printReply("SENDER", sender);
      printReply("SUBJ", subj);
      printReply("TEXT", text);
      printReply("HTML", html);
    }
    ctm.stop;
    writeln("--------------------------------");
    writeln("time: ", ctm.toString, "\x1b[K");
  }

  dbView.close();
}
