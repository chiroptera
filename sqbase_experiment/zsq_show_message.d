/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// this rebuilds FTS5 tables in SQLite supplementary mail database.
module zsq_rebuild_support is aliced;

import iv.encoding;
import iv.sq3;
import iv.strex;
import iv.timer;
import iv.vfs;
import iv.vfs.io;

import chibackend;


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  ChiroTimerEnabled = true;
  chiroParseCommonCLIArgs(args);

  // here, i don't have any threads at all
  if (sqlite3_config(SQLITE_CONFIG_SINGLETHREAD) != SQLITE_OK) throw new Exception("cannot switch SQLite to multi-threaded mode");

  writeln("opening message support db...");
  chiroOpenViewDB();

  if (args.length < 2) {
    import std.conv : to;
    uint uid = 0;
    foreach (auto row; dbView.statement(`SELECT MAX(uid) AS uid FROM info;`).range) uid = row.uid!uint;
    args ~= uid.to!string;
  } else {
    //import std.conv : to;
    //uid = argv[1].to!uint;
  }

  auto stSelEmailInfo = dbView.persistentStatement(`
    SELECT
        uid AS uid
      , from_name AS fromName
      , from_mail AS fromMail
      , to_name AS toName
      , to_mail AS toMail
      , subj AS subj
      , datetime(threads.time, 'unixepoch') AS time
      , ChiroUnpack(content_text.content) AS text
      , ChiroUnpack(content_html.content) AS html
    FROM info
    INNER JOIN threads USING(uid)
    INNER JOIN content_text USING(uid)
    INNER JOIN content_html USING(uid)
    WHERE uid=:uid
    LIMIT 1
  ;`);

  auto stSelEmailTags = dbView.persistentStatement(`
    SELECT
        DISTINCT(threads.tagid) AS tagid
      , tagnames.tag AS tagname
    FROM threads
    INNER JOIN tagnames
      USING(tagid)
    WHERE threads.uid=:uid
  ;`);

  foreach (string uidstr; args[1..$]) {
    uint uid = 0;
    try {
      import std.conv : to;
      uid = uidstr.to!uint;
    } catch (Exception) {
      writeln("INVALID UID: ", uidstr);
    }
    if (!uid) continue;
    foreach (auto mrow; stSelEmailInfo.bind(":uid", uid).range) {
      writeln("================ UID: ", uid, " ================");
      writeln("From: ", mrow.fromName!SQ3Text.recodeToKOI8, " <", mrow.fromMail!SQ3Text.recodeToKOI8, ">");
      writeln("  To: ", mrow.toName!SQ3Text.recodeToKOI8, " <", mrow.toMail!SQ3Text.recodeToKOI8, ">");
      writeln("Subj: ", mrow.subj!SQ3Text.recodeToKOI8);
      writeln("Date: ", mrow.time!SQ3Text);

      write("Tags:");
      bool wasTag = false;
      foreach (auto trow; stSelEmailTags.bind(":uid", uid).range) {
        if (wasTag) write(","); else wasTag = true;
        write(" ", trow.tagname!SQ3Text);
      }
      writeln;

      if (mrow.text!SQ3Text.length) {
        writeln("----------------------------------------------- (text)");
        writeln(mrow.text!SQ3Text.recodeToKOI8);
      }

      if (mrow.html!SQ3Text.length) {
        writeln("----------------------------------------------- (html)");
        writeln(mrow.html!SQ3Text.recodeToKOI8);
      }
    }
  }

  dbView.close();
}
