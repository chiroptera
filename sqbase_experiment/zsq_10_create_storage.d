/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// this converts old chiroptera mailbox CHMT database to new SQLite database
// this only puts message data into "messages" table; you need to rebuild
// other tables with the separate utility
module zsq_create_storage is aliced;

import chibackend;

import std.file;

import iv.sq3;
import iv.strex;
import iv.timer;
import iv.vfs;
import iv.vfs.io;
import iv.vfs.util;


// ////////////////////////////////////////////////////////////////////////// //
bool checkPfx (const(char)[] dir, const(char)[] pfx) {
  if (pfx.length == 0 || dir.length < pfx.length) return false;
  if (dir[0..pfx.length] != pfx) return false;
  return (pfx.length >= dir.length || dir[pfx.length] == '/');
}


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  ChiroTimerEnabled = true;
  chiroParseCommonCLIArgs(args);

  bool allowed = false;
  bool update = false;
  bool emptybase = false;
  bool ignorenntp = true;

  foreach (string s; args[1..$]) {
         if (s == "force") allowed = true;
    else if (s == "empty") emptybase = true;
    else if (s == "nntp") ignorenntp = false;
    else if (s == "update") update = true;
  }

  if (!allowed && !update) throw new Exception("use \"force\" to rebuild the storage");

  // but here, i don't have any threads at all
  if (sqlite3_config(SQLITE_CONFIG_SINGLETHREAD) != SQLITE_OK) throw new Exception("cannot switch SQLite to multi-threaded mode");

  string[] ignores;
  string[] files;
  string[string] tagrepl;

  auto hidfo = VFile("zhidden.rc", "w");

  writeln("scanning '", chiroCLIMailPath, "'...");
  foreach (DirEntry e; dirEntries(cast(string)chiroCLIMailPath, SpanMode.breadth, followSymlink:false)) {
    if (!e.isFile) continue;
    if (e.name.dirName == chiroCLIMailPath) continue;
    if (e.name.getExtension == ".chmt") {
      //writeln(e.name, "\t", e.size);
      files ~= e.name[chiroCLIMailPath.length..$];
    } else if (e.name.baseName == ".chiignore") {
      ignores ~= e.name.dirName[chiroCLIMailPath.length..$];
    } else if (e.name.baseName == ".chihidden") {
      hidfo.writeln(e.name.dirName[chiroCLIMailPath.length-1..$]);
    } else if (e.name.baseName == ".chitag") {
      foreach (auto s; VFile(e.name).byLine) {
        s = s.xstrip;
        if (s.length == 0 || s[0] == '#') continue;
        tagrepl[e.name.dirName[chiroCLIMailPath.length..$]] = s.idup;
        break;
      }
    }
  }
  hidfo.close();

  // remove ignored files
  for (usize f = 0; f < files.length; ++f) {
    bool ok = true;
    foreach (string ign; ignores) if (files[f].dirName.checkPfx(ign)) { ok = false; break; }
    if (!ok) {
      writeln("ignored '", files[f], "'");
      foreach (usize n; f+1..files.length) files[n-1] = files[n];
      files.length -= 1;
      --f;
      continue;
    }
  }

  writeln(files.length, " databases found.");

  if (update) {
    chiroOpenStorageDB();
  } else {
    writeln("creating message store db (compression level: ", ChiroCompressionLevel, ")...");
    chiroRecreateStorageDB();
  }

  writeln("opening accounts db...");
  chiroOpenConfDB(readonly:true);

  auto stInsMsg = dbStore.persistentStatement(`
    INSERT INTO messages (tags, data) VALUES(:tags, ChiroPack(:data))
  ;`);

  auto stGetAcc = dbConf.persistentStatement("SELECT name AS name FROM accounts WHERE inbox=:inbox;");

  auto stGetAccByMail = dbConf.persistentStatement(`
    SELECT name AS name, inbox AS inbox
    FROM accounts
    WHERE nntpgroup='' AND inbox<>'' AND lower(email)=lower(:email)
    LIMIT 1
  ;`);

  bool dupsCreated = false;
  VFile dupfo;

  Timer ctm = Timer(true);

  dbStore.execute("BEGIN TRANSACTION;");
  uint count = 0;
  bool forceUpdateProgress = false;
  foreach (immutable tgidx, string tdbname; files) {
    if (emptybase) continue;

    string tag = tdbname.dirName;
    if (auto rp = tag in tagrepl) {
      auto oldtag = tag;
      tag = *rp;
      if (tag.length && tag[0] == '/') {
        while (tag.length && tag[$-1] == '/') tag = tag[0..$-1];
      }
      writeln(oldtag, " -> ", tag, "\x1b[K");
      assert(tag.length && tag != "/");
      forceUpdateProgress = true;
    } else {
      while (tag.length && tag[0] == '/') tag = tag[1..$];
      while (tag.length && tag[$-1] == '/') tag = tag[0..$-1];
      tag = "/"~tag;
      // join archives
      auto aaidx = tag.lastIndexOf("/arch");
      if (aaidx > 0 && tag.indexOf('/', aaidx+1) < 0) {
        auto oldtag = tag;
        tag = tag[0..aaidx];
        while (tag.length && tag[$-1] == '/') tag = tag[0..$-1];
        writeln(oldtag, " -> ", tag, "\x1b[K");
        assert(tag.length && tag != "/");
        forceUpdateProgress = true;
      }
    }
    //if (tag != "/sent") continue;
    if (ignorenntp && tag.startsWith("/dmars_ng/")) continue;

    char[] alltext;
    scope(exit) delete alltext;
    {
      auto fl = VFile(chiroCLIMailPath~tdbname);
      auto sz = fl.size;
      if (sz > 0x3fffffffU) throw new Exception("text database '"~tdbname~"' too big");
      if (sz > 0) {
        alltext = new char[cast(uint)sz];
        fl.rawReadExact(alltext[]);
      }
    }

    immutable origTag = tag;

    char[] text = alltext;
    while (text.length) {
      usize end = findMessageEnd!true(text); // with dot
      const(char)[] msg = text[0..end];
      text = cutTopMessage(text);

      char[] tags;
      scope(exit) delete tags;
      tags.reserve(1024);

      tag = origTag;
      if (tag.startsWith("/zz_spam")) tag = "#spam";

      if (tag == "/sent") {
        string fromMail = null;
        foreach (auto xrow; dbStore.statement(`SELECT ChiroHdr_FromEmail(:text) AS email;`).bindConstText(":text", msg).range) {
          fromMail = xrow.email!string;
        }
        string found = null;
        foreach (auto srow; stGetAccByMail.bindConstText(":email", fromMail).range) {
          found = srow.inbox!string;
        }
        if (found.length) {
          //forceUpdateProgress = true;
          //writeln(fromMail, " moved to ", found, "\x1b[K");
          tag = found;
        } else {
          forceUpdateProgress = true;
          writeln(fromMail, " has no account\x1b[K");
          writeln(msg);
        }
      }

      bool seenAccount = false;
      tags ~= tag;
      // find account
      foreach (auto row; stGetAcc.bindConstText(":inbox", tag).range) {
        auto name = row.name!SQ3Text;
        if (name.length) {
          tags ~= "|account:";
          tags ~= name;
          seenAccount = true;
        }
      }
      // check for accounts
      if (!seenAccount && tag != "#spam") {
        string fromMail = null;
        string toMail = null;
        foreach (auto xrow; dbStore.statement(`SELECT ChiroHdr_FromEmail(:text) AS frommail, ChiroHdr_ToEmail(:text) AS tomail;`).bindConstText(":text", msg).range) {
          fromMail = xrow.frommail!string;
          toMail = xrow.tomail!string;
        }
        string found = null;
        foreach (auto srow; stGetAccByMail.bindConstText(":email", toMail).range) found = srow.name!string;
        if (found.length == 0) {
          foreach (auto srow; stGetAccByMail.bindConstText(":email", fromMail).range) found = srow.name!string;
        }
        if (found.length) {
          //forceUpdateProgress = true;
          //writeln(fromMail, " moved to ", found, "\x1b[K");
          tags ~= "|account:";
          tags ~= found;
        }
      }
      // done

      stInsMsg
        .bindConstText(":tags", tags)
        .bindConstBlob(":data", msg)
        .doAll();

      ++count;
      if (forceUpdateProgress || count%512 == 1) {
        forceUpdateProgress = false;
        write(" ", count, " ", tgidx*100/files.length, "%  ", tags, "\x1b[K\r");
      }
    }
  }
  dbStore.execute("COMMIT TRANSACTION;");

  ctm.stop;
  writeln("time: ", ctm.toString, "\x1b[K");

  writeln(count, " messages found.\x1b[K");

  // restore journal and sync options (journal is saved into db, and sync MAY BE saved later)
  // we aren't expecting to change things much, so "DELETE" journal seems to be adequate
  dbStore.execute(`
    PRAGMA locking_mode = NORMAL;
    PRAGMA journal_mode = DELETE;
    PRAGMA synchronous = NORMAL;
  `);

  writeln("closing the db");
  dbStore.close();

  dbConf.close();

  // remove journals (required to cleanup WAL files even for R/O database)
  chiroOpenConfDB();
  dbConf.close();
}
