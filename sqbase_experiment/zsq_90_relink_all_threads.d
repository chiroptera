/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// this rebuilds supplementary tables in SQLite mail database.
// it parses the messages, extract mime parts, and populates
// "headers", "content", and "attaches" tables.
// those tables are required for the main program to work properly,
// but they can be dropped and recreated at any time.
module zsq_relink_all_threads is aliced;

import iv.encoding;
import iv.sq3;
import iv.strex;
import iv.timer;
import iv.vfs;
import iv.vfs.io;

import chibackend;


// ////////////////////////////////////////////////////////////////////////// //
__gshared bool wasProgress = false;

extern(C) int SQ3Progress (void *udata) {
  wasProgress = true;
  write(".");
  return 0; // continue
}


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  ChiroTimerEnabled = true;
  chiroParseCommonCLIArgs(args);

  MailDBPath = "_000";

  // here, i don't have any threads at all
  //if (sqlite3_config(SQLITE_CONFIG_SINGLETHREAD) != SQLITE_OK) throw new Exception("cannot switch SQLite to multi-threaded mode");

  writeln("opening message support db (compression level ", ChiroCompressionLevel, ")...");
  chiroOpenViewDB();

  if (args.length < 2) {
    writeln("linking threads...");
    chiroSupportRelinkAllThreads();
  } else {
    dbView.execute(`
      CREATE TEMP TABLE timing (
          tagid INTEGER PRIMARY KEY
        , micro INTEGER
        , relink INTEGER
        , tagname TEXT
      )
    ;`);
    auto stInsTiming = dbView.persistentStatement(`INSERT INTO timing (tagid, micro, relink, tagname) VALUES(:tagid, :micro, :relink, :tagname);`);
    uint count = 0;
    sqlite3_progress_handler(dbView.getHandle, 32, &SQ3Progress);
    Timer ctm = Timer(true);
    foreach (auto row; dbView.statement(`SELECT tag AS tagname, tagid AS tagid, threading AS relink FROM tagnames ORDER BY tagid;`).range) {
      ++count;
      //write("relinking '", row.tagname!SQ3Text, "' ... ");
      Timer xtm = Timer(true);
      chiroSupportRelinkTagThreads(row.tagid!uint);
      xtm.stop;
      //writeln(xtm);
      stInsTiming
        .bind(":tagid", row.tagid!uint)
        .bind(":micro", xtm.micro)
        .bind(":relink", row.relink!uint)
        .bindConstText(":tagname", row.tagname!SQ3Text)
        .doAll();
    }
    ctm.stop;
    if (wasProgress) writeln;
    writeln(count, " threads relink time: ", ctm);
    foreach (auto row; dbView.statement(`SELECT avg(micro) AS micro FROM timing;`).range) {
      ctm.micro = row.micro!uint;
      writeln("average time: ", ctm);
    }
    foreach (auto row; dbView.statement(`SELECT min(micro) AS micro FROM timing;`).range) {
      ctm.micro = row.micro!uint;
      writeln("minimum time: ", ctm);
    }
    foreach (auto row; dbView.statement(`SELECT max(micro) AS micro FROM timing;`).range) {
      ctm.micro = row.micro!uint;
      writeln("maximum time: ", ctm);
    }
    foreach (auto row; dbView.statement(`SELECT tagid AS tagid, micro AS micro, tagname AS tagname, relink AS relink FROM timing ORDER BY micro;`).range) {
      uint total = 0;
      foreach (auto trow; dbView.statement(`SELECT count(uid) AS total FROM threads WHERE tagid=:tagid;`).bind(":tagid", row.tagid!uint).range) total = trow.total!uint;
      ctm.micro = row.micro!uint;
      writefln("%5s:(%s): %s -- %s", total, (row.relink!uint ? "relink" : "clear "), row.tagname!SQ3Text, ctm);
    }
  }

  uint ttotal = 0;
  foreach (auto row; dbView.statement("SELECT count(*) AS total FROM threads WHERE EXISTS(SELECT threading FROM tagnames WHERE tagnames.tagid=threads.tagid AND threading=1);").range) {
    ttotal = row.total!uint;
  }

  if (ttotal == 0) { writeln("WUTAFUCK!?"); ttotal = 1; }

  foreach (auto row; dbView.statement("SELECT count(*) AS parented FROM threads WHERE parent<>0;").range) {
    writeln(row.parented!uint, " of ", ttotal, " messages with parents found (", row.parented!uint*100/ttotal, "%).");
  }

  dbView.close();
}
