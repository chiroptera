/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module zsq_show_threads_tty is aliced;

import iv.encoding;
import iv.sq3;
import iv.strex;
import iv.timer;
import iv.vfs;
import iv.vfs.io;

import chibackend;


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  ChiroTimerEnabled = true;
  chiroParseCommonCLIArgs(args);

  // here, i don't have any threads at all
  //chiroSwitchToSingleThread();

  bool speedTest = false;
  bool showLast = false;
  bool allowThreads = true;
  int monthes = 6; // last 6 monthes

  writeln("opening message support db...");
  chiroOpenViewDB();

  if (args.length > 1 && args[1] == "list") {
    string[] folderlist = chiroGetTagList();
    foreach (string fld; folderlist) writeln("  ", fld);
    return;
  }

  string tagname = "/dmars_ng/general";
  bool tagset = false;

  for (usize aidx = 1; aidx < args.length; ) {
    string arg = args[aidx++];
    if (args.length == 0) continue;
    if (arg == "speed") { speedTest = true; continue; }
    if (arg == "last") { showLast = true; continue; }
    if (arg == "threads") { allowThreads = true; continue; }
    if (arg == "nothreads") { allowThreads = false; continue; }
    if (arg == "list") {
      string[] folderlist = chiroGetTagList();
      foreach (string fld; folderlist) writeln("  ", fld);
      continue;
    }
    if (arg == "limit") {
      // limit in monthes
      if (aidx >= args.length) throw new Exception("\"limit\" requires an argument");
      import std.conv : to;
      monthes = args[aidx++].to!int;
      continue;
    }
    if (tagset) throw new Exception("tag already selected");
    tagset = true;
    tagname = arg;
    while (tagname.length > 1 && tagname[$-1] == '/') tagname = tagname[0..$-1];
  }

  writeln("collecting messages...");

  Timer ctm = Timer(true);
  version(all) {
    chiroCreateTreePaneTable(tagname, monthes, allowThreads);
  } else {
    chiroCreateTreePaneTable(chiroGetTagUid(tagname), monthes, allowThreads);
  }
  ctm.stop;

  uint ncount = 0;
  foreach (auto row; dbView.statement(`SELECT max(rowid) AS ncount FROM treepane;`).range) ncount = row.ncount!uint;
  writeln("collect time (", ncount, " messages): ", ctm);

  if (speedTest) return;

  // 128 spaces for slicing
  immutable string padstr = `                                                                                                                                `;
  // 128 dots for slicing
  immutable string paddot = `................................................................................................................................`;

  uint pgstart = 0;
  uint pgsize = 128;
  //if (pgstart > ncount) pgstart = 0;
  if (showLast && ncount > pgsize) pgstart = ncount-pgsize+2;
  ctm.restart;
  auto count = chiroGetPaneTablePage(pgstart, pgsize,
      delegate (uint pgofs, /* offset from the page start, from zero and up to `limit` */
                uint iid, /* item id, never zero */
                uint uid, /* msguid, never zero */
                uint parentuid, /* parent msguid, may be zero */
                uint level, /* threading level, from zero */
                int appearance, /* flags, etc. */
                SQ3Text date, /* string representation of receiving date and time */
                SQ3Text subj, /* message subject, can be empty string */
                SQ3Text fromName, /* message sender name, can be empty string */
                SQ3Text fromMail) /* message sender email, can be empty string */
  {
    version(all) {
      writef("%3s | ", pgofs);
      writef("%6s | ", iid);
      writef("%6s | ", uid);
      writef("%6s | ", parentuid);
      write(date, " | ");
      //foreach (; 0..level) write(".");
      if (level > paddot.length) level = cast(uint)paddot.length;
      write(paddot[0..level]);
      subj = subj.recodeToKOI8;
      auto nlen = 64-level;
      if (nlen > 0) {
        if (subj.length > nlen) {
          if (nlen > 3) {
            write(subj[0..nlen-3], "...");
          } else {
            write(subj[0..nlen]);
          }
        } else {
          write(subj);
        }
      }
      //foreach (; subj.length..cast(usize)nlen) write(" ");
      if (subj.length < cast(usize)nlen) write(padstr[0..cast(usize)nlen-subj.length]);
      enum MaxNameLen = 32;
      fromName = fromName.recodeToKOI8;
      if (fromName.length > MaxNameLen) {
        write(" | ", fromName[0..MaxNameLen-3], "...");
      } else {
        write(" | ", fromName);
        //foreach (; fromName.length..MaxNameLen) write(" ");
        write(padstr[0..cast(usize)MaxNameLen-fromName.length]);
      }
      if (fromMail.length > MaxNameLen) {
        writeln(" | <", fromMail[0..MaxNameLen-3], "...");
      } else {
        writeln(" | <", fromMail, ">");
      }
    }
  });
  ctm.stop;
  writeln("render time for ", count, " items: ", ctm);

  // this is not necessary, but meh...
  //chiroReleaseTreePaneTable();

  writeln("closing the db");
  dbView.close();
}
