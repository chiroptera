/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// this rebuilds supplementary tables in SQLite mail database.
// it parses the messages, extract mime parts, and populates
// "headers", "content", and "attaches" tables.
// those tables are required for the main program to work properly,
// but they can be dropped and recreated at any time.
module zsq_rebuild_support is aliced;

import iv.encoding;
import iv.sq3;
import iv.strex;
import iv.timer;
import iv.vfs;
import iv.vfs.io;

import chibackend;


// ////////////////////////////////////////////////////////////////////////// //
const(char)[] extractAccount (const(char)[] tags) {
  auto stp = tags.indexOf("account:");
  while (stp >= 0) {
    if (stp == 0 || tags[stp-1] == '|') {
      tags = tags[stp+8..$];
      stp = tags.indexOf('|');
      if (stp >= 0) tags = tags[0..stp];
      return tags;
    }
  }
  return null;
}


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  ChiroTimerEnabled = true;
  chiroParseCommonCLIArgs(args);

  // here, i don't have any threads at all
  if (sqlite3_config(SQLITE_CONFIG_SINGLETHREAD) != SQLITE_OK) throw new Exception("cannot switch SQLite to multi-threaded mode");

  // we will need to update "nntplastindex"
  chiroOpenConfDB();

  // updating indicies each time is not the best idea
  // let's collect them first
  dbConf.execute(`
    CREATE TEMP TABLE nntpidc (
        nntpidx INTEGER
      , accname TEXT NOT NULL UNIQUE  /* autoindex */
    );

    -- reset indicies
    UPDATE accounts SET nntplastindex=0 WHERE nntplastindex<>0;
  `);

  auto stInsNNTPIdx = dbConf.persistentStatement(`
    INSERT INTO nntpidc (accname, nntpidx) VALUES(:accname,:nntpidx)
    ON CONFLICT(accname)
    DO UPDATE SET nntpidx = max(nntpidx, :nntpidx)
  ;`);

  // and this will update the accounts
  auto stUpdateNNTP = dbConf.persistentStatement(`
    UPDATE accounts
    SET
      --nntplastindex = MAX(nntplastindex, nntpidc.nntpidx)
      nntplastindex = nntpidc.nntpidx
    FROM
      (SELECT accname AS accname, nntpidx AS nntpidx FROM nntpidc) AS nntpidc
    WHERE
      accounts.name = nntpidc.accname
  ;`);

  chiroOpenStorageDB(readonly:true);

  bool[string] hiddentags;
  try {
    foreach (string s; VFile("zhidden.rc").byLineCopy) {
      s = s.xstrip;
      if (s.length == 0 || s[0] == '#') continue;
      hiddentags[s] = true;
    }
  } catch (Exception) {}


  writeln("creating message support db (compression level ", ChiroCompressionLevel, ")...");
  chiroRecreateViewDB();

  uint total = 0;
  foreach (auto row; dbStore.persistentStatement(`SELECT count(uid) AS total FROM messages WHERE tags <> '';`).range) {
    total = row.total!uint;
    break;
  }
  writeln("found ", total, " messages to process.");

  // this monstrosity selects all unique tags
  auto stAllTags = dbStore.persistentStatement(`
    WITH RECURSIVE tagtable(tag, rest) AS (
      VALUES('', (SELECT group_concat(tags,'|') FROM (SELECT DISTINCT(tags) AS tags FROM messages WHERE tags <> ''))||'|')
     UNION ALL
      SELECT
        SUBSTR(rest, 0, INSTR(rest, '|')),
        SUBSTR(rest, INSTR(rest, '|')+1)
      FROM tagtable
      WHERE rest <> '')
    SELECT DISTINCT(tag) AS tag
    FROM tagtable
    WHERE tag <> ''
    ORDER BY tag
  ;`);


  auto stInsTagName = dbView.persistentStatement("INSERT INTO tagnames (tag, hidden, threading, noattaches) VALUES(:tag,:hidden,:threading,:noattaches);");

  bool[string] noattachtags;

  // insert all unique tags
  dbView.execute("BEGIN TRANSACTION;");
  foreach (auto row; stAllTags.range) {
    auto tag = row.tag!SQ3Text;
    assert(tag.length);
    // all non-folder tags are hidden
    bool isHidden = (tag[0] != '/');
    immutable bool isSpam = tag.startsWith("#spam");
    immutable bool noattaches =
      isSpam ||
      tag.startsWith("/notifications/") || tag == "/notifications" ||
      tag.startsWith("/lists/") || tag == "/lists" ||
      tag.startsWith("/lj/") || tag == "/lj" ||
      tag.startsWith("/ljross/") || tag == "/ljross" ||
      false;
    immutable bool nothreads =
      isHidden || isSpam ||
      tag.startsWith("/notifications/") || tag == "/notifications" ||
      false;
    if (noattaches) noattachtags[tag.idup] = true;
    if (tag in hiddentags) isHidden = true;
    stInsTagName
      .bindConstText(":tag", tag)
      .bind(":hidden", (isHidden ? 1 : 0))
      .bind(":threading", (nothreads ? 0 : 1))
      .bind(":noattaches", (noattaches ? 1 : 0))
      .doAll();
  }
  dbView.execute("COMMIT TRANSACTION;");

  Timer ctm = Timer(true);
  RunningAverageExp ravg;
  //ravg.progressThreshold = 1024;
  ravg.startTimer(0);
  uint count = chiroParseAndInsertMessages(0, relink:false, asread:true, //emsgid:616241,
    progresscb:delegate (uint count, uint total, uint nntpidx, const(char)[] tags) {
      if (count == 1) ravg.timerTotal = total;

      if (nntpidx > 0) {
        auto accname = extractAccount(tags);
        if (accname.length) {
          stInsNNTPIdx
            .bindConstText(":accname", accname)
            .bind(":nntpidx", nntpidx)
            .doAll();
        }
      }

      ravg.updateProcessedWithProgress(1);
      /*
      if (count%1024 == 1) {
        write(" ", count, " ", count*100u/total, "%  ", tags, "\x1b[K\r");
      }

      if (nntpidx > 0) {
        while (tags.length) {
          auto eep = tags.indexOf('|');
          auto tagname = (eep >= 0 ? tags[0..eep] : tags[0..$]);
          tags = (eep >= 0 ? tags[eep+1..$] : tags[0..0]);
          if (tagname.length == 0) continue;
          if (nntpidx > 0 && tagname.startsWith("account:")) {
            auto accname = tagname[8..$];
            stInsNNTPIdx
              .bindConstText(":accname", accname)
              .bind(":nntpidx", nntpidx)
              .doAll();
          }
        }
      }
      */
    }
  );
  ctm.stop;
  writeln("time: ", ctm.toString, "\x1b[K");
  writeln(count, " messages processed.\x1b[K");

  dbStore.close();

  // update NNTP indicies
  stUpdateNNTP.doAll();
  dbConf.close();

  writeln("creating indicies...");
  ctm.restart;
  chiroCreateViewIndiciesDB();
  ctm.stop;
  writeln("time (indexing): ", ctm.toString, "\x1b[K");

  ctm.restart;
  dbView.execute("ANALYZE;");
  ctm.stop;
  writeln("time (analyzing): ", ctm.toString, "\x1b[K");

  writeln("linking threads...");
  ctm.restart;
  chiroSupportRelinkAllThreads();
  ctm.stop;
  writeln("time: ", ctm.toString, "\x1b[K");
  uint ttotal = 0;
  foreach (auto row; dbView.statement("SELECT count(*) AS total FROM threads WHERE EXISTS(SELECT threading FROM tagnames WHERE tagnames.tagid=threads.tagid AND threading=1);").range) {
    ttotal = row.total!uint;
  }
  if (ttotal == 0) { writeln("WUTAFUCK!?"); ttotal = 1; }
  foreach (auto row; dbView.statement("SELECT count(*) AS parented FROM threads WHERE parent<>0;").range) {
    writeln(row.parented!uint, " of ", ttotal, " messages with parents found (", row.parented!uint*100/ttotal, "%).");
  }

  writeln("closing the db");
  ctm.restart;
  dbView.close();
  ctm.stop;
  writeln("time (analyzing): ", ctm.toString, "\x1b[K");
}
