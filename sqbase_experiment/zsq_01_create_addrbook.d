/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// WARNING! this doesn't perform any sanity checks on "accounts.rc"!
module zsq_create_addrbook is aliced;

import chibackend;

import iv.sq3;
import iv.strex;
import iv.utfutil;
import iv.vfs;
import iv.vfs.io;
import iv.vfs.util;


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  ChiroTimerEnabled = true;
  chiroParseCommonCLIArgs(args);

  // here, i don't have any threads at all
  if (sqlite3_config(SQLITE_CONFIG_SINGLETHREAD) != SQLITE_OK) throw new Exception("cannot switch SQLite to multi-threaded mode");

  bool allowed = false;
  foreach (string s; args[1..$]) {
    if (s == "force") allowed = true;
  }
  if (!allowed) throw new Exception("use \"force\" to rebuild");

  chiroOpenConfDB();

  auto stInsAddress = dbConf.persistentStatement(`
    INSERT INTO addressbook
            ( nick, name, email, notes)
      VALUES(:nick,:name,:email,:notes)
  ;`);

  dbConf.execute("BEGIN TRANSACTION;");
  scope(failure) dbConf.execute("ROLLBACK TRANSACTION;");
  uint count = 0;
  foreach (string[] argv; loadRCFile("addressbook.rc")) {
    if (argv[0] != "addressbook_add") throw new Exception("expected \"addressbook_add\", but got \""~argv[0]~"\"");
    if (argv.length < 2) throw new Exception("\"addressbook_add\" don't have any arguments");

    //uint accid = chiroGetAccountUid(dbConf, argv[1]);
    //if (!accid) throw new Exception("account \""~argv[1]~"\" not found");

    string nick = argv[1];
    if (!isValidUTFNick(nick)) throw new Exception("invalid addressbook nick \""~nick~"\"");

    string name = null;
    string email = null;
    string notes = null;

    for (usize aidx = 2; aidx < argv.length; ) {
      string aname = argv[aidx++];
      switch (aname) {
        case "name":
          if (name !is null) throw new Exception("duplicate name in account \""~nick~"\"");
          name = argv[aidx++];
          break;
        case "email":
          if (email !is null) throw new Exception("duplicate email in account \""~nick~"\"");
          email = argv[aidx++];
          if (!isGoodEmail(email)) throw new Exception("invalid email in account \""~nick~"\"");
          break;
        case "notes":
          if (notes !is null) throw new Exception("duplicate notes in account \""~nick~"\"");
          notes = argv[aidx++];
          if (!utf8Valid(notes)) throw new Exception("invalid notes in account \""~nick~"\"");
          break;
        default:
          throw new Exception("unknown option \""~aname~"\"");
      }
    }

    if (email.length == 0) throw new Exception("missing email in account \""~nick~"\"");

    stInsAddress
      .bindConstText(":nick", nick)
      .bindConstText(":name", name)
      .bindConstText(":email", email)
      .bindConstText(":notes", notes, allowNull:true)
      .doAll();

    ++count;
  }
  dbConf.execute("COMMIT TRANSACTION;");

  writeln(count, " address book entries added.");

  writeln("closing the db");
  dbConf.close();
}
