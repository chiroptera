/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// WARNING! this doesn't perform any sanity checks on "accounts.rc"!
module zsq_create_accounts is aliced;

import chibackend;

import iv.sq3;
import iv.strex;
import iv.vfs;
import iv.vfs.io;
import iv.vfs.util;


// ////////////////////////////////////////////////////////////////////////// //
void setMonthLimits () {
  auto st = dbConf.persistentStatement(`
    INSERT INTO options
            ( name, value)
      VALUES(:name,:value)
    ON CONFLICT(name)
    DO UPDATE SET value=:value
  `);

  st
    .bindConstText(":name", "/mainpane/msgview/monthlimit")
    .bind(":value", 6)
    .doAll();

  st
    .bindConstText(":name", "/mainpane/msgview/monthlimit/accounts")
    .bind(":value", -1)
    .doAll();

  st
    .bindConstText(":name", "/mainpane/msgview/monthlimit/dmars_ng")
    .bind(":value", 4)
    .doAll();

  st
    .bindConstText(":name", "/mainpane/msgview/monthlimit/lj_and_ljr")
    .bind(":value", 2)
    .doAll();
}


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  ChiroTimerEnabled = true;
  chiroParseCommonCLIArgs(args);

  // here, i don't have any threads at all
  if (sqlite3_config(SQLITE_CONFIG_SINGLETHREAD) != SQLITE_OK) throw new Exception("cannot switch SQLite to multi-threaded mode");

  bool allowed = false;
  foreach (string s; args[1..$]) {
    if (s == "force") allowed = true;
  }
  if (!allowed) throw new Exception("use \"force\" to rebuild");

  try {
    chiroOpenConfDB();
  } catch (Exception) {
    chiroRecreateConfDB();
  }

  auto stInsAcc = dbConf.persistentStatement(`
    INSERT INTO accounts
            ( checktime, nosendauth, debuglog, name, recvserver, sendserver, user, pass, realname, email, inbox, nntpgroup)
      VALUES(:checktime,:nosendauth,:debuglog,:name,:recvserver,:sendserver,:user,:pass,:realname,:email,:inbox,:nntpgroup)
  ;`);

  dbConf.execute("BEGIN TRANSACTION;");
  scope(failure) dbConf.execute("ROLLBACK TRANSACTION;");
  uint count = 0;
  foreach (string[] argv; loadRCFile("accounts.rc")) {
    string accname = argv[1];
    if (!isValidUTFNick(accname)) throw new Exception("invalid account name \""~accname~"\"");
    string inbox = "/accounts/"~accname~"/inbox";

    stInsAcc
      .bind(":checktime", 15)
      .bind(":nosendauth", 0)
      .bind(":debuglog", 0)
      .bindConstText(":name", accname)
      .bindConstText(":recvserver", "")
      .bindConstText(":sendserver", "")
      .bindConstText(":user", "")
      .bindConstText(":pass", "")
      .bindConstText(":realname", "")
      .bindConstText(":email", "")
      .bindConstText(":inbox", inbox)
      .bindConstText(":nntpgroup", "");

    for (usize aidx = 2; aidx < argv.length; ) {
      string aname = argv[aidx++];
      switch (aname) {
        case "debuglog": case "debug_log":
          stInsAcc.bind(":debuglog", 1);
          break;
        case "no_debuglog": case "no_debug_log":
          stInsAcc.bind(":debuglog", 0);
          break;
        case "smtp_no_auth": case "smtp_noauth":
          stInsAcc.bind(":nosendauth", 1);
          break;
        case "default":
          break;
        case "user":
          stInsAcc.bindConstText(":user", argv[aidx++]);
          break;
        case "pass":
        case "password":
          stInsAcc.bindConstText(":pass", argv[aidx++]);
          break;
        case "name":
        case "realname":
        case "real_name":
          stInsAcc.bindConstText(":realname", argv[aidx++]);
          break;
        case "server":
          stInsAcc.bindConstText(":recvserver", argv[aidx++]);
          break;
        case "smtpserver":
        case "smtp_server":
          stInsAcc.bindConstText(":sendserver", argv[aidx++]);
          break;
        case "mail":
        case "email":
        case "e-mail":
          stInsAcc.bindConstText(":email", argv[aidx++]);
          break;
        case "folder":
          {
            string s = argv[aidx++];
            while (s.length && s[0] == '/') s = s[1..$];
            while (s.length && s[$-1] == '/') s = s[0..$-1];
            s = "/"~s;
            assert(s != "/");
            stInsAcc.bindText(":inbox", s);
          }
          break;
        case "group":
          stInsAcc.bindConstText(":nntpgroup", argv[aidx++]);
          break;
        case "checktime":
        case "check_time":
          {
            import std.conv : to;
            stInsAcc.bind(":checktime", argv[aidx++].to!uint);
          }
          break;
        default:
          throw new Exception("unknown option \""~aname~"\"");
      }
    }

    switch (argv[0]) {
      case "popbox":
      case "nntpbox":
        break;
      default:
        throw new Exception("unknown account type \""~argv[0]~"\"");
    }

    stInsAcc.doAll();
    ++count;
  }
  setMonthLimits();
  dbConf.execute("COMMIT TRANSACTION;");

  writeln(count, " accounts added.");

  writeln("closing the db");
  dbConf.close();
}
