/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// this rebuilds FTS5 tables in SQLite supplementary mail database.
module zsq_rebuild_fts5 is aliced;

import iv.encoding;
import iv.sq3;
import iv.strex;
import iv.timer;
import iv.vfs;
import iv.vfs.io;

import chibackend;

// ////////////////////////////////////////////////////////////////////////// //
extern(C) int progressCB (void*) nothrow @nogc {
  import core.sys.posix.unistd : write, STDOUT_FILENO;
  static immutable string rotator = "|/-\\";
  static ubyte rpos = 0;
  char[4] buf = void;
  buf[0] = ' ';
  buf[1] = rotator.ptr[rpos++];
  buf[2] = '\r';
  write(STDOUT_FILENO, buf.ptr, 3);
  return 0;
}


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  ChiroTimerEnabled = true;
  chiroParseCommonCLIArgs(args);

  // here, i don't have any threads at all
  if (sqlite3_config(SQLITE_CONFIG_SINGLETHREAD) != SQLITE_OK) throw new Exception("cannot switch SQLite to multi-threaded mode");

  writeln("opening message support db...");
  chiroOpenViewDB();

  writeln("recreating FTS5 table...");

  Timer ctm = Timer(false);

  // now update FTS5
  if (args.length < 2 || args[1] != "separate") {
    chiroRecreateFTS5();

    uint total = 0;
    foreach (auto row; dbView.statement(`SELECT COUNT(*) AS total FROM info;`).range) total = row.total!uint;

    //sqlite3_progress_handler(dbView.getHandle, 2, &progressCB);

    uint processed = 0;
    foreach (auto row; dbView.statement(`
      SELECT COUNT(uid) AS processed
      FROM info
      WHERE
        EXISTS (
          SELECT threads.tagid FROM threads
          INNER JOIN tagnames USING(tagid)
          WHERE
            threads.uid=info.uid AND
            tagnames.hidden=0 AND SUBSTR(tagnames.tag, 1, 1)='/'
        )
    ;`).range) processed = row.processed!uint;

    writeln("\r", total, " messages processed (", total-processed, " skipped; ", processed, " recorded).\x1b[K");
  } else {
    chiroRecreateFTS5(repopulate:false);

    auto stInsFTS5 = dbView.persistentStatement(`
      INSERT INTO fts5_messages
              ( rowid, sender, subj, text, html)
        VALUES(  :uid,:sender,:subj,:text,:html)
    ;`);

    auto stSelEmailNeedFTS = dbView.persistentStatement(`
      SELECT threads.tagid AS tagid
      FROM threads
      INNER JOIN tagnames USING(tagid)
      WHERE
        threads.uid=:uid AND
        tagnames.hidden=0 AND SUBSTR(tagnames.tag, 1, 1)='/'
      LIMIT 1
    ;`);

    writeln("populating FTS5 table...");
    uint total = 0;
    foreach (auto row; dbView.statement(`SELECT count(*) AS total FROM info;`).range) { total = row.total!uint; break; }

    uint processed = 0;
    ctm.restart;
    dbView.execute(`BEGIN TRANSACTION;`);
    foreach (auto row; dbView.statement(`
      SELECT uid AS uid, sender AS sender, subj AS subj, text AS text, html AS html
      FROM fts5_msgview
      WHERE
        EXISTS (
          SELECT threads.tagid FROM threads
          INNER JOIN tagnames USING(tagid)
          WHERE
            threads.uid=fts5_msgview.uid AND
            tagnames.hidden=0 AND SUBSTR(tagnames.tag, 1, 1)='/'
        )
    ;`).range) {
      ++processed;
      immutable uid = row.uid!uint;

      stInsFTS5
        .bind(":uid", uid)
        .bindConstText(":sender", row.sender!SQ3Text)
        .bindConstText(":subj", row.subj!SQ3Text)
        .bindConstText(":text", row.text!SQ3Text)
        .bindConstText(":html", row.html!SQ3Text)
        .doAll();

      if (processed%1024 == 1) {
        write(" ", processed, " ", processed*100u/total, "%\x1b[K\r");
      }
    }
    dbView.execute(`COMMIT TRANSACTION;`);
    ctm.stop;
    writeln("time: ", ctm.toString, "\x1b[K");
    writeln(total, " messages processed (", total-processed, " skipped, ", processed, " recorded).\x1b[K");
  }

  ctm.restart;
  dbView.close();
  writeln("closing time: ", ctm.toString, "\x1b[K");
}
