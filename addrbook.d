/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module addrbook /*is aliced*/;
private:

import iv.alice;
import iv.cmdcon;
import iv.dynstring;
import iv.strex;
import iv.vfs.io;


// ////////////////////////////////////////////////////////////////////////// //
public final class AddressBookEntry {
  dynstring nick;
  dynstring realname; // can be empty
  dynstring mail; // email
}


public __gshared AddressBookEntry[] abook;


// ////////////////////////////////////////////////////////////////////////// //
public AddressBookEntry abookFindByNickFirst (const(char)[] nick) {
  nick = nick.xstrip;
  if (nick.length == 0) return null;

  foreach (AddressBookEntry ae; abook) {
    if (ae.nick.strEquCI(nick)) return ae;
  }

  foreach (AddressBookEntry ae; abook) {
    if (ae.nick.startsWithCI(nick)) return ae;
  }

  return null;
}


public AddressBookEntry abookFindByNick (const(char)[] nick) {
  nick = nick.xstrip;
  if (nick.length == 0) return null;

  foreach (AddressBookEntry ae; abook) {
    if (ae.nick.strEquCI(nick)) return ae;
  }

  AddressBookEntry partial;
  foreach (AddressBookEntry ae; abook) {
    if (ae.nick.startsWithCI(nick)) {
      if (partial !is null) return null;
      partial = ae;
    }
  }
  return partial;
}


// ////////////////////////////////////////////////////////////////////////// //
public AddressBookEntry abookFindByMailFirst (const(char)[] mail) {
  mail = mail.xstrip;
  if (mail.length == 0) return null;

  foreach (AddressBookEntry ae; abook) {
    if (ae.mail.strEquCI(mail)) return ae;
  }

  foreach (AddressBookEntry ae; abook) {
    if (ae.mail.startsWithCI(mail)) return ae;
  }

  return null;
}


public AddressBookEntry abookFindByMail (const(char)[] mail) {
  mail = mail.xstrip;
  if (mail.length == 0) return null;

  foreach (AddressBookEntry ae; abook) {
    if (ae.mail.strEquCI(mail)) return ae;
  }

  AddressBookEntry partial;
  foreach (AddressBookEntry ae; abook) {
    if (ae.mail.startsWithCI(mail)) {
      if (partial !is null) return null;
      partial = ae;
    }
  }
  return partial;
}


// ////////////////////////////////////////////////////////////////////////// //
shared static this () {
  //addressbook_add  nick noip    name "Ketmar Dark"  email "ketmar@ketmar.no-ip.org"
  conRegFunc!((ConString nick, ConString[] args) {
    nick = nick.xstrip;
    if (nick.length == 0) { conwriteln("addressbook_add: empty nick"); return; }
    auto origargs = args;
    auto ae = new AddressBookEntry();
    ae.nick = nick.idup;
    while (args.length) {
      if (args.length < 2) { conwriteln("addressbook_add: invalid args: ", origargs); return; }
      auto opt = args[0];
      auto arg = args[1];
      args = args[2..$];
      switch (opt) {
        case "mail":
        case "email":
          if (ae.mail.length != 0) {
            conwriteln("addressbook_add: duplicate mail option: '", arg, "'");
            conwriteln("addressbook_add: invalid args: ", origargs);
            return;
          }
          ae.mail = arg.idup;
          break;
        case "name":
        case "realname":
        case "real_name":
          if (ae.realname.length != 0) {
            conwriteln("addressbook_add: duplicate name option: '", arg, "'");
            conwriteln("addressbook_add: invalid args: ", origargs);
            return;
          }
          ae.realname = arg.idup;
          break;
        default:
          conwriteln("addressbook_add: unknown options: '", opt, "'");
          conwriteln("addressbook_add: invalid args: ", origargs);
          return;
      }
    }
    if (ae.mail.length == 0) { conwriteln("addressbook_add: invalid args (no mail): ", origargs); return; }
    foreach (ref AddressBookEntry oae; abook) {
      if (oae.nick.strEquCI(nick)) { oae = ae; return; }
    }
    abook ~= ae;
  })("addressbook_add", "add address book entry");
}
