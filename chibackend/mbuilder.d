/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module chibackend.mbuilder is aliced;
private:

import iv.strex;
import iv.vfs;

import chibackend : DynStr, SysTimeToRFCString;
import chibackend.parse : skipOneLine;


// ////////////////////////////////////////////////////////////////////////// //
public:
mixin(NewExceptionClass!("MessageBuilderException", "Exception"));


// ////////////////////////////////////////////////////////////////////////// //
struct MessageBuilder {
private:
  static class Attach {
    DynStr name;
    DynStr mime;
    DynStr data;
  }

  static class Reference {
    DynStr s;
    this (const(char)[] as) nothrow @trusted @nogc { s = as; }
  }

  //WARNING! all those fields are NOT GC-ALLOCATED!
  DynStr fromName;
  DynStr fromMail;
  DynStr toName;
  DynStr toMail;
  DynStr toNewsgroup;
  DynStr subj;
  DynStr body;
  Reference[] references = null;
  DynStr boundary;
  DynStr prepared;

  Attach[] attaches = null;


  void buildHeaders () {
    boundary.clear();
    prepared.clear();
    prepared.reserve(8192);

    prepared ~= "From: ";
    if (fromName.length) { prepared.appendQEncoded(fromName); prepared ~= " <"; }
    prepared ~= fromMail;
    if (fromName.length) prepared ~= ">";
    prepared ~= "\r\n";

    if (toNewsgroup.length) {
      prepared ~= "Newsgroups: ";
      prepared ~= toNewsgroup;
      prepared ~= "\r\n";
    } else {
      prepared ~= "To: ";
      if (toName.length) { prepared.appendQEncoded(toName); prepared ~= " <"; }
      prepared ~= toMail;
      if (toName.length) prepared ~= ">";
      prepared ~= "\r\n";
    }

    prepared ~= "Subject: ";
    if (subj.length) {
      prepared.appendQEncoded(subj);
    } else {
      prepared ~= "no subject";
    }
    prepared ~= "\r\n";

    // msgid
    {
      prepared ~= "Message-ID: <";
      import std.uuid;
      UUID id = randomUUID();
      foreach (immutable ubyte b; id.data[]) {
        prepared ~= "0123456789abcdef"[b>>4];
        prepared ~= "0123456789abcdef"[b&0x0f];
      }
      prepared ~= "@chiroptera>\r\n";
    }

    prepared ~= "User-Agent: Chiroptera\r\n";

    if (references.length) {
      prepared ~= "In-Reply-To: <";
      prepared ~= references[0].s;
      prepared ~= ">\r\n";
    }

    if (references.length) {
      prepared ~= "References:";
      usize nlen = 12;
      foreach (Reference reference; references) {
        if (nlen >= 76) {
          prepared ~= "\r\n ";
          nlen = 1;
        }
        prepared ~= " <";
        prepared ~= reference.s;
        prepared ~= ">";
        nlen += reference.s.length+3;
      }
      prepared ~= "\r\n";
    }

    // date
    {
      import std.datetime;
      prepared ~= "Date: ";
      prepared ~= SysTimeToRFCString(Clock.currTime);
      prepared ~= "\r\n";
    }

    string textEncoding = "US-ASCII";
    foreach (immutable char ch; body.getData) if (ch >= 128) { textEncoding = "UTF-8"; break; }

    prepared ~= "Mime-Version: 1.0\r\n";
    if (attaches.length == 0) {
      // no attachments
      prepared ~= "Content-Type: text/plain; charset=";
      prepared ~= textEncoding;
      prepared ~= "; format=flowed; delsp=no\r\n";
      prepared ~= "Content-Transfer-Encoding: 8bit\r\n";
    } else {
      // generate boundary
      import std.uuid;
      UUID id = randomUUID();
      boundary.clear();
      boundary.reserve!false(2+16*2+11+8+64);
      boundary ~= "------==--";
      foreach (immutable ubyte b; id.data[]) {
        boundary ~= "0123456789abcdef"[b>>4];
        boundary ~= "0123456789abcdef"[b&0x0f];
      }
      boundary ~= ".chiroptera--";

      prepared ~= "Content-Type: multipart/mixed; boundary=\"";
      prepared ~= boundary;
      prepared ~= "\"\r\n";
      // end of main headers
      prepared ~= "\r\n";
      // useless comment
      prepared ~= "This is a multi-part message in MIME format.\r\n";
      prepared ~= boundary;
      prepared ~= "\r\n";
      prepared ~= "Content-Type: text/plain; charset=";
      prepared ~= textEncoding;
      prepared ~= "; format=flowed; delsp=no\r\n";
      prepared ~= "Content-Transfer-Encoding: 8bit\r\n";
    }

    // end of headers
    prepared ~= "\r\n";
  }

  void appendBody () {
    // put body; correctly dot-stuffed, with CRLF
    const(char)[] buf = body.getData.xstripright;
    while (buf.length && (buf[0] == '\r' || buf[0] == '\n')) buf = buf[1..$];
    while (buf.length) {
      usize epos = skipOneLine(buf, 0);
      usize eend = epos;
           if (eend >= 2 && buf[eend-2] == '\r' && buf[eend-1] == '\n') eend -= 2;
      else if (eend >= 1 && buf[eend-1] == '\n') eend -= 1;
      // dot-stuffing
      if (eend == 1 && buf[0] == '.') {
        prepared ~= '.';
      } else if (boundary.length && eend == boundary.length && buf[0..eend] == boundary.getData) {
        prepared ~= '_';
      }
      bool canWhole = true;
      foreach (immutable char ch; buf[0..eend]) {
        if (ch < 32) {
          if (ch < 9 || ch > 13 || ch == 11) { canWhole = false; break; }
        }
      }
      if (canWhole) {
        prepared ~= buf[0..eend];
      } else {
        foreach (char ch; buf[0..eend]) {
          if (ch < 32) {
            if (ch < 9 || ch > 13 || ch == 11) ch = ' ';
          }
          prepared ~= ch;
        }
      }
      prepared ~= "\r\n";
      buf = buf[epos..$];
    }
  }

  void appendAttaches () {
    static struct ExtMime {
      string ext;
      string mime;
    }
    static immutable ExtMime[24] knownMimes = [
      ExtMime(".png", "image/png"),
      ExtMime(".jpg", "image/jpeg"),
      ExtMime(".jpeg", "image/jpeg"),
      ExtMime(".gif", "image/gif"),
      // text
      /*
      ExtMime(".txt", "text/plain; charset=US-ASCII"),
      ExtMime(".patch", "text/plain; charset=US-ASCII"),
      ExtMime(".diff", "text/plain; charset=US-ASCII"),
      ExtMime(".d", "text/plain; charset=US-ASCII"),
      ExtMime(".c", "text/plain; charset=US-ASCII"),
      ExtMime(".cc", "text/plain; charset=US-ASCII"),
      ExtMime(".h", "text/plain; charset=US-ASCII"),
      ExtMime(".hpp", "text/plain; charset=US-ASCII"),
      // html
      ExtMime(".htm", "text/html; charset=US-ASCII"),
      ExtMime(".html", "text/html; charset=US-ASCII"),
      */
      // archives
      ExtMime(".zip", "application/x-compressed"),
      ExtMime(".7z", "application/x-compressed"),
      ExtMime(".pk3", "application/x-compressed"),
      ExtMime(".gz", "application/x-compressed"),
      ExtMime(".lz", "application/x-compressed"),
      ExtMime(".xz", "application/x-compressed"),
      ExtMime(".tgz", "application/x-compressed"),
      ExtMime(".tlz", "application/x-compressed"),
      ExtMime(".txz", "application/x-compressed"),
      ExtMime(".tar", "application/x-compressed"),
    ];

    foreach (const ref Attach attach; attaches) {
      if (attach.data.length == 0) continue;
      const(char)[] fname = attach.name.getData.xstrip;
      while (fname.length) {
        auto stp = fname.lastIndexOf('/');
        if (stp < 0) break;
        fname = fname[stp+1..$].xstrip;
      }

      void putFName () {
        prepared ~= '"';
        if (fname.length == 0) {
          prepared ~= "unnamed";
        } else {
          foreach (char ch; fname) {
            if (ch <= 32 || ch >= 127 || ch == '/' || ch == '\\' || ch == '?' ||
                ch == '*' || ch == '&' || ch == '|' || ch == '<' || ch == '>' ||
                ch == '"' || ch == '\'')
            {
              ch = '_';
            }
            prepared ~= ch;
          }
        }
        prepared ~= '"';
      }

      prepared ~= boundary;
      prepared ~= "\r\n";

      prepared ~= "Content-Disposition: attachment; filename=";
      putFName();
      prepared ~= "\r\n";

      string tenc = "base64";
      const(char)[] mime = attach.mime.getData;
      if (mime.length == 0) {
        foreach (const ref ExtMime ee; knownMimes) {
          if (fname.endsWithCI(ee.ext)) {
            mime = ee.mime;
            break;
          }
        }
        // check if it can be treated as a text
        if (mime.length == 0) {
          bool oktext = true;
          foreach (immutable idx, immutable char ch; attach.data.getData) {
            if (ch < 32) {
              if (ch < 9 || ch > 13 || ch == 11) { oktext = false; break; }
              if (ch == 27 && idx == attach.data.length-1) break;
            } else if (ch >= 127) {
              oktext = false;
              break;
            }
          }
          if (oktext) {
            mime = "text/plain; charset=US-ASCII";
            tenc = "8bit";
          } else {
            mime = "application/octet-stream";
          }
        }
      }
      prepared ~= "Content-Type: ";
      prepared ~= mime;
      prepared ~= "; name=";
      putFName();
      prepared ~= "\r\n";
      prepared ~= "Content-Transfer-Encoding: ";
      prepared ~= tenc;
      prepared ~= "\r\n";
      prepared ~= "\r\n"; // end of headers
      if (tenc == "base64") {
        prepared.appendB64Encoded(attach.data);
      } else {
        assert(tenc == "8bit");
        prepared ~= attach.data;
      }
    }
  }

  void finishPrepared () {
    // final boundary
    if (attaches.length != 0) {
      prepared ~= "--";
      prepared ~= boundary;
      prepared ~= "\r\n";
    }
    // final dot
    prepared ~= ".\r\n";
  }

public:
  //this () pure nothrow @trusted @nogc {}

  @disable this (this);

  ~this () {
    foreach (ref Reference reference; references) delete reference;
    delete references;
    foreach (ref Attach attach; attaches) delete attach;
    delete attaches;
  }

  void setFromName (const(char)[] value) nothrow @trusted @nogc { fromName = value; }
  void setFromMail (const(char)[] value) nothrow @trusted @nogc { fromMail = value; }
  void setToName (const(char)[] value) nothrow @trusted @nogc { toName = value; }
  void setToMail (const(char)[] value) nothrow @trusted @nogc { toMail = value; }
  void setNewsgroup (const(char)[] value) nothrow @trusted @nogc { toNewsgroup = value; }
  void setSubj (const(char)[] value) nothrow @trusted @nogc { subj = value; }
  void setBody (const(char)[] value) nothrow @trusted @nogc { body = value; }

  // first appended reference will be "In-Reply-To"
  void appendReference (const(char)[] msgid) {
    msgid = msgid.xstrip;
    if (msgid.length == 0) return;
    references ~= new Reference(msgid);
  }

  void appendAttach (const(char)[] filename, const(void)[] data, const(char)[] mime=null) {
    if (data.length == 0) return;
    Attach att = new Attach;
    att.name = filename;
    att.mime = mime;
    att.data = cast(const(char)[])data;
    attaches ~= att;
  }

  void attachFile (const(char)[] filename) {
    auto fl = VFile(filename);
    if (fl.size > 0x00ff_ffff) throw new MessageBuilderException("file too big");
    if (fl.size == 0) return;
    Attach att = new Attach;
    scope(failure) delete att;
    att.name = filename;
    //att.mime = mime; // let the engine determine it
    //att.data.reserve!false(cast(uint)fl.size);
    att.data.length = cast(uint)fl.size;
    assert(!att.data.isShared && !att.data.isSlice && att.data.length == cast(uint)fl.size);
    fl.rawReadExact(att.data.makeUniquePointer[0..cast(uint)fl.size]);
    attaches ~= att;
  }

  // should be called after setting everything
  // WARNING! returned data will NOT outlive this struct!
  const(char)[] getPrepared () {
    buildHeaders();
    appendBody();
    appendAttaches();
    finishPrepared();
    return prepared.getData;
  }
}
