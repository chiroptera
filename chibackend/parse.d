/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module chibackend.parse is aliced;

import chibackend.decode;

import iv.strex;
private import iv.vfs;
private import iv.vfs.util;
private import iv.vfs.io : byLine;
private import chibackend : chiroCLIMailPath, DynStr;


// ////////////////////////////////////////////////////////////////////////// //
public string[][] loadRCFile (const(char)[] fname) {
  string[][] res;
  string ff;
  if (fname.length && fname[0] == '/') {
    ff = cast(string)fname; // it is safe to cast here
  } else if (fname.length && fname[0] == '~') {
    char[] dpath = new char[fname.length+128];
    dpath = expandTilde(dpath, fname);
    ff = cast(string)dpath; // it is safe to cast here
  } else {
    char[] dpath;
    dpath.reserve(chiroCLIMailPath.length+fname.length+65);
    dpath ~= chiroCLIMailPath;
    dpath ~= fname;
    ff = cast(string)dpath; // it is safe to cast here
  }
  foreach (auto line; VFile(ff).byLine) {
    line = line.xstrip;
    if (line.length == 0 || line[0] == '#') continue;
    string[] argv;
    while (line.length) {
      if (line[0] <= 32) { line = line[1..$]; continue; }
      char[] word;
      word.reserve(64);
      if (line[0] == '"') {
        line = line[1..$];
        while (line.length) {
          char ch = line[0];
          line = line[1..$];
          if (ch == '"') break;
          if (ch == '\\') { ch = line[0]; line = line[1..$]; }
          word ~= ch;
        }
      } else {
        while (line.length && line[0] > 32) {
          word ~= line[0];
          line = line[1..$];
        }
      }
      argv ~= cast(string)word; // it is safe to cast here
    }
    if (argv.length) res ~= argv;
  }
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
// returned position is always [0..buf.length]
public usize skipOneLine (const(char)[] buf, usize pos) pure nothrow @trusted @nogc {
  import core.stdc.string : memchr;
  if (pos >= buf.length || buf.length == 0) return buf.length;
  const(char)* ep = cast(const(char) *)memchr(buf.ptr+pos, '\n', buf.length-pos);
  if (ep is null) return buf.length;
  ++ep;
  return cast(usize)(ep-buf.ptr);
}


// ////////////////////////////////////////////////////////////////////////// //
// return `false` from dg to stop
public void forEachHeaderLine (const(char)[] buf, bool delegate (const(char)[] line) dg) {
  auto anchor = buf;
  if (dg is null) return;
  if (buf.length == 0) return;
  usize lpos = 0;
  while (lpos < buf.length) {
    if (isEmptyLine(buf, lpos)) return;
    usize nlpos = skipOneLine(buf, lpos);
    // collect continuations
    while (nlpos < buf.length && buf.ptr[nlpos] <= ' ') nlpos = skipOneLine(buf, nlpos);
    if (!dg(buf[lpos..nlpos])) return;
    lpos = nlpos;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
private bool isDotLine (const(char)[] buf, usize pos) pure nothrow @trusted @nogc {
  if (pos >= buf.length || buf.ptr[pos] != '.') return false;
  ++pos;
  if (pos < buf.length && buf.ptr[pos] == '\r') ++pos;
  return (pos >= buf.length || buf.ptr[pos] == '\n');
}


// ////////////////////////////////////////////////////////////////////////// //
private bool isEmptyLine (const(char)[] buf, usize pos) pure nothrow @trusted @nogc {
  if (pos >= buf.length) return true;
  if (buf.ptr[pos] == '\r') { if (++pos >= buf.length) return false; }
  return (pos >= buf.length || buf.ptr[pos] == '\n');
}


// ////////////////////////////////////////////////////////////////////////// //
// returns `buf.length` if no proper end was found
// otherwise returns position BEFORE the final dot and newline
public usize findMessageEnd(bool withDot=false) (const(char)[] buf) pure nothrow @trusted @nogc {
  if (buf.length == 0) return 0;
  usize lpos = 0;
  while (lpos < buf.length) {
    if (isDotLine(buf, lpos)) {
      static if (withDot) {
        return skipOneLine(buf, lpos);
      } else {
        return lpos;
      }
    }
    lpos = skipOneLine(buf, lpos);
  }
  return buf.length;
}


// ////////////////////////////////////////////////////////////////////////// //
// returns `buf.length` if no proper end was found
// otherwise returns position at the beginnig of the empty line
public usize findHeadersEnd (const(char)[] buf) pure nothrow @trusted @nogc {
  if (buf.length == 0) return 0;
  usize lpos = 0;
  while (lpos < buf.length) {
    if (isEmptyLine(buf, lpos)) return lpos;
    lpos = skipOneLine(buf, lpos);
  }
  return buf.length;
}


// ////////////////////////////////////////////////////////////////////////// //
public T cutTopMessage(T:const(char)[]) (T buf) pure nothrow @trusted @nogc {
  static if (!is(T == typeof(null))) {
    if (buf.length == 0) return null;
    usize lpos = 0;
    while (lpos < buf.length) {
      immutable usize nlpos = skipOneLine(buf, lpos);
      if (isDotLine(buf, lpos)) return (nlpos < buf.length ? buf[nlpos..$] : null);
      lpos = nlpos;
    }
  }
  return null;
}


// ////////////////////////////////////////////////////////////////////////// //
// this takes the first field
// returns field data, or `null` (never returns empty values)
// field name should not contain ':'
public T findHeaderField(T:const(char)[]) (T buf, const(char)[] fldname, uint fidx=0) pure nothrow @trusted @nogc {
  static if (!is(T == typeof(null))) {
    if (buf.length == 0) return null;
    fldname = fldname.xstrip;
    while (fldname.length && (fldname[$-1] == ':' || fldname[$-1] <= ' ')) fldname = fldname[0..$-1];
    if (fldname.length == 0) return null;
    usize lpos = 0;
    while (lpos < buf.length) {
      if (isEmptyLine(buf, lpos)) return null;
      usize nlpos = skipOneLine(buf, lpos);
      auto hl = buf[lpos..nlpos];
      if (!hl.startsWithCI(fldname)) { lpos = nlpos; continue; }
      //{ import std.stdio; writeln("hl=<", hl.xstripright, "> : <", fldname, ">"); }
      hl = hl[fldname.length..$].xstrip;
      if (hl.length == 0 || hl.ptr[0] != ':') { lpos = nlpos; continue; }
      // i found her!
      if (fidx) { --fidx; lpos = nlpos; continue; }
      // collect continuations
      while (nlpos < buf.length && buf.ptr[nlpos] <= ' ') nlpos = skipOneLine(buf, nlpos);
      hl = buf[lpos..nlpos];
      // skip field name
      while (hl.length && hl.ptr[0] != ':') hl = hl[1..$];
      if (hl.length) hl = hl[1..$]; // skip ':'
      hl = hl.xstrip;
      if (hl.length == 0) { lpos = nlpos; continue; } // skip empty fields (because why not)
      return hl;
    }
  }
  return null;
}


// ////////////////////////////////////////////////////////////////////////// //
// stops at `termch`
// returned position is always valid for slicing
private usize skipWord (T:const(char)[]) (T buf, usize pos, char termch) pure nothrow @trusted @nogc {
  static if (is(T == typeof(null))) {
    return 0;
  } else {
    if (pos >= buf.length) return buf.length;
    bool inq = false;
    while (pos < buf.length) {
      immutable char ch = buf.ptr[pos++];
      if (inq) {
        if (ch == '"') inq = false;
      } else {
             if (ch == '"') inq = true;
        else if (ch == termch) return pos-1;
      }
    }
    return buf.length;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
private T strUnquote (T:const(char)[]) (T buf) pure nothrow @trusted @nogc {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    buf = buf.xstrip;
    if (buf.length >= 2) {
           if (buf.ptr[0] == '"' && buf[$-1] == '"') buf = buf[1..$-1];
      else if (buf.ptr[0] == '<' && buf[$-1] == '>') buf = buf[1..$-1];
    }
    return buf;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// removes double quotes, or "<>" quotes
public T getFieldValue (T:const(char)[]) (T buf) pure nothrow @trusted @nogc {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    return strUnquote(buf);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// removes double quotes, or "<>" quotes
public T getNextFieldValue (T:const(char)[]) (ref T buf) pure nothrow @trusted @nogc {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    buf = buf.xstrip;
    if (buf.length == 0) return null;
    if (buf.ptr[0] == '<') {
      usize pos = 1;
      while (pos < buf.length && buf.ptr[pos] != '>') ++pos;
      T res = buf[1..pos];
      if (pos < buf.length && buf.ptr[pos] == '>') ++pos;
      buf = buf[pos..$].xstrip;
      return res;
    } else if (buf.ptr[0] == '"') {
      usize pos = 1;
      while (pos < buf.length && buf.ptr[pos] != '"') ++pos;
      T res = buf[1..pos];
      if (pos < buf.length && buf.ptr[pos] == '"') ++pos;
      buf = buf[pos..$].xstrip;
      return res;
    } else {
      usize pos = 1;
      while (pos < buf.length && buf.ptr[pos] > 32) ++pos;
      T res = buf[0..pos];
      buf = buf[pos..$].xstrip;
      return res;
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// get next word until ";"
// eats ";"
// skips empty ";" (this is not standard, because it can skip the first empty token)
// returns empty slice when there are no more words
public T getFieldParams (T:const(char)[]) (ref T buf) pure nothrow @trusted @nogc {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    while (buf.length && (buf.ptr[0] <= 32 || buf.ptr[0] == ';')) buf = buf[1..$];
    if (buf.length == 0) return null;
    immutable usize end = skipWord(buf, 0, ';');
    // it is guaranteed that we have at least one non-space char here
    T res = buf[0..end].xstripright;
    buf = buf[end..$];
    while (buf.length && (buf.ptr[0] <= 32 || buf.ptr[0] == ';')) buf = buf[1..$];
    return res;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// returns name part of `getFieldParams()` result
// removes double quotes, or "<>" quotes
public T getParamName (T:const(char)[]) (T buf) pure nothrow @trusted @nogc {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    while (buf.length && buf.ptr[0] <= 32) buf = buf[1..$];
    if (buf.length == 0) return null;
    immutable usize end = skipWord(buf, 0, '=');
    return buf[0..end].strUnquote;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// returns value part of `getFieldParams()` result
// removes double quotes, or "<>" quotes
public T getParamValue (T:const(char)[]) (T buf) pure nothrow @trusted @nogc {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    while (buf.length && buf.ptr[0] <= 32) buf = buf[1..$];
    if (buf.length == 0) return null;
    usize start = skipWord(buf, 0, '=');
    if (start >= buf.length) return null;
    ++start;
    return buf[start..$].xstrip.strUnquote;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// returns starting position of the found boundary
// if no boundary was found, returns `buf.length`,
public usize findBoundary (T:const(char)[]) (T buf, usize stpos, const(char)[] boundary, out bool last) pure nothrow @trusted @nogc {
  static if (is(T == typeof(null))) {
    last = true;
    return null;
  } else {
    if (boundary.length == 0 || stpos >= buf.length) { last = true; return buf.length; }
    last = false;
    usize pos = stpos;
    // just in case, find line beginning
    while (pos > 0 && buf.ptr[pos-1] != '\n') --pos;
    while (pos < buf.length) {
      immutable usize bpos = pos;
      pos = skipOneLine(buf, pos);
      if (pos-bpos < boundary.length+2) continue;
      if (buf.ptr[bpos] != '-' || buf.ptr[bpos+1] != '-') continue;
      if (buf[bpos+2..bpos+2+boundary.length] != boundary) continue;
      usize epos = bpos+2+boundary.length;
      if (epos >= buf.length) return bpos;
      if (buf.ptr[epos] == '\n') return bpos;
      if (buf.ptr[epos] == '\r' && (epos+1 >= buf.length || buf.ptr[epos+1] == '\n')) return bpos;
      if (buf.ptr[epos] == '-' && epos+1 < buf.length && buf.ptr[epos+1] == '-') {
        epos += 2;
        last = true;
        if (epos >= buf.length) return bpos;
        if (buf.ptr[epos] == '\n') return bpos;
        if (buf.ptr[epos] == '\r' && (epos+1 >= buf.length || buf.ptr[epos+1] == '\n')) return bpos;
        last = false;
      }
    }
    last = true;
    return buf.length;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public struct Content {
  DynStr mime; // always lowercased
  DynStr name; // for attachments; `null` for normal parts
  DynStr format;
  DynStr data; // properly decoded
}


// ////////////////////////////////////////////////////////////////////////// //
public void parseContent (ref Content[] content, const(char)[] hdrs, const(char)[] body, bool noattaches=false) {
  const(char)[] enc = findHeaderField(hdrs, "Content-Transfer-Encoding").getFieldValue;
  if (enc.length == 0) enc = "8bit";

  // parse content type
  DynStr name;
  DynStr boundary;
  DynStr format = "";
  DynStr charset;
  DynStr mime = "text/plain";
  auto ctype = findHeaderField(hdrs, "Content-Type");
  if (ctype.length) {
    // mime type
    mime = getFieldParams(ctype).getFieldValue;
    mime.lowerInPlace();
         if (mime.length == 0) mime = "text/plain";
    else if (mime == "text" || mime == "text/") mime = "text/plain";
    // additional fields
    while (ctype.length) {
      auto kv = getFieldParams(ctype);
      if (kv.length == 0) continue;
      auto n = getParamName(kv);
      auto v = getParamValue(kv);
      // charset
      if (n.strEquCI("charset")) {
        if (charset.length != 0) continue;
        v = v.xstrip;
        if (v.length != 0) {
          charset = v;
          charset.lowerInPlace();
        }
        continue;
      }
      // format
      if (n.strEquCI("format")) {
        if (format.length != 0) continue;
        v = v.xstrip;
        if (v.length != 0) {
          format = v;
          format.lowerInPlace();
        }
        continue;
      }
      // name
      if (n.strEquCI("name")) {
        if (name.length != 0) continue;
        v = v.sanitizeFileNameStr;
        if (v.length != 0) name = v;
        continue;
      }
      // boundary
      if (n.strEquCI("boundary")) {
        if (boundary.length != 0) continue;
        if (v.length != 0) boundary = v;
        continue;
      }
    }
    if (mime == "text/richtext" || mime == "text/enriched") {
      mime = "text/plain";
      format = "enriched";
    } else if (mime.startsWith("text/html") || mime.startsWith("text/xhtml")) {
      mime = "text/html";
      format.clear();
    } else if (mime.startsWith("text/")) {
      if (format.length) {
        format ~= "; ";
        format ~= mime[5..$];
      } else {
        format = mime[5..$];
      }
      mime = "text/plain";
    }
  }
  if (charset.length == 0) charset = "us-ascii";

  bool inline = true;
  auto disp = findHeaderField(hdrs, "Content-Disposition");
  while (disp.length) {
    auto kv = getFieldParams(disp);
    if (kv.length == 0) continue;
    auto n = getParamName(kv);
    auto v = getParamValue(kv);
    if (n.strEquCI("attachment")) {
      inline = false;
      continue;
    }
    // filename?
    if (n.strEquCI("filename")) {
      v = v.sanitizeFileNameStr;
      if (v.length != 0) name = v;
      continue;
    }
  }

  /*
  writeln("--------------------------------");
  writeln("encoding: <", enc, ">");
  writeln("name    : <", name, ">");
  writeln("boundary: <", boundary, ">");
  writeln("format  : <", format, ">");
  writeln("charset : <", charset, ">");
  writeln("inline  : ", inline);
  */

  if (boundary.length == 0 || (mime != "multipart" && !mime.startsWith("multipart/"))) {
    immutable bool istext = mime.startsWith("text/");
    // not a multipart
    if (noattaches && (!istext || !inline)) return; // not a text, or not an inline text, do not want
    Content cc;
    cc.mime = mime.idup;
    cc.name = (inline && istext ? null : name.idup);
    cc.format = (format.length ? format.idup : "");
    if (istext) {
      cc.data = decodeContent(body, enc).xstripright.recodeToUtf8(charset); // it is safe to cast here
    } else {
      cc.data = decodeContent(body, enc); // it is safe to cast here
    }
    /*
    {
      static uint cnt = 0;
      import std.string : format;
      auto fo = VFile("z__%04u.bin".format(cnt++), "w");
      fo.rawWriteExact(cc.data[]);
    }
    */
    content ~= cc;
    return;
  }

  // multipart, process it recursively
  bool last;
  for (;;) {
    usize bpos = findBoundary(body, 0, boundary, out last);
    if (last) break;
    // skip it
    bpos = skipOneLine(body, bpos);
    // find next boundary
    immutable usize epos = findBoundary(body, bpos, boundary, out last);
    // get part
    const(char)[] bpart = body[bpos..epos].xstripright;
    if (bpart.length != 0) {
      //{ writeln("===[", enc, "]===[", boundary, "]=== (bpos=", bpos, "; epos=", epos, ")"); writeln(bpart); writeln("------------------"); }
      bpart = decodeContent(bpart, enc);
      // get headers
      usize hdrend = findHeadersEnd(bpart);
      hdrs = bpart[0..hdrend];
      hdrend = skipOneLine(bpart, hdrend);
      bpart = bpart[hdrend..$];
      parseContent(ref content, hdrs, bpart, noattaches);
    }
    body = body[epos..$];
  }
}
