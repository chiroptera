/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module chibackend.decode is aliced;

import iv.cmdcon;
import iv.dynstring;
import iv.encoding;
import iv.strex;
import iv.utfutil;
import iv.utfutil : utf8CodeLen, utf8Valid;


/*
// ////////////////////////////////////////////////////////////////////////// //
// `ch`: utf8 start
// -1: invalid utf8
public byte utf8CodeLen (char ch) pure nothrow @trusted @nogc {
  //pragma(inline, true);
  if (ch < 0x80) return 1;
  if ((ch&0b1111_1110) == 0b1111_1100) return 6;
  if ((ch&0b1111_1100) == 0b1111_1000) return 5;
  if ((ch&0b1111_1000) == 0b1111_0000) return 4;
  if ((ch&0b1111_0000) == 0b1110_0000) return 3;
  if ((ch&0b1110_0000) == 0b1100_0000) return 2;
  return -1; // invalid
}


// ////////////////////////////////////////////////////////////////////////// //
public bool utf8Valid (const(void)[] buf) pure nothrow @trusted @nogc {
  const(ubyte)* bp = cast(const(ubyte)*)buf.ptr;
  auto left = buf.length;
  while (left--) {
    auto len = utf8CodeLen(*bp++)-1;
    if (len < 0 || len > left) return false;
    left -= len;
    while (len-- > 0) if (((*bp++)&0b1100_0000) != 0b1000_0000) return false;
  }
  return true;
}
*/


// ////////////////////////////////////////////////////////////////////////// //
public bool isValidNickUniChar (immutable dchar ch) pure nothrow @safe @nogc {
  pragma(inline, true);
  return
    (ch >= '0' && ch <= '9') ||
    (ch >= 'A' && ch <= 'Z') ||
    (ch >= 'a' && ch <= 'z') ||
    ch == '-' || ch == '_' || ch == '.' ||
    isValidCyrillicUni(ch);
}


public bool isValidUTFNick (const(char)[] s) nothrow @safe @nogc {
  if (s.length == 0) return false;
  Utf8DecoderFast dc;
  foreach (immutable char ch; s) {
    dc.decode(cast(ubyte)ch);
    if (dc.invalid) return false;
    if (dc.complete && !isValidNickUniChar(dc.codepoint)) return false;
  }
  return true;
}


// ////////////////////////////////////////////////////////////////////////// //
public bool isGoodCtlChar (immutable char ch) pure nothrow @safe @nogc {
  pragma(inline, true);
  return (ch == '\t' || ch == '\n');
}


// ////////////////////////////////////////////////////////////////////////// //
public bool isGoodText (const(char)[] buf) pure nothrow @trusted @nogc {
  foreach (immutable char ch; buf) {
    if (ch == 127 || (ch < 32 && !isGoodCtlChar(ch))) return false;
  }
  return true;
}


// ////////////////////////////////////////////////////////////////////////// //
private bool isGoodFileNameChar (immutable char ch) pure nothrow @safe @nogc {
  if (ch <= 32 || ch == 127) return false;
  if (ch >= 128) return true;
  if (ch == '/' || ch == '\\') return false;
  return true;
}


// ////////////////////////////////////////////////////////////////////////// //
// this also sanitizes it
public T toLowerStr (T:const(char)[]) (T s) nothrow @trusted {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    bool needwork = false;
    foreach (immutable char ch; s) {
      if (ch == 127 || (ch < 32 && !isGoodCtlChar(ch)) || (ch >= 'A' && ch <= 'Z')) {
        needwork = true;
        break;
      }
    }
    if (!needwork) {
      return s;
    } else {
      char[] res;
      res.reserve(s.length);
      foreach (immutable idx, char ch; s) {
             if (ch == 13) { if (idx+1 >= s.length || s.ptr[idx+1] != 10) res ~= '\n'; }
        else if (ch < 32 && !isGoodCtlChar(ch)) res ~= ' ';
        else if (ch == 127) res ~= '~';
        else if (ch >= 'A' && ch <= 'Z') res ~= ch.tolower;
        else res ~= ch;
      }
      return cast(T)res; // it is safe to cast here
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// this also sanitizes it
public T sanitizeFileNameStr (T:const(char)[]) (T s) nothrow @trusted {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    bool needwork = false;
    foreach (immutable char ch; s) if (!isGoodFileNameChar(ch)) { needwork = true; break; }
    if (!needwork) {
      return s;
    } else {
      char[] res = new char[s.length];
      res[] = s[];
      foreach (ref char ch; res) {
        if (!isGoodFileNameChar(ch)) ch = '_';
      }
      return cast(T)res; // it is safe to cast here
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public T sanitizeStr (T:const(char)[]) (T s) nothrow @trusted {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    if (isGoodText(s)) {
      return s;
    } else {
      char[] res;
      res.reserve(s.length);
      foreach (immutable idx, char ch; s) {
             if (ch == 13) { if (idx+1 >= s.length || s.ptr[idx+1] != 10) res ~= '\n'; }
        else if (ch < 32 && !isGoodCtlChar(ch)) res ~= ' ';
        else if (ch == 127) res ~= '~';
        else res ~= ch;
      }
      return cast(T)res; // it is safe to cast here
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public T sanitizeStrLine (T:const(char)[]) (T s) nothrow @trusted {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    bool found = false;
    foreach (immutable idx, char ch; s) {
      if (ch < 32 || ch == 127) { found = true; break; }
      if (ch == 32 && (idx == 0 || s.ptr[idx-1] <= 32)) { found = true; break; }
    }
    if (!found) {
      return s;
    } else {
      char[] res;
      res.reserve(s.length);
      foreach (char ch; s) {
        if (ch < 32 || ch == 127) ch = ' ';
        if (ch <= 32 && (res.length == 0 || res[$-1] <= 32)) continue;
        res ~= ch;
      }
      while (res.length && res[$-1] <= 32) res = res[0..$-1];
      return cast(T)res; // it is safe to cast here
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// for decoded subject parts
public T sanitizeStrSubjPart (T:const(char)[]) (T s) nothrow @trusted {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    bool found = false;
    foreach (immutable idx, immutable char ch; s) {
      if (ch < 32 || ch == 127 || ch == '_') { found = true; break; }
    }
    if (!found) {
      return s;
    } else {
      char[] res = new char[s.length];
      res[] = s[];
      foreach (ref char ch; res) if (ch < 32 || ch == 127 || ch == '_') ch = ' ';
      return cast(T)res; // it is safe to cast here
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// this also sanitizes it
public T binaryToUtf8 (T:const(char)[]) (T s) nothrow @trusted {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    bool found = false;
    foreach (immutable char ch; s) {
      if (ch >= 127 || (ch < 32 && !isGoodCtlChar(ch))) { found = true; break; }
    }
    if (!found) {
      return s;
    } else {
      import iv.utfutil : utf8Valid;
      if (utf8Valid(s)) return sanitizeStr(s);
      char[8] uc;
      char[] res;
      // calc length
      usize sz = s.length;
      foreach (immutable char ch; s) {
        if (ch >= 128) {
          immutable int len = utf8Encode(uc[], cast(dchar)ch);
          assert(len > 1);
          sz += cast(uint)len;
        }
      }
      res.reserve(sz);
      foreach (immutable idx, char ch; s) {
        if (ch < 128) {
               if (ch == 13) { if (idx+1 >= s.length || s.ptr[idx+1] != 10) res ~= '\n'; }
          else if (ch < 32 && !isGoodCtlChar(ch)) res ~= ' ';
          else if (ch == 127) res ~= '~';
          else res ~= ch;
        } else {
          immutable int len = utf8Encode(uc[], cast(dchar)ch);
          assert(len > 1);
          res ~= uc[0..len];
        }
      }
      return cast(T)res; // it is safe to cast here
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// this also sanitizes it
public T utf8ToUtf8 (T:const(char)[]) (T s) nothrow @trusted {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    bool found = false;
    foreach (immutable char ch; s) {
      if (ch >= 127 || (ch < 32 && !isGoodCtlChar(ch))) { found = true; break; }
    }
    if (!found) {
      return s;
    } else {
      import iv.utfutil : utf8Valid;
      if (utf8Valid(s)) return sanitizeStr(s);
      char[8] uc;
      char[] res;
      res.reserve(s.length);
      int utfleft = 0;
      foreach (immutable idx, char ch; s) {
        if (utfleft) { --utfleft; res ~= ch; continue; }
        if (ch < 128) {
               if (ch == 13) { if (idx+1 >= s.length || s.ptr[idx+1] != 10) res ~= '\n'; }
          else if (ch < 32 && !isGoodCtlChar(ch)) res ~= ' ';
          else if (ch == 127) res ~= '~';
          else res ~= ch;
        } else {
          immutable byte ulen = utf8CodeLen(ch);
          if (ulen < 1) { res ~= '?'; continue; }
          if (s.length-idx < ulen) { res ~= '?'; break; }
          if (!utf8Valid(s[idx..idx+ulen])) { res ~= '?'; continue; }
          res ~= ch;
          utfleft = ulen-1;
        }
      }
      return cast(T)res; // it is safe to cast here
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public T subjRemoveRe(T:const(char)[]) (T s) nothrow @trusted {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    for (;;) {
      s = s.xstrip;
      if (s.length < 3) break;
      if (s.ptr[0] != 'r' && s.ptr[0] != 'R') break;
      if (s.ptr[1] != 'e' && s.ptr[1] != 'E') break;
      usize pp = 2;
      while (pp < s.length && s.ptr[pp] <= 32) ++pp;
      if (pp >= s.length || s.ptr[pp] != ':') break;
      s = s[pp+1..$];
    }
    return s;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
private static immutable string b64alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

private static immutable ubyte[256] b64dc = () {
  ubyte[256] res = 0xff; // invalid
  foreach (immutable idx, immutable char ch; b64alphabet) {
    res[cast(ubyte)ch] = cast(ubyte)idx;
  }
  res['='] = 0xfe; // padding
  // ignore
  res[0..32] = 0xf0;
  res[127] = 0xf0; // just in case
  return res;
}();

public char[] decodeBase64(bool ignoreUnderscore=false) (const(void)[] datavoid, out bool error) nothrow @trusted {
  const(ubyte)[] data = cast(const(ubyte)[])datavoid;

  bool inPadding = false;
  ubyte[4] bts = void;
  uint btspos = 0;

  char[] dcx;
  dcx.reserve((data.length+3U)/4U*3U+8U);
  error = false;

  bool decodeChunk () nothrow @trusted {
    if (btspos == 0) return true;
    if (btspos == 1) return false; //throw new Base64Exception("incomplete data in base64 decoder");
    dcx ~= cast(char)((bts.ptr[0]<<2)|((bts.ptr[1]&0x30)>>4)); // 2 and more
    if (btspos > 2) dcx ~= cast(char)(((bts.ptr[1]&0x0f)<<4)|((bts.ptr[2]&0x3c)>>2)); // 3 and more
    if (btspos > 3) dcx ~= cast(char)(((bts.ptr[2]&0x03)<<6)|bts.ptr[3]);
    return true;
  }

  while (data.length) {
    immutable ubyte cb = b64dc.ptr[data.ptr[0]];
    if (cb == 0xff) { error = true; delete dcx; return "<invalid base64 data>".dup; }
    data = data[1..$];
    if (cb == 0xf0) continue; // empty
    static if (ignoreUnderscore) {
      if (cb == '_') continue;
    }
    if (cb == 0xfe) {
      // padding
      if (!inPadding) {
        if (!decodeChunk()) { error = true; delete dcx; return "<invalid base64 data>".dup; }
        inPadding = true;
      }
      if (++btspos == 4) { inPadding = false; btspos = 0; }
    } else {
      // normal
      if (inPadding) {
        if (btspos != 0) { error = true; delete dcx; return "<invalid base64 data>".dup; }
        inPadding = false;
      }
      bts.ptr[btspos++] = cb;
      if (btspos == 4) {
        if (!decodeChunk()) { error = true; delete dcx; return "<invalid base64 data>".dup; }
        btspos = 0;
      }
    }
  }
  if (btspos != 0 && !inPadding) {
    // assume that it is not padded
    if (!decodeChunk()) { error = true; delete dcx; return "<invalid base64 data>".dup; }
  }

  return dcx;
}


// ////////////////////////////////////////////////////////////////////////// //
public char[] decodeQuotedPrintable(bool multiline) (const(void)[] datavoid) nothrow @trusted {
  const(char)[] data = cast(const(char)[])datavoid;
  //{ import core.stdc.stdio; fprintf(stderr, "***<%.*s>\n", cast(uint)data.length, data.ptr); }
  char[] dcx;
  dcx.reserve(data.length);
  while (data.length) {
    if (data.ptr[0] == '=') {
      if (data.length == 1) break;
      if (data.length >= 3 && digitInBase(data.ptr[1], 16) >= 0 && digitInBase(data.ptr[2], 16) >= 0) {
        dcx ~= cast(char)(digitInBase(data.ptr[1], 16)*16+digitInBase(data.ptr[2], 16));
        data = data[3..$];
        continue;
      }
      // check if it is followed by blanks up to the newline
      // if it is so, then this is "line continuation" -- remove both '=' and blanks
      static if (multiline) {
        bool ateol = false;
        usize epos = 1; // skip '='
        while (epos < data.length) {
          char ch = data.ptr[epos++];
          if (ch == 9 || ch == 32) continue;
          if (ch == 13) {
            if (epos >= data.length) { ateol = true; break; }
            if (data.ptr[epos] == 10) continue;
            ch = 10; // trigger next check
          }
          if (ch == 10) {
            // check for most fuckin' idiots: new line started with a dot has two dots
            if (epos < data.length && data.ptr[epos] == '.' &&
                epos+1 < data.length && data.ptr[epos+1] == '.')
            {
              ++epos; // skip first dot
            }
            ateol = true;
            break;
          }
          --epos;
          break;
        }
        if (epos > data.length) epos = data.length; // just in case
        if (ateol || epos >= data.length) {
          data = data[epos..$];
          continue;
        }
      }
    } else {
      // check for most fuckin' idiots: new line started with a dot has two dots
      static if (multiline) {
        if (data.length >= 3 &&
            (data.ptr[0] == '\n' || data.ptr[0] == '\r') &&
            data.ptr[1] == '.' && data.ptr[2] == '.')
        {
          dcx ~= data.ptr[0];
          data = data[2..$];
        }
      }
    }
    dcx ~= data.ptr[0];
    data = data[1..$];
  }
  return dcx;
}


// ////////////////////////////////////////////////////////////////////////// //
public T ensureProper7Bit(T:const(char)[]) (T s) nothrow @trusted {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    bool needwork = false;
    foreach (immutable char ch; s) if (ch >= 128) { needwork = true; break; }
    if (!needwork) return s;
    char[] dcx = new char[s.length];
    dcx[] = s[];
    foreach (ref char ch; dcx) ch &= 0x7f;
    return cast(T)dcx; // it is safe to cast here
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// decode things like "=?UTF-8?B?Tm9yZGzDtnc=?="
public T decodeSubj(T:const(char)[]) (T s) nothrow @trusted {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    if (s.indexOf("=?") < 0) return s.sanitizeStrLine.utf8ToUtf8;

    // have to do some work
    auto origs = s;
    char[] res;
    res.reserve(s.length); // at least

    while (s.length > 2) {
      auto stqpos = s.indexOf("=?");
      if (stqpos < 0) break;
      if (stqpos > 0) res ~= s[0..stqpos].utf8ToUtf8;
      s = s[stqpos+2..$];

      auto eepos = s.indexOf('?');
      if (eepos < 0) break;
      auto enc = s[0..eepos];

      //conwriteln("ENCODING: '", enc, "'");
      s = s[eepos+1..$];
      if (enc.length == 0) enc = "utf-8";
      if (s.length < 2 || s.ptr[1] != '?') return origs.sanitizeStrLine.utf8ToUtf8;

      char ect = s.ptr[0];
      s = s[2..$];
      eepos = s.indexOf("?=");
      if (eepos < 0) return origs.sanitizeStrLine.utf8ToUtf8;

      auto part = s[0..eepos];
      s = s[eepos+2..$];

      // several encoded parts may be separated with spaces; those spaces should be ignored
      stqpos = 0;
      while (stqpos < s.length && s.ptr[stqpos] <= ' ') ++stqpos;
      if (s.length-stqpos >= 2 && s.ptr[stqpos] == '=' && s.ptr[stqpos+1] == '?') s = s[stqpos..$];

      // decode part
      if (ect == 'Q' || ect == 'q') {
        // quoted printable
        part = cast(T)decodeQuotedPrintable!false(part); // it is safe to cast here
      } else if (ect == 'B' || ect == 'b') {
        // base64
        //auto xpart = part;
        bool error = false;
        part = cast(T)decodeBase64!true(part, out error); // it is safe to cast here
        if (error) {
          //conwriteln("CANNOT DECODE B64: ", xpart);
          delete part;
          return origs.sanitizeStrLine.utf8ToUtf8;
        }
      }

      // reencode part if necessary
      if (!enc.strEquCI("utf-8") && !enc.strEquCI("utf8") && !enc.strEquCI("US-ASCII")) {
        try {
          //conwriteln("RECODING: ", enc);
          part = recode(part, "utf-8", enc);
        } catch (Exception e) {
          //conwriteln("RECODE ERROR: ", e.msg);
          return origs.sanitizeStrLine.utf8ToUtf8;
        }
      }

      part = part.sanitizeStrSubjPart.utf8ToUtf8;
      if (part.length) res ~= part;
    }

    if (s.length) res ~= s.utf8ToUtf8;
    return cast(T)res.sanitizeStrLine; // it should be valid utf8 here; also, it is safe to cast here
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// decode content with the given encoding type
public T decodeContent(T:const(char)[]) (T data, const(char)[] encoding) nothrow @trusted {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    if (data.length == 0 || encoding.length == 0 || encoding.strEquCI("8bit") || encoding.strEquCI("binary")) {
      return data;
    }

    if (encoding.strEquCI("7bit")) {
      return cast(T)ensureProper7Bit(data); // it is safe to cast here
    }

    if (encoding.strEquCI("base64")) {
      bool error;
      return cast(T)decodeBase64(data, out error); // it is safe to cast here
    }

    if (encoding.strEquCI("quoted-printable")) {
      return cast(T)decodeQuotedPrintable!true(data); // it is safe to cast here
    }

    if (encoding.length != 0) {
      char[] res = "<invalid encoding:".dup;
      res ~= encoding;
      res ~= ">";
      return cast(T)res; // it is safe to cast here
    }

    return data;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public T recodeToUtf8(T:const(char)[]) (T data, const(char)[] charset) nothrow @trusted {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    if (data.length == 0) return data;
    bool found = false;
    foreach (immutable char ch; data) if (ch >= 128) { found = true; break; }
    if (!found) return sanitizeStr(data);
    if (charset.length == 0 || charset.strEquCI("utf-8") || charset.strEquCI("utf8") || charset.strEquCI("US-ASCII")) {
      return utf8ToUtf8(data);
    }
    try {
      data = recode(data, "utf-8", charset);
      if (data.length == 0) return data;
      return data.sanitizeStr;
    } catch (Exception e) {}
    char[] res = "<cannot decode '".dup;
    foreach (char ch; charset) {
      if (ch <= 32 || ch >= 127) continue;
      res ~= ch;
    }
    res ~= "'>";
    return cast(T)res; // it is safe to cast here
  }
}


// ////////////////////////////////////////////////////////////////////////// //
private T mailNameUnquote (T:const(char)[]) (T buf) pure nothrow @trusted @nogc {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    buf = buf.xstrip;
    if (buf.length >= 2) {
      if ((buf.ptr[0] == '"' && buf[$-1] == '"') ||
          (buf.ptr[0] == '<' && buf[$-1] == '>') ||
          (buf.ptr[0] == '`' && buf[$-1] == '\'') ||
          (buf.ptr[0] == '\'' && buf[$-1] == '\''))
      {
        buf = buf[1..$-1].xstrip;
      }
    }
    return buf;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// extract email from decoded "From" and "To" fields
public T extractMail(bool doSanitize=true, T:const(char)[]) (T data) nothrow @trusted {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    if (data.length == 0) return data;
    if (data[$-1] == '>') {
      usize pos = data.length;
      while (pos > 0 && data.ptr[pos-1] != '<') --pos;
      data = data[pos..$-1].xstrip;
    } else {
      data = data.xstrip;
    }
    static if (doSanitize) {
      // hack for idiotic LJ (those morons are breaking all possible standards)
      auto sppos = data.indexOf(' ');
      if (sppos > 0) data = data[0..sppos];
    }
    return data.toLowerStr;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// strip email from decoded "From" and "To" fields
public T stripMail(T:const(char)[]) (T data) nothrow @trusted {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    if (data.length == 0) return data;
    if (data[$-1] == '>') {
      usize pos = data.length;
      while (pos > 0 && data.ptr[pos-1] != '<') --pos;
      if (pos == 0) return data[0..0];
      return data[0..pos-1].xstrip;
    }
    return data[0..0];
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// extract name from decoded "From" and "To" fields
// can construct name if there is none
// special hack for idiotic LJ
public T extractName(T:const(char)[]) (T data) nothrow @trusted {
  static if (is(T == typeof(null))) {
    return null;
  } else {
    if (data.length == 0) return data;
    auto origData = data;
    T mail = extractMail(data);
    data = stripMail(data).decodeSubj.xstrip;
    // hack for idiotic LJ (those morons are breaking all possible standards)
    if (mail.startsWith("lj_dontreply@lj.rossia.org")) {
      auto dd = extractMail!false(origData);
      auto spos = dd.indexOf(" (");
      if (spos >= 0) {
        dd = dd[spos+2..$-(dd[$-1] == ')' ? 1 : 0)].xstrip;
        if (dd == "LJR Comment") {
          dd = "anonymous";
        } else if (dd.endsWith(" - LJR Comment")) {
          auto dpos = dd.lastIndexOf('-');
          dd = dd[0..dpos].xstrip;
          if (dd.length == 0) dd = "anonymous";
        }
        dd = dd.mailNameUnquote;
        if (dd.length) return dd;
      }
    }
    data = data.mailNameUnquote;
    if (data.length) {
      if (mail.startsWith("lj-notify@livejournal.com")) {
        if (data == "LJ Comment") {
          data = "anonymous";
        } else if (data.endsWith(" - LJ Comment")) {
          auto dpos = data.lastIndexOf('-');
          data = data[0..dpos].xstrip;
          if (data.length == 0) data = "anonymous";
        }
      }
      return data;
    }
    // construct name from the mail
    auto npos = mail.indexOf('@');
    if (npos <= 0) return mail;
    data = mail[0..npos].xstrip;
    if (data.length == 0) return mail;
    char[] res;
    res.reserve(data.length);
    foreach (char ch; data) {
      if (ch <= 32 || ch == '.' || ch == '-' || ch == '_') ch = 32;
      if (ch == 32) {
        if (res.length && res[$-1] != 32) res ~= ch;
      } else {
        if (res.length == 0 || res[$-1] == 32) ch = ch.toupper; else ch = ch.tolower;
        res ~= ch;
      }
    }
    res = res.xstrip;
    if (res.length == 0) return mail;
    return cast(T)res; // it is safe to cast here
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// encode string if it contains some non-ascii
// always returns new string, which is safe to `delete`
// passed string must be in UTF-8
// can return `null` for empty string
public dynstring strEncodeQ (const(char)[] s) nothrow @trusted {
  static bool isSpecial (immutable char ch) pure nothrow @safe @nogc {
    return
      ch < ' ' ||
      ch >= 127 ||
      ch == '\'' ||
      ch == '`' ||
      ch == '"' ||
      ch == '\\' ||
      ch == '@';
  }
  dynstring res;
  if (s.length == 0) return res;
  static immutable string hexd = "0123456789abcdef";
  bool needWork = (s[0] == '=' || s[0] == '?');
  if (!needWork) foreach (char ch; s) if (isSpecial(ch)) { needWork = true; break; }
  if (!needWork) {
    res = s;
  } else {
    res.reserve(s.length*3+32);
    res ~= "=?UTF-8?Q?"; // quoted printable
    foreach (char ch; s) {
      if (ch <= ' ') ch = '_';
      if (!isSpecial(ch) && ch != '=' && ch != '?') {
        res ~= ch;
      } else {
        res ~= "=";
        res ~= hexd[(cast(ubyte)ch)>>4];
        res ~= hexd[(cast(ubyte)ch)&0x0f];
      }
    }
    res ~= "?=";
  }
  return res;
}
