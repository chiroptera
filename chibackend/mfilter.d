/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module chibackend.mfilter is aliced;

import iv.cmdcon;
import iv.strex;
import chibackend : DynStr;


// ////////////////////////////////////////////////////////////////////////// //
mixin(NewExceptionClass!("FilterSyntaxException", "Exception"));


// ////////////////////////////////////////////////////////////////////////// //
class FilterHelper {
  enum Action {
    Nothing, // do nothing
    Delete, // delete message
    SoftDelete, // "soft-delete" message (mark as read, strike through, but don't remove)
    Spam, // mark message as spam
    Ham, // mark message as ham
    Read, // mark message as read
    Stop, // do not process other filters
    Move, // <tagname> -- move message to the specified tag
    Exec, // <command>
  }

  // called if a filter was matched
  abstract void filterMatched ();

  abstract DynStr getAccount ();

  abstract DynStr getHeaderField (const(char)[] header, out bool exists);

  abstract DynStr getFromName ();
  abstract DynStr getFromMail ();
  abstract DynStr getToName ();
  abstract DynStr getToMail ();
  abstract DynStr getSubj (out bool exists);

  // returns first string from stdout
  // it should be action name
  abstract DynStr exec (const(char)[] cmd);

  abstract void move (const(char)[] dest);

  // won't be called for `Nothing`, `Move`, `Exec` and `Stop`
  abstract void performAction (Action action);

  abstract bool match (const(char)[] pat, const(char)[] str, bool casesens);

  static Action parseActionName (const(char)[] tok, out bool unknown) pure nothrow @trusted @nogc {
    unknown = false;
    tok = tok.xstrip;
    if (tok.strEquCI("nothing") || tok.strEquCI("noop") || tok.strEquCI("nop")) return FilterHelper.Action.Nothing;
    if (tok.strEquCI("delete")) return FilterHelper.Action.Delete;
    if (tok.strEquCI("softdelete") || tok.strEquCI("soft_delete") || tok.strEquCI("soft-delete")) return FilterHelper.Action.SoftDelete;
    if (tok.strEquCI("spam")) return FilterHelper.Action.Spam;
    if (tok.strEquCI("ham")) return FilterHelper.Action.Ham;
    if (tok.strEquCI("read")) return FilterHelper.Action.Read;
    if (tok.strEquCI("stop")) return FilterHelper.Action.Stop;
    if (tok.strEquCI("move")) return FilterHelper.Action.Move;
    if (tok.strEquCI("exec")) return FilterHelper.Action.Exec;
    unknown = true;
    return FilterHelper.Action.Nothing;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// filter "xx"
// (frommail matchcase "abc@def" OR from-mail matchcase "gex@boo") AND from-name match "zoo" move "0rare/doo" ham stop
// returns `false` if no more filters should be processed ("stop" action)
// pass `null` as `hlp` to perform syntax check only
public bool executeMailFilter (const(char)[] filter, FilterHelper hlp) {
  char[256] temptoken = void;
  const(char)[] tok;
  auto anchor = filter;

  // returns false if there are no more tokens
  bool getToken () {
    tok = null;
    while (filter.length && filter[0] <= ' ') filter = filter[1..$];
    if (filter.length == 0) return false;
    char ch = filter[0];
    if (ch == '#') return false;
    if (ch == '"' || ch == '\'') {
      // quoted string
      immutable char qch = ch;
      filter = filter[1..$];
      usize pos = 0;
      while (filter.length) {
        ch = filter[0];
        filter = filter[1..$];
        if (ch != '\\') {
          if (ch == qch) break;
        } else {
          // escape
          if (filter.length == 0) throw new FilterSyntaxException("unfinished escape");
          ch = filter[0];
          filter = filter[1..$];
          if (ch == 'x' || ch == 'X' || ch == 'u' || ch == 'U') throw new FilterSyntaxException("charcode escapes are not supported");
        }
        if (pos >= temptoken.length) throw new FilterSyntaxException("quoted string too long");
        temptoken[pos++] = ch;
      }
      tok = temptoken[0..pos];
    } else if ("()[]!".indexOf(filter[0]) >= 0) {
      // delimiter
      tok = filter[0..1];
      filter = filter[1..$];
    } else if (filter[0] == '&') {
      tok = "&&";
      filter = filter[(filter.length > 1 && filter[1] == '&' ? 2 : 1)..$];
    } else if (filter[0] == '|') {
      tok = "||";
      filter = filter[(filter.length > 1 && filter[1] == '|' ? 2 : 1)..$];
    } else if (filter[0] == '-' && (filter.startsWith("-->") || filter.startsWith("->"))) {
      // special delimiter
      tok = "-->";
      filter = filter[(filter[1] == '>' ? 2 : 3)..$];
    } else {
      // just a token until a space or a special char
      usize pos = 1;
      while (pos < filter.length) {
        ch = filter[pos];
        if (ch <= 32 || `"'()[]!&|`.indexOf(ch) >= 0) break;
        if (ch == '-') {
          if (filter[pos..$].startsWith("->") || filter[pos..$].startsWith("-->")) break;
        }
        ++pos;
      }
      tok = filter[0..pos];
      filter = filter[pos..$];
    }
    //{ import iv.vfs.io; writeln("TOKEN: <", tok, ">"); }
    return true;
  }

  bool delegate (bool doskip) parseExpr;

  // parse expression
  // `tok` must be valid
  /*
    <simpleexpr> is:
      from-name <checker>
      from-mail <checker>
      from <checker>
      to-name <checker>
      to-mail <checker>
      to <checker>
      subj <checker>
      header <checker>

    <checker> is:
      [not] {match|matchcase} <pattern>
      [not] exists -- because checking for empty pattern is not enough ;-)
  */
  bool parseExprSimple (bool doskip) {
    if (tok is null) throw new FilterSyntaxException("unexpected end of expression");
    bool res = true;

    // subexpression?
    if (tok == "(") {
      getToken();
      res = parseExpr(doskip);
      if (tok != ")") throw new FilterSyntaxException("missing closing paren");
      getToken();
      return res;
    }

    // simple matcher
    enum Cmd {
      FromName,
      FromMail,
      From,
      ToName,
      ToMail,
      To,
      Subj,
      Account,
      Header,
    }
    Cmd cmd = Cmd.Header;
         if (tok.strEquCI("fromname") || tok.strEquCI("from-name") || tok.strEquCI("from_name")) cmd = Cmd.FromName;
    else if (tok.strEquCI("frommail") || tok.strEquCI("from-mail") || tok.strEquCI("from_mail")) cmd = Cmd.FromMail;
    else if (tok.strEquCI("toname") || tok.strEquCI("to-name") || tok.strEquCI("to_name")) cmd = Cmd.ToName;
    else if (tok.strEquCI("tomail") || tok.strEquCI("to-mail") || tok.strEquCI("to_mail")) cmd = Cmd.ToMail;
    else if (tok.strEquCI("from")) cmd = Cmd.From;
    else if (tok.strEquCI("to")) cmd = Cmd.To;
    else if (tok.strEquCI("subj") || tok.strEquCI("subject")) cmd = Cmd.Subj;
    else if (tok.strEquCI("account")) cmd = Cmd.Account;
    else if (tok.strEquCI("header")) cmd = Cmd.Header;
    else throw new FilterSyntaxException("unknown filter matcher \""~tok.idup~"\"");

    DynStr fldname;
    if (cmd == Cmd.Header) {
      if (!getToken()) throw new FilterSyntaxException("unexpected end of expression");
      if (tok.length == 0) throw new FilterSyntaxException("empty field name for \"header\"");
      fldname = tok;
    }

    if (!getToken()) throw new FilterSyntaxException("\"match\", \"matchcase\" or \"exists\" expected");

    bool inverse = false;
    if (tok.strEquCI("not")) {
      inverse = true;
      if (!getToken()) throw new FilterSyntaxException("\"match\", \"matchcase\" or \"exists\" expected");
    }

    enum Matcher {
      Match,
      MatchCase,
      Exists,
    }
    Matcher mt = Matcher.Match;
         if (tok.strEquCI("match")) mt = Matcher.Match;
    else if (tok.strEquCI("matchcase") || tok.strEquCI("match_case") || tok.strEquCI("match-case")) mt = Matcher.MatchCase;
    else if (tok.strEquCI("exists")) mt = Matcher.Exists;
    else throw new FilterSyntaxException("\"match\", \"matchcase\" or \"exists\" expected");

    if (mt != Matcher.Exists) {
      if (!getToken()) throw new FilterSyntaxException("pattern expected");
    } else {
      getToken(); // no args
    }

    if (!doskip && hlp !is null) {
      bool exists;
      DynStr val;
      final switch (cmd) {
        case Cmd.FromName: val = hlp.getFromName(); exists = true; break;
        case Cmd.FromMail: val = hlp.getFromMail(); exists = true; break;
        case Cmd.ToName: val = hlp.getToName(); exists = true; break;
        case Cmd.ToMail: val = hlp.getToMail(); exists = true; break;
        case Cmd.From: val = hlp.getHeaderField("From", out exists); break;
        case Cmd.To: val = hlp.getHeaderField("To", out exists); break;
        case Cmd.Subj: val = hlp.getSubj(out exists); break;
        case Cmd.Account: val = hlp.getAccount(); exists = (val.length() != 0); break;
        case Cmd.Header: val = hlp.getHeaderField(fldname, out exists); break;
      }
      if (mt == Matcher.Exists) {
        res = exists;
      } else {
        res = hlp.match(tok, val, (mt == Matcher.MatchCase));
      }
      if (inverse) res = !res;
    }

    getToken(); // skip pattern
    return res;
  }

  bool parseExpression (bool doskip) {
    bool res = parseExprSimple(doskip);
    //writeln("!!!");
    while (tok !is null) {
      //writeln("*** <", tok, ">");
      char cond = 0;
           if (tok.strEquCI("or") || tok == "||") cond = '|';
      else if (tok.strEquCI("and") || tok == "&&") cond = '&';
      else break;
      if (!doskip) {
        final switch (cond) {
          case '|': if (res) doskip = true; break;
          case '&': if (!res) doskip = true; break;
        }
      }
      if (!getToken()) throw new FilterSyntaxException("expression expected");
      immutable bool rval = parseExprSimple(doskip);
      if (!doskip) {
        final switch (cond) {
          case '|': res = (res || rval); if (res) doskip = true; break;
          case '&': res = (res && rval); if (!res) doskip = true; break;
        }
      }
    }
    return res;
  }

  parseExpr = &parseExpression;

  if (!getToken()) return true;
  immutable bool match = parseExpr(false);

  if (match && hlp !is null) hlp.filterMatched();

  // actions
  /*
    move <tagname> -- move message to the specified tag
    delete -- delete message
    softdelete -- "soft-delete" message (mark as read, strike through, but don't remove)
    spam -- mark message as spam
    ham -- mark message as ham
    read -- mark message as read
    exec <command> -- see below
    stop -- do not process other filters
  */
  bool res = true;
  while (tok !is null) {
    if (tok == "-->" || tok.strEquCI("do") || tok.strEquCI("perform") || tok.strEquCI("and") || tok == "&&") {
      getToken();
      if (tok is null) throw new FilterSyntaxException("perform what?");
    }
    bool unknown;
    FilterHelper.Action act = FilterHelper.parseActionName(tok, out unknown);
    if (unknown) throw new FilterSyntaxException("unknown filter action \""~tok.idup~"\"");
    getToken(); // skip action name
  doaction:
    if (act == FilterHelper.Action.Stop) {
      if (match) {
        res = false;
        if (hlp !is null) hlp.performAction(act);
      }
      continue;
    }
    if (act == FilterHelper.Action.Nothing) continue;
    if (act == FilterHelper.Action.Exec) {
      if (tok is null) throw new FilterSyntaxException("unknown filter action \"exec\" expects one argument");
      if (match) {
        DynStr eres = (hlp !is null ? hlp.exec(tok).xstrip : "nothing");
        const(char)[] execres = eres.getData;
        //TODO: process "move" here!
        immutable bool doStop = (execres.length && execres[0] == '-');
        if (doStop) {
          execres = execres[1..$];
          res = false;
          if (hlp !is null) hlp.performAction(act);
        }
        if (execres.startsWithCI("move")) {
          execres = execres[4..$];
          if (execres.length == 0 || execres[0] > 32) throw new FilterSyntaxException("filter action \"exec\" tried to move the message to the empty tag");
          execres = execres.xstrip;
          if (execres.length == 0) throw new FilterSyntaxException("filter action \"exec\" tried to move the message to the blank tag");
          //throw new FilterSyntaxException("filter action \"exec\" cannot move messages yet");
          tok = execres.dup; // need to copy due to DynStr
          // do not skip arguments here, "move" handler will do it for us
          goto domove;
        }
        getToken(); // skip arguments
        act = FilterHelper.parseActionName(execres, out unknown);
        if (act == FilterHelper.Action.Exec || execres.startsWithCI("exec")) throw new FilterSyntaxException("filter action \"exec\" cannot perform recursive execs");
        goto doaction;
      } else {
        getToken(); // skip arguments
        continue;
      }
    }
    if (act == FilterHelper.Action.Move) {
     domove:
      if (tok is null) throw new FilterSyntaxException("unknown filter action \"move\" expects one argument");
      // normalize tag
      tok = tok.xstrip;
      while (tok.length && tok[$-1] == '/') tok = tok[0..$-1].xstrip;
      if (tok.length == 0) throw new FilterSyntaxException("\"move\" expects non-empty tag name");
      if (tok.indexOf("//") >= 0) throw new FilterSyntaxException("\"move\" tag name cannot contain \"//\"");
      if ((tok[0].isalnum || tok[0] >= 128) && tok.indexOf(':') < 0) {
        if (match && hlp) {
          // fix name
          DynStr tn = "/";
          tn ~= tok;
          hlp.move(tn.getData);
        }
      } else {
        if (match && hlp !is null) hlp.move(tok);
      }
      getToken(); // skip arguments
      continue;
    }
    if (match && hlp !is null) hlp.performAction(act);
  }
  return res;
}
