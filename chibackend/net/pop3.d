/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module chibackend.net.pop3 /*is aliced*/;

import std.socket;

import iv.alice;
import iv.base64;
import iv.cmdcon;
import iv.dynstring;
import iv.sslsocket;

import chibackend : DynStr;
import chibackend.net.linesocket;

/*
usage:

receiving
=========

  conwriteln("*** [", name, "]: connecting...");
  auto pop3 = new SocketPOP3(server);
  scope(exit) pop3.close();
  conwriteln("[", name, "]: authenticating...");
  pop3.auth(user, pass);
  auto newmsg = pop3.getNewMailCount;
  if (newmsg == 0) {
    conwriteln("[", name, "]: no new messages");
    return;
  }
  conwriteln("[", name, "]: ", newmsg, " new message", (newmsg > 1 ? "s" : ""));
  foreach (immutable int popidx; 1..newmsg+1) {
    auto msg = pop3.getMessage(popidx); // full message, with the ending dot
    //auto msg = pop3.getMessage!true(popidx); // full message, with the ending dot, and exact terminators
    // process
    pop3.deleteMessage(popidx);
  }


sending
========
  string from = art.frommail;
  if (from.length == 0) { conwriteln("SMTP ERROR: no FROM!"); return false; }

  auto to = extractToMail(art);
  if (to.length == 0) { conwriteln("SMTP ERROR: no TO!"); return false; }

  SocketSMTP nsk;
  try {
    nsk = new SocketSMTP(sendserver);
  } catch (Exception e) {
    conwriteln("[", name, "]: connection error: ", e.msg);
    return false;
  }
  scope(exit) nsk.close();

  try {
    if (!smtpNoAuth) nsk.auth(email, user, pass);
    nsk.sendMessage(from, to, art.getTextToSend);
  } catch (Exception e) {
    conwriteln("[", name, "]: sending error: ", e.msg);
    return false;
  }
*/


// ////////////////////////////////////////////////////////////////////////// //
public final class SocketPOP3 : SocketLine {
public:
  this (string server, bool debugdump=false) {
    super(server, plainport:110, tlsport:995, debugdump:debugdump);
    scope(failure) abort(doshutdown:false);
    auto hello = readLine();
    if (!isOK(hello)) throw new Exception("invalid hello (no ok)"); // ("~hello.idup~")");
  }

  override void quit () {
    if (!active) return;
    doSend("QUIT");
    auto ln = readLine();
  }

  void auth (const(char)[] uname, const(char)[] upass) {
    doSendCmdArg("USER", uname);
    auto ln = readLine();
    if (!isOK(ln)) throw new Exception("cannot auth");
    doSendCmdArg("PASS", upass);
    ln = readLine();
    if (!isOK(ln)) throw new Exception("cannot auth");
  }

  int getNewMailCount () {
    doSend("STAT");
    auto ln = readLine();
    if (!isOK(ln)) throw new Exception("cannot stat");
    getStrToken(ln);
    return getToken!int(ln);
  }

  // returns full unmodified message, with the ending dot line
  // if `exact` is true, returns line with exact terminators
  // can return truncated message if it is too big
  DynStr getMessage(bool exact=false) (int idx, bool useRETR=true) {
    if (idx < 1) throw new Exception("invalid message index");
    // "RETR" seems to delete messages on some servers so there is a way to use "TOP".
    // yet the message may contain more lines than we specified in "TOP", therefore
    // default choice is "RETR". it didn't caused problems so far, but who knows...
    if (useRETR) {
      doSendCmdArg("RETR", idx);
    } else {
      import core.stdc.stdio : snprintf;
      char[128] buf = void;
      auto len = snprintf(buf.ptr, buf.length, "TOP %d 65534", idx);
      doSendCmdArg(buf[0..len]);
    }
    auto ln = readLine();
    if (!isOK(ln)) throw new Exception("cannot retr");
    DynStr res;
    bool toobig = false;
    for (;;) {
      ln = readLine!exact();
      if (!toobig) {
        res ~= ln;
        static if (!exact) res ~= '\n';
      }
      if (ln.length == 0 || ln[0] != '.') continue;
      if (ln.length == 1) break;
      static if (exact) {
        if (ln.length == 3 && ln == ".\r\n") break;
        if (ln.length == 2 && ln == ".\n") break;
      }
      if (!toobig && res.length > 1024*1024*128) {
        conwriteln("WARNING: mail message too big!");
        toobig = true;
      }
    }
    return res;
  }

  void deleteMessage (int idx) {
    if (idx < 1) throw new Exception("invalid message index");
    doSendCmdArg("DELE", idx);
    auto ln = readLine();
    if (!isOK(ln)) throw new Exception("cannot retr");
  }

private:
  static bool isOK (const(char)[] s) {
    if (s.length < 3) return false;
    if (s[0] == '+' && (s[1] == 'O' || s[1] == 'o') && (s[2] == 'K' || s[2] == 'k')) return (s.length == 3 || s[3] <= ' ');
    if (s.length > 8+3) {
      import iv.strex : indexOf;
      auto idx = s.indexOf("+OK Gpop ready for requests from ");
      if (idx >= 0) return true;
    }
    return false;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public final class SocketSMTP : SocketLine {
private:
  dynstring srv; // for hello

private:
  const(char)[] getResponse () {
    for (;;) {
      auto ln = readLine();
      if (ln.length < 4 || ln[3] <= ' ') return ln;
    }
  }

public:
  this (string server, bool adodump=true) {
    srv = server;
    if (srv.length >= 4 && (srv[0..4] == "ssl:" || srv[0..4] == "tls:")) srv = server[4..$];
    super(server, 25, 465, adodump);
    scope(failure) abort(doshutdown:false);
    auto hello = getResponse();
    if (hello.length < 3 || hello[0..2] != "22") throw new Exception("invalid hello (not 22)");
    doSendCmdArg("HELO", srv);
    hello = getResponse();
    if (hello.length < 3 || hello[0..2] != "25") throw new Exception("invalid hello (not 25)");
  }

  override void quit () {
    if (!active) return;
    doSend("QUIT");
    auto ln = getResponse();
  }

  void auth (const(char)[] authority, const(char)[] user, const(char)[] pass) {
    auto authstr = base64Encode!char(authority~'\0'~user~'\0'~pass);
    doSendCmdArg("AUTH PLAIN", authstr);
    auto ln = getResponse();
    if (ln.length < 3 || ln[0..2] != "23") throw new Exception("authentication failed");
  }

  // `dsdata` should be dot-stuffed, and end with proper termination dot line (with CRLF)
  void sendMessage (const(char)[] from, const(char)[] to, const(char)[] dsdata) {
    dynstring mf = "MAIL FROM: <";
    mf ~= from;
    mf ~= ">";
    doSend(mf.getData);
    auto ln = getResponse();
    if (ln.length < 3 || ln[0..2] != "25") throw new Exception("sending from failed");
    mf = "RCPT TO: <";
    mf ~= to;
    mf ~= ">";
    doSend(mf.getData);
    ln = getResponse();
    if (ln.length < 3 || ln[0..2] != "25") throw new Exception("sending to failed");
    doSend("DATA");
    ln = getResponse();
    if (ln.length < 3 || ln[0..2] != "35") throw new Exception("sending data failed");
    doSendRaw(dsdata);
    ln = getResponse();
    if (ln.length < 3 || ln[0..2] != "25") throw new Exception("sending data failed");
  }
}
