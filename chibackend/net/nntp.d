/* DigitalMars NNTP reader
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module chibackend.net.nntp /*is aliced*/;

import std.socket;

import iv.alice;
import iv.cmdcon;

import chibackend : DynStr;
import chibackend.net.linesocket;


/*
usage:

receiving
=========

  auto nsk = new SocketNNTP(server);
  scope(exit) nsk.close();

  nsk.selectGroup(group);
  if (nsk.emptyGroup) {
    conwriteln("[", name, ":", group, "]: no new articles");
    return;
  }

  uint stnum = inbox.maxNntpIndex+1;
  if (stnum == 0) stnum = (nsk.hiwater > 1023 ? nsk.hiwater-1023 : 0);
  if (stnum > nsk.hiwater) {
    conwriteln("[", name, ":", group, "]: no new articles");
    return;
  }

  conwriteln("[", name, ":", group, "]: ", nsk.hiwater+1-stnum, " (possible) new articles");

  // download new articles
  foreach (immutable uint anum; stnum..nsk.hiwater+1) {
    auto msg = nsk.getArticle(anum);
  }

sending
========
  SocketNNTP nsk;
  try {
    nsk = new SocketNNTP(server);
  } catch (Exception e) {
    conwriteln("[", name, ":", group, "]: connection error: ", e.msg);
    return false;
  }
  scope(exit) nsk.close();

  try {
    nsk.selectGroup(group);
    nsk.doSend("POST");
    nsk.doSendRaw(art.getTextToSend);
    auto ln = nsk.readLine;
    conwriteln(ln); // 340 Ok, recommended message-ID <o7dq4o$mpm$1@digitalmars.com>
    if (ln.length == 0 || ln[0] != '3') throw new Exception(ln.idup);
  } catch (Exception e) {
    conwriteln("[", name, ":", group, "]: sending error: ", e.msg);
    return false;
  }
*/


// ////////////////////////////////////////////////////////////////////////// //
public final class SocketNNTP : SocketLine {
private:
  bool canPost;

public: //WARNING! DO NOT CHANGE!
  dynstring group;
  uint lowater, hiwater;
  bool emptyGroup = true;

public:
  this (string server, bool debugdump=false) {
    super(server, plainport:119, tlsport:563, debugdump:debugdump);
    scope(failure) abort(doshutdown:false);
    auto hello = readLine();
    if (hello.length < 4 && hello[0] != '2' || hello[1] != '0' || hello[3] != ' ') throw new Exception("invalid hello");
         if (hello[2] == '0') canPost = true;
    else if (hello[2] == '1') canPost = false;
    else throw new Exception("invalid hello");
  }

  @property bool postingAllowed () const nothrow @safe @nogc { return canPost; }

  override void quit () {
    if (!active) return;
    doSend("QUIT");
    auto ln = readLine();
  }

  void selectGroup (const(char)[] name) {
    doSendCmdArg("GROUP", name);
    const(char)[] ln;
    try {
      ln = readLine();
    } catch (Throwable e) {
      abort(doshutdown:false);
      throw e;
    }
    scope(failure) { group.clear(); lowater = hiwater = 0; emptyGroup = true; }
    auto origln = ln;
    if (getStrToken(ln) != "211") {
      conwriteln("*ERROR: NNTP server GROUP reply: ", origln);
      throw new Exception("invalid GROUP reply");
    }
    auto cnt = getToken!uint(ln);
    lowater = getToken!uint(ln);
    hiwater = getToken!uint(ln);
    if (cnt == 0 || lowater > hiwater || (lowater == 0 && hiwater == 0)) {
      lowater = hiwater = 0;
      emptyGroup = true;
    } else {
      emptyGroup = false;
    }
    group = name;
  }

  // if `exact` is true, returns line with exact terminators
  // returns empty string if there's no such article (this is not a error!)
  // can return truncated message if it is too big
  DynStr getArticle(bool exact=false) (uint num) {
    DynStr res;
    doSendCmdArg("ARTICLE", num);
    auto ln = readLine();
    //conwriteln("|", ln, "|");
    auto rescode = getToken!uint(ln);
    if (rescode != 220) {
      if (rescode == 420 || rescode == 423) {
        //throw new Exception("no such article");
        return res;
      }
      close();
      throw new Exception("ARTICLE failed");
    }
    if (getToken!uint(ln) != num) { close(); throw new Exception("invalid replied article number"); }
    uint number = num;
    //string msgid = getToken(ln).idup;
    // add index header
    static if (exact) bool needAddEOL = false;
    if (number) {
      res ~= "NNTP-Index: ";
      res.appendNum(number);
      static if (exact) needAddEOL = true; else res.append('\n');
      if (mDebugDump) conwriteln("+", number);
    }
    // read text
    bool toobig = false;
    for (;;) {
      ln = readLine!exact;
      static if (exact) {
        if (needAddEOL && ln.length) {
          if (ln.length >= 2 && ln[$-2..$] == "\r\n") res.append("\r\n"); else res.append("\n");
          needAddEOL = false;
        }
      }
      if (!toobig) {
        res ~= ln;
        static if (!exact) res ~= '\n';
      }
      if (ln.length == 0 || ln[0] != '.') continue;
      if (ln.length == 1) break;
      static if (exact) {
        if (ln.length == 3 && ln == ".\r\n") break;
        if (ln.length == 2 && ln == ".\n") break;
      }
      if (!toobig && res.length > 1024*1024*128) {
        conwriteln("WARNING: mail message too big!");
        toobig = true;
      }
    }
    static if (exact) {
      if (needAddEOL) res.append('\n');
    }
    return res;
  }
}
