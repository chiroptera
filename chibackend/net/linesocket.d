/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module chibackend.net.linesocket /*is aliced*/;

import std.socket;

import iv.alice;
import iv.cmdcon;
public import iv.dynstring;
import iv.gnutls;
import iv.sslsocket;
import iv.strex : indexOf;


// ////////////////////////////////////////////////////////////////////////// //
public class SocketLine {
protected:
  Socket sk;
  usize rdbufp;
  uint rdbufsize;
  uint rdbufused;
  bool isgnutls;
  bool mDebugDump = false;

private:
  final @property char* rdbuf () nothrow @trusted @nogc { pragma(inline, true); return (cast(char*)rdbufp); }
  final void freeRdBuf () nothrow @trusted @nogc {
    if (rdbufp) {
      import core.stdc.stdlib : free;
      free(cast(void*)rdbufp);
      rdbufp = 0;
    }
  }

private:
  void growRdBuf () {
    import core.stdc.stdlib : realloc;
    enum delta = 8192;
    auto nlen = rdbufsize+delta;
    if (nlen >= 65536) { freeRdBuf(); throw new Exception("line too long"); }
    auto nb = cast(char*)realloc(cast(void*)rdbufp, nlen);
    if (nb is null) { freeRdBuf(); import core.exception : onOutOfMemoryErrorNoGC; onOutOfMemoryErrorNoGC(); }
    rdbufp = cast(usize)nb;
    rdbufsize = nlen;
  }

  bool isErrorAgain (const int res) nothrow @trusted @nogc {
    if (res >= 0) return false;
    if (isgnutls) {
      return (res == GNUTLS_E_AGAIN || res == GNUTLS_E_INTERRUPTED);
    } else {
      import core.stdc.errno;
      return (errno == EAGAIN || errno == EINTR);
    }
  }

public:
  this (string server, ushort plainport, ushort tlsport, bool debugdump=false) {
    mDebugDump = debugdump;
    string caddr;
    ushort cport;
    bool isplain;
    if (server.length >= 4 && (server[0..4] == "ssl:" || server[0..4] == "tls:")) {
      if (tlsport == 0) throw new Exception("TLS port is not defined for '"~server[4..$].idup~"'");
      conwriteln("connecting to [", server[4..$], ":", tlsport, "] (tls)...");
      auto xsk = new SSLClientSocket(AddressFamily.INET, server[4..$]);
      xsk.manualHandshake = false;
      //xsk.connect(new InternetAddress(server[4..$], tlsport));
      caddr = server[4..$];
      cport = tlsport;
      sk = xsk;
      isgnutls = true;
      isplain = false;
    } else {
      if (plainport == 0) throw new Exception("PLAIN port is not defined for '"~server.idup~"'");
      conwriteln("connecting to [", server, ":", plainport, "] (plain)...");
      sk = new TcpSocket();
      caddr = server;
      cport = plainport;
      isgnutls = false;
      isplain = true;
    }
    scope(failure) abort(doshutdown:false);
    import core.time;
    sk.setOption(SocketOptionLevel.SOCKET, SocketOption.SNDTIMEO, 5.seconds);
    sk.setOption(SocketOptionLevel.SOCKET, SocketOption.RCVTIMEO, 5.seconds);
    sk.connect(new InternetAddress(caddr, cport));
    conwriteln("connected to [", caddr, ":", cport, "] (", (isplain ? "plain" : "tls"), ").");
    if (isgnutls) {
      conwriteln("session info: ", (cast(SSLClientSocket)sk).getSessionInfo);
    }
  }

  ~this () nothrow @trusted @nogc { freeRdBuf(); }

  void abort (bool doshutdown) {
    if (sk !is null) {
      try {
        if (cast(SSLClientSocket)sk) {
          sk.close();
        } else {
          if (doshutdown) sk.shutdown(SocketShutdown.BOTH);
          sk.close();
        }
      } catch (Throwable e) {
        conwriteln("CLOSE ERROR: ", e.msg);
      }
      delete sk;
      sk = null;
    }
    freeRdBuf();
  }

  void close () {
    if (sk is null) return;
    bool doshutdown = true;
    try { quit(); } catch (Throwable e) { doshutdown = false; }
    try { abort(doshutdown); } catch (Throwable e) {}
  }

  void quit () {}

  final @property bool active () { return (sk !is null && sk.isAlive); }

  final void doSendRaw(bool full=true) (const(void)[] vdata) {
    import core.sys.posix.sys.socket : MSG_NOSIGNAL;
    if (sk is null || !sk.isAlive) throw new Exception("can't send to closed socket");
    if (vdata.length == 0) return;
    //auto dataanchor = data; // 'cause we may get data which requires pointer to head
    const(char)[] data = cast(const(char)[])vdata; // arg is not modified, and properly anchored
    int againsLeft = 6;
    for (;;) {
      static if (full) {
        if (data.length <= 1) break;
        auto sd = sk.send(data[0..$-1], cast(SocketFlags)(MSG_NOSIGNAL|0x8000/*MSG_MORE*/));
      } else {
        if (data.length == 0) break;
        auto sd = sk.send(data, cast(SocketFlags)(MSG_NOSIGNAL|0x8000/*MSG_MORE*/));
      }
      if (sd <= 0) {
        if (isErrorAgain(sd)) {
          againsLeft -= 1;
          if (againsLeft != 0) continue;
        }
        abort(doshutdown:false);
        import std.conv : to;
        throw new Exception("send error ("~sd.to!string~")");
      }
      data = data[sd..$];
    }
    while (data.length) {
      auto sd = sk.send(data, cast(SocketFlags)(MSG_NOSIGNAL));
      if (sd <= 0) {
        if (isErrorAgain(sd)) {
          againsLeft -= 1;
          if (againsLeft != 0) continue;
        }
        abort(doshutdown:false);
        import std.conv : to;
        throw new Exception("send error ("~sd.to!string~")");
      }
      data = data[sd..$];
    }
  }

  /*
  final void doSend(A...) (const(char)[] fmt, A args) {
    import std.format;
    if (sk is null || !sk.isAlive) throw new Exception("can't send to closed socket");
    immutable ppos = fmt.indexOf('%');
    if (ppos < 0) {
      if (mDebugDump) {
        if (fmt.length > 4 && fmt[0..4] == "PASS") conwriteln("|>PASS *|"); else conwriteln("|>", fmt, "|");
      }
      doSendRaw!false(fmt); // not full yet
    } else {
      string s = fmt.format(args);
      if (mDebugDump) {
        if (s.length > 4 && s[0..4] == "PASS") conwriteln("|>PASS *|"); else conwriteln("|>", s, "|");
      }
      doSendRaw!false(s); // not full yet
    }
    doSendRaw("\r\n");
  }
  */

  final void doSend (const(char)[] str) {
    if (sk is null || !sk.isAlive) throw new Exception("can't send to closed socket");
    if (mDebugDump) {
      if (str.length > 4 && str[0..4] == "PASS") conwriteln("|>PASS *|"); else conwriteln("|>", str, "|");
    }
    doSendRaw!false(str); // not full yet
    doSendRaw("\r\n");
  }

  final void doSendCmdArg (const(char)[] cmd, const(char)[] arg=null) {
    if (sk is null || !sk.isAlive) throw new Exception("can't send to closed socket");
    if (mDebugDump) {
      if (cmd.length > 4 && cmd[0..4] == "PASS") conwriteln("|>PASS *|"); else conwriteln("|>", cmd, " ", arg, "|");
    }
    doSendRaw!false(cmd); // not full yet
    if (arg.length) {
      doSendRaw!false(" "); // not full yet
      doSendRaw!false(arg); // not full yet
    }
    doSendRaw("\r\n");
  }

  final void doSendCmdArg(T) (const(char)[] cmd, const T arg) if (__traits(isIntegral, T)) {
    if (sk is null || !sk.isAlive) throw new Exception("can't send to closed socket");
    if (mDebugDump) {
      if (cmd.length > 4 && cmd[0..4] == "PASS") conwriteln("|>PASS *|"); else conwriteln("|>", cmd, " ", arg, "|");
    }
    doSendRaw!false(cmd); // not full yet
    doSendRaw!false(" "); // not full yet
    import core.stdc.stdio : snprintf;
    static if (T.sizeof <= 4) {
      char[32] buf = void;
      static if (__traits(isUnsigned, T)) {
        auto len = snprintf(buf.ptr, buf.length, "%u", cast(uint)arg);
      } else {
        auto len = snprintf(buf.ptr, buf.length, "%d", cast(int)arg);
      }
      doSendRaw!false(buf[0..len]); // not full yet
    } else {
      char[128] buf = void;
      static if (__traits(isUnsigned, T)) {
        auto len = snprintf(buf.ptr, buf.length, "%llu", cast(ulong)arg);
      } else {
        auto len = snprintf(buf.ptr, buf.length, "%lld", cast(long)arg);
      }
      doSendRaw!false(buf[0..len]); // not full yet
    }
    doSendRaw("\r\n");
  }

  //WARNING! result is valid only until the next `readLine()` call
  // if `exact` is true, returns line with terminators
  final const(char)[] readLine(bool exact=false) () {
    import core.stdc.string : memmove;
    if (sk is null) return null;
    // throw away old line
    // we cannot throw it after receiving, beause we are returning the slice with it, not a copy
    //conwriteln("rdbufused=", rdbufused);
    uint pos = 0;
    while (pos < rdbufused && rdbuf[pos] != '\n') ++pos;
    if (pos < rdbufused) {
      assert(rdbuf[pos] == '\n');
      ++pos;
    }
    if (pos > 0) {
      //conwriteln("removed ", pos, " bytes out of ", rdbufused, " bytes");
      assert(pos <= rdbufused);
      if (pos < rdbufused) memmove(rdbuf, rdbuf+pos, rdbufused-pos);
      rdbufused -= pos;
    }
    // check if we have another line buffered
    //conwriteln("pos=", pos, "; rdbufused=", rdbufused);
    pos = 0;
    while (pos < rdbufused && rdbuf[pos] != '\n') ++pos;
    if (pos < rdbufused) {
      //conwriteln("GOT: ", pos, " out of ", rdbufused);
      static if (exact) {
        // keep terminators
        return rdbuf[0..pos+1];
      } else {
        // remove terminators
        if (pos > 0 && rdbuf[pos-1] == '\r') --pos;
        if (mDebugDump) conwriteln("|<", rdbuf[0..pos], "|");
        return rdbuf[0..pos];
      }
    }
    // no buffered line, get more data
    int againsLeft = 6;
    for (;;) {
      assert(rdbufused <= rdbufsize);
      if (rdbufused >= rdbufsize) growRdBuf();
      auto rc = sk.receive(rdbuf[rdbufused..rdbufsize], SocketFlags.NONE);
      //conwriteln(rc, " bytes read");
      //if (rc == 0) { return rdbuf[0..rdbufused]; }
      if (rc == 0) {
        abort(doshutdown:false);
        throw new Exception("read error (connection closed)");
      }
      if (rc < 0) {
        if (isErrorAgain(rc)) {
          againsLeft -= 1;
          if (againsLeft != 0) continue;
        }
        import std.conv : to;
        abort(doshutdown:false);
        throw new Exception("read error ("~rc.to!string~")");
      }
      // check for line end
      uint stpos = rdbufused;
      rdbufused += rc;
      foreach (immutable idx; stpos..stpos+rc) {
        if (rdbuf[idx] == '\n') {
          if (mDebugDump) {
            if (idx > 0 && rdbuf[idx-1] == '\r') {
              conwriteln("|<", rdbuf[0..idx-1], "|");
            } else {
              conwriteln("|<", rdbuf[0..idx], "|");
            }
          }
          static if (exact) {
            // keep terminators
            return rdbuf[0..idx+1];
          } else {
            // remove terminators
            if (idx > 0 && rdbuf[idx-1] == '\r') return rdbuf[0..idx-1];
            return rdbuf[0..idx];
          }
        }
      }
    }
  }

static:
  const(char)[] getStrToken (ref const(char)[] s) nothrow @trusted @nogc {
    while (s.length && s[0] <= ' ') s = s[1..$];
    if (s.length == 0) return null;
    int pos = 0;
    while (pos < s.length && s.ptr[pos] > ' ') ++pos;
    auto res = s[0..pos];
    s = s[pos..$];
    while (s.length && s[0] <= ' ') s = s[1..$];
    return res;
  }

  T getToken(T) (ref const(char)[] s) {
    import std.conv : to;
    auto tk = getStrToken(s);
    return to!T(tk);
  }
}
