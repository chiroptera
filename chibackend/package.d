/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module chibackend;

public import chibackend.sqbase;
public import chibackend.decode;
public import chibackend.parse;
public import chibackend.mfilter;
public import chibackend.mbuilder;

private import iv.strex;
private import iv.vfs.util;

public import iv.dynstring : DynStr = dynstring;


// ////////////////////////////////////////////////////////////////////////// //
// some utilities

// this marks the buffer as "opaque", so GC won't scan its contents
// this is to avoid false positives in GC
/+
public void markGCOpaque (const(void)[] buf) /*nothrow @trusted*/ {
  import core.memory : GC;
  if (buf !is null && buf.length != 0 && GC.addrOf(buf.ptr)) {
    { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "X000!\n"); }
    GC.setAttr(buf.ptr, GC.BlkAttr.NO_SCAN/*|GC.BlkAttr.NO_INTERIOR*/);
    { import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "X001!\n"); }
  }
}
+/


public __gshared string chiroCLIMailPath = "~/Mail";

public void chiroParseCommonCLIArgs (ref string[] args) {
  for (usize idx = 1; idx < args.length; ) {
    string arg = args[idx];
    if (arg.length == 0) { ++idx; continue; }
    if (arg == "--") break;

    usize argcount = 1;
    if (arg == "--mailpath") {
      argcount = 2;
      if (idx+1 >= args.length) throw new Exception("\"--mailpath\" expects argument");
      chiroCLIMailPath = args[idx+1];
    } else if (arg.strEquCI("--nocompress")) {
      ChiroCompressionLevel = 0;
    } else if (arg.strEquCI("--max")) {
      ChiroCompressionLevel = 666;
    } else if (arg.length == 2 && arg[0] == '-' && arg[1] >= '0' && arg[1] <= '9') {
      ChiroCompressionLevel = arg[1]-'0';
    } else {
      ++idx;
      continue;
    }
    foreach (usize c; idx+argcount..args.length) args[c-argcount] = args[c];
    args.length -= argcount;
  }

  if (chiroCLIMailPath.length == 0) {
    chiroCLIMailPath = "./";
  } else if (chiroCLIMailPath[0] == '~') {
    char[] dpath = new char[chiroCLIMailPath.length+4096];
    dpath = expandTilde(dpath, chiroCLIMailPath);
    while (dpath.length > 1 && dpath[$-1] == '/') dpath = dpath[0..$-1];
    dpath ~= '/';
    chiroCLIMailPath = cast(string)dpath; // it is safe to cast here
  } else if (chiroCLIMailPath[$-1] == '/') {
    chiroCLIMailPath ~= '/';
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public string SysTimeToRFCString() (in auto ref Imp!"std.datetime".SysTime tm) {
  import std.datetime;

  //Sun, 7 Dec 2014 16:04:04 +0200
  immutable string[7] daysOfWeekNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  immutable string[12] monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

  string tzstr (Duration utcOffset) {
    import std.format : format;
    immutable absOffset = abs(utcOffset);
    int hours;
    int minutes;
    absOffset.split!("hours", "minutes")(hours, minutes);
    return format(utcOffset < Duration.zero ? "-%02d%02d" : "+%02d%02d", hours, minutes);
  }

  try {
    import std.format : format;
    auto dateTime = cast(DateTime)tm;
    return "%s, %d %s %d %02d:%02d:%02d %s".format(
      daysOfWeekNames[dateTime.dayOfWeek],
      dateTime.day,
      monthNames[dateTime.month-1],
      dateTime.year,
      dateTime.hour,
      dateTime.minute,
      dateTime.second,
      tzstr(tm.utcOffset),
    );
  } catch (Exception e) {
    assert(0, "format() threw.");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// simple exponential running average
struct RunningAverageExp {
protected:
  double mFadeoff = 0.1; // 10%
  double mCurrValue = 0.0;
  ulong mStartTime = 0;
  uint mProcessed = 0;
  uint mTotal = 0;
  uint mProgTrh = 1024;
  ulong mTimerNextUpdate = 0;

public nothrow @trusted @nogc:
  this (const double aFadeoff) { pragma(inline, true);mFadeoff = aFadeoff; }

  void reset () { pragma(inline, true);mCurrValue = 0.0; }

  @property double fadeoff () const pure { pragma(inline, true);return mFadeoff; }
  @property void fadeoff (const double aFadeoff) { pragma(inline, true);mFadeoff = aFadeoff; }

  void update (const double newValue) { pragma(inline, true);mCurrValue = mFadeoff*newValue+(1.0-mFadeoff)*mCurrValue; }

  @property double value () const pure { pragma(inline, true);return mCurrValue; }
  @property void value (const double aValue) { pragma(inline, true);mCurrValue = aValue; }

  @property uint uintValue () const pure { pragma(inline, true);return cast(uint)mCurrValue; }

  void startTimer (uint total) {
    mStartTime = GetTickCount();
    mProcessed = 0;
    mTotal = total;
    mCurrValue = 0.0;
    mTimerNextUpdate = 0;
  }

  @property uint progressThreshold () const pure { pragma(inline, true); return mProgTrh; }
  @property void progressThreshold (uint trh) { pragma(inline, true); mProgTrh = trh; }

  @property uint timerTotal () const pure { pragma(inline, true); return mTotal; }
  @property void timerTotal (uint total) { pragma(inline, true); mTotal = total; }

  // returns `true` if it is time to show progress
  bool updateProcessed (uint count, bool delta=true) {
    if (!count) return false;
    immutable uint prev = mProcessed;
    if (delta) mProcessed += count; else mProcessed = count;
    if (mProgTrh == 0 || prev == 0) return true;
    return (prev/mProgTrh != mProcessed/mProgTrh);
  }

  // seconds
  void getTimeETA() (out uint time, out uint eta, out uint curr, out uint total, out uint percent, ulong ctime=ulong.max) {
    if (ctime == ulong.max) ctime = GetTickCount();
    immutable uint secs = cast(uint)(ctime-mStartTime);
    time = secs;
    if (secs && mTotal && mProcessed) {
      //secs/count*total
      eta = cast(uint)(cast(ulong)secs*mTotal/mProcessed);
      if (mCurrValue < eta) mCurrValue = eta;
      update(eta);
      if (mCurrValue < eta) mCurrValue = eta;
    }
    eta = cast(uint)mCurrValue;

    curr = mProcessed;
    total = mTotal;
    percent = (mTotal ? cast(uint)(cast(ulong)mProcessed*100U/mTotal) : 0U);
  }

  bool updateProcessedWithProgress (uint count, bool delta=true, bool forceWrite=false, bool writeNL=false) {
    if (!updateProcessed(count, delta)) {
      if (!forceWrite) return false;
    }

    immutable ctime = GetTickCount();
    if (!forceWrite && mTimerNextUpdate > ctime) return false;
    mTimerNextUpdate = ctime+1;

    uint secs, eta, curr, tot, prc;
    getTimeETA(out secs, out eta, out curr, out tot, out prc, ctime);

    char[128] buf = void;
    uint h, m, s;

    import core.stdc.stdio : snprintf;
    auto len = snprintf(buf.ptr, buf.length, "\r%s[%u/%u] %3u%%", (writeNL ? "".ptr : " ".ptr), curr, tot, prc);
    extractHMS(secs, out h, out m, out s);
    if (len < buf.length) len += snprintf(buf.ptr+len, buf.length-len, "  %02u:%02u:%02u", h, m, s);
    extractHMS(eta, out h, out m, out s);
    if (len < buf.length) len += snprintf(buf.ptr+len, buf.length-len, "  %02u:%02u:%02u", h, m, s);
    if (len < buf.length) len += snprintf(buf.ptr+len, buf.length-len, "\x1b[K%c", (writeNL ? '\n' : '\r'));

    import core.sys.posix.unistd : write, STDOUT_FILENO;
    write(STDOUT_FILENO, buf.ptr, cast(usize)len);

    return true;
  }

  static extractHMS (uint secs, out uint h, out uint m, out uint s) {
    s = secs%60; secs /= 60;
    m = secs%60; secs /= 60;
    h = secs;
  }

  static ulong GetTickCount() () {
    import core.time;
    immutable MonoTime ctt = MonoTime.currTime;
    return cast(ulong)ctt.ticks/cast(ulong)ctt.ticksPerSecond;
  }
};
