/*
wimlib
GNU GPLv3
https://wimlib.net/
*/
module chibackend.pack.libwim;
pragma(lib, "wimk8");
pragma(lib, "xml2");
extern(C):
public nothrow @nogc @trusted:


/**
 * Possible values of the error code returned by many functions in wimlib.
 *
 * See the documentation for each wimlib function to see specifically what error
 * codes can be returned by a given function, and what they mean.
 */
alias wimlib_error_code = int;
enum /*wimlib_error_code*/ {
  WIMLIB_ERR_SUCCESS                            = 0,
  WIMLIB_ERR_ALREADY_LOCKED                     = 1,
  WIMLIB_ERR_DECOMPRESSION                      = 2,
  WIMLIB_ERR_FUSE                               = 6,
  WIMLIB_ERR_GLOB_HAD_NO_MATCHES                = 8,
  WIMLIB_ERR_IMAGE_COUNT                        = 10,
  WIMLIB_ERR_IMAGE_NAME_COLLISION               = 11,
  WIMLIB_ERR_INSUFFICIENT_PRIVILEGES            = 12,
  WIMLIB_ERR_INTEGRITY                          = 13,
  WIMLIB_ERR_INVALID_CAPTURE_CONFIG             = 14,
  WIMLIB_ERR_INVALID_CHUNK_SIZE                 = 15,
  WIMLIB_ERR_INVALID_COMPRESSION_TYPE           = 16,
  WIMLIB_ERR_INVALID_HEADER                     = 17,
  WIMLIB_ERR_INVALID_IMAGE                      = 18,
  WIMLIB_ERR_INVALID_INTEGRITY_TABLE            = 19,
  WIMLIB_ERR_INVALID_LOOKUP_TABLE_ENTRY         = 20,
  WIMLIB_ERR_INVALID_METADATA_RESOURCE          = 21,
  WIMLIB_ERR_INVALID_OVERLAY                    = 23,
  WIMLIB_ERR_INVALID_PARAM                      = 24,
  WIMLIB_ERR_INVALID_PART_NUMBER                = 25,
  WIMLIB_ERR_INVALID_PIPABLE_WIM                = 26,
  WIMLIB_ERR_INVALID_REPARSE_DATA               = 27,
  WIMLIB_ERR_INVALID_RESOURCE_HASH              = 28,
  WIMLIB_ERR_INVALID_UTF16_STRING               = 30,
  WIMLIB_ERR_INVALID_UTF8_STRING                = 31,
  WIMLIB_ERR_IS_DIRECTORY                       = 32,
  WIMLIB_ERR_IS_SPLIT_WIM                       = 33,
  WIMLIB_ERR_LINK                               = 35,
  WIMLIB_ERR_METADATA_NOT_FOUND                 = 36,
  WIMLIB_ERR_MKDIR                              = 37,
  WIMLIB_ERR_MQUEUE                             = 38,
  WIMLIB_ERR_NOMEM                              = 39,
  WIMLIB_ERR_NOTDIR                             = 40,
  WIMLIB_ERR_NOTEMPTY                           = 41,
  WIMLIB_ERR_NOT_A_REGULAR_FILE                 = 42,
  WIMLIB_ERR_NOT_A_WIM_FILE                     = 43,
  WIMLIB_ERR_NOT_PIPABLE                        = 44,
  WIMLIB_ERR_NO_FILENAME                        = 45,
  WIMLIB_ERR_NTFS_3G                            = 46,
  WIMLIB_ERR_OPEN                               = 47,
  WIMLIB_ERR_OPENDIR                            = 48,
  WIMLIB_ERR_PATH_DOES_NOT_EXIST                = 49,
  WIMLIB_ERR_READ                               = 50,
  WIMLIB_ERR_READLINK                           = 51,
  WIMLIB_ERR_RENAME                             = 52,
  WIMLIB_ERR_REPARSE_POINT_FIXUP_FAILED         = 54,
  WIMLIB_ERR_RESOURCE_NOT_FOUND                 = 55,
  WIMLIB_ERR_RESOURCE_ORDER                     = 56,
  WIMLIB_ERR_SET_ATTRIBUTES                     = 57,
  WIMLIB_ERR_SET_REPARSE_DATA                   = 58,
  WIMLIB_ERR_SET_SECURITY                       = 59,
  WIMLIB_ERR_SET_SHORT_NAME                     = 60,
  WIMLIB_ERR_SET_TIMESTAMPS                     = 61,
  WIMLIB_ERR_SPLIT_INVALID                      = 62,
  WIMLIB_ERR_STAT                               = 63,
  WIMLIB_ERR_UNEXPECTED_END_OF_FILE             = 65,
  WIMLIB_ERR_UNICODE_STRING_NOT_REPRESENTABLE   = 66,
  WIMLIB_ERR_UNKNOWN_VERSION                    = 67,
  WIMLIB_ERR_UNSUPPORTED                        = 68,
  WIMLIB_ERR_UNSUPPORTED_FILE                   = 69,
  WIMLIB_ERR_WIM_IS_READONLY                    = 71,
  WIMLIB_ERR_WRITE                              = 72,
  WIMLIB_ERR_XML                                = 73,
  WIMLIB_ERR_WIM_IS_ENCRYPTED                   = 74,
  WIMLIB_ERR_WIMBOOT                            = 75,
  WIMLIB_ERR_ABORTED_BY_PROGRESS                = 76,
  WIMLIB_ERR_UNKNOWN_PROGRESS_STATUS            = 77,
  WIMLIB_ERR_MKNOD                              = 78,
  WIMLIB_ERR_MOUNTED_IMAGE_IS_BUSY              = 79,
  WIMLIB_ERR_NOT_A_MOUNTPOINT                   = 80,
  WIMLIB_ERR_NOT_PERMITTED_TO_UNMOUNT           = 81,
  WIMLIB_ERR_FVE_LOCKED_VOLUME                  = 82,
  WIMLIB_ERR_UNABLE_TO_READ_CAPTURE_CONFIG      = 83,
  WIMLIB_ERR_WIM_IS_INCOMPLETE                  = 84,
  WIMLIB_ERR_COMPACTION_NOT_POSSIBLE            = 85,
  WIMLIB_ERR_IMAGE_HAS_MULTIPLE_REFERENCES      = 86,
  WIMLIB_ERR_DUPLICATE_EXPORTED_IMAGE           = 87,
  WIMLIB_ERR_CONCURRENT_MODIFICATION_DETECTED   = 88,
  WIMLIB_ERR_SNAPSHOT_FAILURE                   = 89,
  WIMLIB_ERR_INVALID_XATTR                      = 90,
  WIMLIB_ERR_SET_XATTR                          = 91,
}


/**
 * Specifies a compression type.
 *
 * A WIM file has a default compression type, indicated by its file header.
 * Normally, each resource in the WIM file is compressed with this compression
 * type.  However, resources may be stored as uncompressed; for example, wimlib
 * may do so if a resource does not compress to less than its original size.  In
 * addition, a WIM with the new version number of 3584, or "ESD file", might
 * contain solid resources with different compression types.
 */
alias wimlib_compression_type = int;
enum /*wimlib_compression_type*/ {
  /**
   * No compression.
   *
   * This is a valid argument to wimlib_create_new_wim() and
   * wimlib_set_output_compression_type(), but not to the functions in the
   * compression API such as wimlib_create_compressor().
   */
  //WIMLIB_COMPRESSION_TYPE_NONE = 0,

  /**
   * The XPRESS compression format.  This format combines Lempel-Ziv
   * factorization with Huffman encoding.  Compression and decompression
   * are both fast.  This format supports chunk sizes that are powers of 2
   * between <c>2^12</c> and <c>2^16</c>, inclusively.
   *
   * wimlib's XPRESS compressor will, with the default settings, usually
   * produce a better compression ratio, and work more quickly, than the
   * implementation in Microsoft's WIMGAPI (as of Windows 8.1).
   * Non-default compression levels are also supported.  For example,
   * level 80 will enable two-pass optimal parsing, which is significantly
   * slower but usually improves compression by several percent over the
   * default level of 50.
   *
   * If using wimlib_create_compressor() to create an XPRESS compressor
   * directly, the @p max_block_size parameter may be any positive value
   * up to and including <c>2^16</c>.
   */
  WIMLIB_COMPRESSION_TYPE_XPRESS = 1,

  /**
   * The LZX compression format.  This format combines Lempel-Ziv
   * factorization with Huffman encoding, but with more features and
   * complexity than XPRESS.  Compression is slow to somewhat fast,
   * depending on the settings.  Decompression is fast but slower than
   * XPRESS.  This format supports chunk sizes that are powers of 2
   * between <c>2^15</c> and <c>2^21</c>, inclusively.  Note: chunk sizes
   * other than <c>2^15</c> are not compatible with the Microsoft
   * implementation.
   *
   * wimlib's LZX compressor will, with the default settings, usually
   * produce a better compression ratio, and work more quickly, than the
   * implementation in Microsoft's WIMGAPI (as of Windows 8.1).
   * Non-default compression levels are also supported.  For example,
   * level 20 will provide fast compression, almost as fast as XPRESS.
   *
   * If using wimlib_create_compressor() to create an LZX compressor
   * directly, the @p max_block_size parameter may be any positive value
   * up to and including <c>2^21</c>.
   */
  WIMLIB_COMPRESSION_TYPE_LZX = 2,

  /**
   * The LZMS compression format.  This format combines Lempel-Ziv
   * factorization with adaptive Huffman encoding and range coding.
   * Compression and decompression are both fairly slow.  This format
   * supports chunk sizes that are powers of 2 between <c>2^15</c> and
   * <c>2^30</c>, inclusively.  This format is best used for large chunk
   * sizes.  Note: LZMS compression is only compatible with wimlib v1.6.0
   * and later, WIMGAPI Windows 8 and later, and DISM Windows 8.1 and
   * later.  Also, chunk sizes larger than <c>2^26</c> are not compatible
   * with the Microsoft implementation.
   *
   * wimlib's LZMS compressor will, with the default settings, usually
   * produce a better compression ratio, and work more quickly, than the
   * implementation in Microsoft's WIMGAPI (as of Windows 8.1).  There is
   * limited support for non-default compression levels, but compression
   * will be noticeably faster if you choose a level < 35.
   *
   * If using wimlib_create_compressor() to create an LZMS compressor
   * directly, the @p max_block_size parameter may be any positive value
   * up to and including <c>2^30</c>.
   */
  WIMLIB_COMPRESSION_TYPE_LZMS = 3,
};

// must be power of 2
enum {
  WIMLIB_XPRESS_MIN_CHUNK = 1<<12,
  WIMLIB_XPRESS_MAX_CHUNK = 1<<16,
}

enum {
  WIMLIB_LZX_MIN_CHUNK = 1,
  WIMLIB_LZX_MAX_CHUNK = 1<<21,
}

enum {
  WIMLIB_LZMS_MIN_CHUNK = 1,
  WIMLIB_LZMS_MAX_CHUNK = 1<<30,
}


/**
 * @defgroup G_compression Compression and decompression functions
 *
 * @brief Functions for XPRESS, LZX, and LZMS compression and decompression.
 *
 * These functions are already used by wimlib internally when appropriate for
 * reading and writing WIM archives.  But they are exported and documented so
 * that they can be used in other applications or libraries for general-purpose
 * lossless data compression.  They are implemented in highly optimized C code,
 * using state-of-the-art compression techniques.  The main limitation is the
 * lack of sliding window support; this has, however, allowed the algorithms to
 * be optimized for block-based compression.
 *
 * @{
 */

/** Opaque compressor handle.  */
struct wimlib_compressor;

/** Opaque decompressor handle.  */
struct wimlib_decompressor;

/**
 * Set the default compression level for the specified compression type.  This
 * is the compression level that wimlib_create_compressor() assumes if it is
 * called with @p compression_level specified as 0.
 *
 * wimlib's WIM writing code (e.g. wimlib_write()) will pass 0 to
 * wimlib_create_compressor() internally.  Therefore, calling this function will
 * affect the compression level of any data later written to WIM files using the
 * specified compression type.
 *
 * The initial state, before this function is called, is that all compression
 * types have a default compression level of 50.
 *
 * @param ctype
 *  Compression type for which to set the default compression level, as one
 *  of the ::wimlib_compression_type constants.  Or, if this is the special
 *  value -1, the default compression levels for all compression types will
 *  be set.
 * @param compression_level
 *  The default compression level to set.  If 0, the "default default" level
 *  of 50 is restored.  Otherwise, a higher value indicates higher
 *  compression, whereas a lower value indicates lower compression.  See
 *  wimlib_create_compressor() for more information.
 *
 * @return 0 on success; a ::wimlib_error_code value on failure.
 *
 * @retval ::WIMLIB_ERR_INVALID_COMPRESSION_TYPE
 *  @p ctype was neither a supported compression type nor -1.
 */
int wimlib_set_default_compression_level (int ctype, uint compression_level);

/**
 * Return the approximate number of bytes needed to allocate a compressor with
 * wimlib_create_compressor() for the specified compression type, maximum block
 * size, and compression level.  @p compression_level may be 0, in which case
 * the current default compression level for @p ctype is used.  Returns 0 if the
 * compression type is invalid, or the @p max_block_size for that compression
 * type is invalid.
 */
extern ulong wimlib_get_compressor_needed_memory (wimlib_compression_type ctype,
                                                  usize max_block_size,
                                                  uint compression_level);

enum WIMLIB_COMPRESSOR_FLAG_DESTRUCTIVE = 0x80000000;

/**
 * Allocate a compressor for the specified compression type using the specified
 * parameters.  This function is part of wimlib's compression API; it is not
 * necessary to call this to process a WIM file.
 *
 * @param ctype
 *  Compression type for which to create the compressor, as one of the
 *  ::wimlib_compression_type constants.
 * @param max_block_size
 *  The maximum compression block size to support.  This specifies the
 *  maximum allowed value for the @p uncompressed_size parameter of
 *  wimlib_compress() when called using this compressor.
 *  <br/>
 *  Usually, the amount of memory used by the compressor will scale in
 *  proportion to the @p max_block_size parameter.
 *  wimlib_get_compressor_needed_memory() can be used to query the specific
 *  amount of memory that will be required.
 *  <br/>
 *  This parameter must be at least 1 and must be less than or equal to a
 *  compression-type-specific limit.
 *  <br/>
 *  In general, the same value of @p max_block_size must be passed to
 *  wimlib_create_decompressor() when the data is later decompressed.
 *  However, some compression types have looser requirements regarding this.
 * @param compression_level
 *  The compression level to use.  If 0, the default compression level (50,
 *  or another value as set through wimlib_set_default_compression_level())
 *  is used.  Otherwise, a higher value indicates higher compression.  The
 *  values are scaled so that 10 is low compression, 50 is medium
 *  compression, and 100 is high compression.  This is not a percentage;
 *  values above 100 are also valid.
 *  <br/>
 *  Using a higher-than-default compression level can result in a better
 *  compression ratio, but can significantly reduce performance.  Similarly,
 *  using a lower-than-default compression level can result in better
 *  performance, but can significantly worsen the compression ratio.  The
 *  exact results will depend heavily on the compression type and what
 *  algorithms are implemented for it.  If you are considering using a
 *  non-default compression level, you should run benchmarks to see if it is
 *  worthwhile for your application.
 *  <br/>
 *  The compression level does not affect the format of the compressed data.
 *  Therefore, it is a compressor-only parameter and does not need to be
 *  passed to the decompressor.
 *  <br/>
 *  Since wimlib v1.8.0, this parameter can be OR-ed with the flag
 *  ::WIMLIB_COMPRESSOR_FLAG_DESTRUCTIVE.  This creates the compressor in a
 *  mode where it is allowed to modify the input buffer.  Specifically, in
 *  this mode, if compression succeeds, the input buffer may have been
 *  modified, whereas if compression does not succeed the input buffer still
 *  may have been written to but will have been restored exactly to its
 *  original state.  This mode is designed to save some memory when using
 *  large buffer sizes.
 * @param compressor_ret
 *  A location into which to return the pointer to the allocated compressor.
 *  The allocated compressor can be used for any number of calls to
 *  wimlib_compress() before being freed with wimlib_free_compressor().
 *
 * @return 0 on success; a ::wimlib_error_code value on failure.
 *
 * @retval ::WIMLIB_ERR_INVALID_COMPRESSION_TYPE
 *  @p ctype was not a supported compression type.
 * @retval ::WIMLIB_ERR_INVALID_PARAM
 *  @p max_block_size was invalid for the compression type, or @p
 *  compressor_ret was @c NULL.
 * @retval ::WIMLIB_ERR_NOMEM
 *  Insufficient memory to allocate the compressor.
 */
int wimlib_create_compressor (wimlib_compression_type ctype,
                              usize max_block_size,
                              uint compression_level,
                              wimlib_compressor** compressor_ret);

/**
 * Compress a buffer of data.
 *
 * @param uncompressed_data
 *  Buffer containing the data to compress.
 * @param uncompressed_size
 *  Size, in bytes, of the data to compress.  This cannot be greater than
 *  the @p max_block_size with which wimlib_create_compressor() was called.
 *  (If it is, the data will not be compressed and 0 will be returned.)
 * @param compressed_data
 *  Buffer into which to write the compressed data.
 * @param compressed_size_avail
 *  Number of bytes available in @p compressed_data.
 * @param compressor
 *  A compressor previously allocated with wimlib_create_compressor().
 *
 * @return
 *  The size of the compressed data, in bytes, or 0 if the data could not be
 *  compressed to @p compressed_size_avail or fewer bytes.
 */
usize wimlib_compress (const(void)* uncompressed_data, usize uncompressed_size,
                       void* compressed_data, usize compressed_size_avail,
                       wimlib_compressor* compressor);

/**
 * Free a compressor previously allocated with wimlib_create_compressor().
 *
 * @param compressor
 *  The compressor to free.  If @c NULL, no action is taken.
 */
void wimlib_free_compressor (wimlib_compressor* compressor);

/**
 * Allocate a decompressor for the specified compression type.  This function is
 * part of wimlib's compression API; it is not necessary to call this to process
 * a WIM file.
 *
 * @param ctype
 *  Compression type for which to create the decompressor, as one of the
 *  ::wimlib_compression_type constants.
 * @param max_block_size
 *  The maximum compression block size to support.  This specifies the
 *  maximum allowed value for the @p uncompressed_size parameter of
 *  wimlib_decompress().
 *  <br/>
 *  In general, this parameter must be the same as the @p max_block_size
 *  that was passed to wimlib_create_compressor() when the data was
 *  compressed.  However, some compression types have looser requirements
 *  regarding this.
 * @param decompressor_ret
 *  A location into which to return the pointer to the allocated
 *  decompressor.  The allocated decompressor can be used for any number of
 *  calls to wimlib_decompress() before being freed with
 *  wimlib_free_decompressor().
 *
 * @return 0 on success; a ::wimlib_error_code value on failure.
 *
 * @retval ::WIMLIB_ERR_INVALID_COMPRESSION_TYPE
 *  @p ctype was not a supported compression type.
 * @retval ::WIMLIB_ERR_INVALID_PARAM
 *  @p max_block_size was invalid for the compression type, or @p
 *  decompressor_ret was @c NULL.
 * @retval ::WIMLIB_ERR_NOMEM
 *  Insufficient memory to allocate the decompressor.
 */
int wimlib_create_decompressor (wimlib_compression_type ctype,
                                usize max_block_size,
                                wimlib_decompressor** decompressor_ret);

/**
 * Decompress a buffer of data.
 *
 * @param compressed_data
 *  Buffer containing the data to decompress.
 * @param compressed_size
 *  Size, in bytes, of the data to decompress.
 * @param uncompressed_data
 *  Buffer into which to write the uncompressed data.
 * @param uncompressed_size
 *  Size, in bytes, of the data when uncompressed.  This cannot exceed the
 *  @p max_block_size with which wimlib_create_decompressor() was called.
 *  (If it does, the data will not be decompressed and a nonzero value will
 *  be returned.)
 * @param decompressor
 *  A decompressor previously allocated with wimlib_create_decompressor().
 *
 * @return 0 on success; nonzero on failure.
 *
 * No specific error codes are defined; any nonzero value indicates that the
 * decompression failed.  This can only occur if the data is truly invalid;
 * there will never be transient errors like "out of memory", for example.
 *
 * This function requires that the exact uncompressed size of the data be passed
 * as the @p uncompressed_size parameter.  If this is not done correctly,
 * decompression may fail or the data may be decompressed incorrectly.
 */
int wimlib_decompress (const(void)* compressed_data, usize compressed_size,
                       void* uncompressed_data, usize uncompressed_size,
                       wimlib_decompressor* decompressor);

/**
 * Free a decompressor previously allocated with wimlib_create_decompressor().
 *
 * @param decompressor
 *  The decompressor to free.  If @c NULL, no action is taken.
 */
void wimlib_free_decompressor (wimlib_decompressor* decompressor);
