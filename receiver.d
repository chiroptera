/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// mail/nntp receiver thread
module receiver is aliced;
private:

//version = debug_filter_helper;
//version = debug_updater;

import std.concurrency;

import iv.cmdcon;
import iv.dynstring;
import iv.strex;
import iv.sq3;
import iv.timer : DurTimer = Timer;
import iv.utfutil;
import iv.vfs.io;

import iv.egra;

import chibackend;
import chibackend.net;

import chievents;

static if (__traits(compiles, () { import extfilter; })) {
  static import extfilter;
  enum HasExtFilter = true;
} else {
  enum HasExtFilter = false;
}


// ////////////////////////////////////////////////////////////////////////// //
class RealFilterHelper : FilterHelper {
public:
  enum {
    // only one of these can be set
    ActFlagDelete = 1u<<0,
    ActFlagPurge = 1u<<1,

    // only one of these can be set
    ActFlagSpam = 1u<<2,
    ActFlagHam = 1u<<3,

    ActFlagRead = 1u<<4,
    ActFlagStop = 1u<<5,
  }

public:
  DynStr account;
  DynStr tag; // destination tag
  uint actFlags; // see above
  DynStr message;
  bool matched;

public:
  ~this () nothrow @nogc { account.clear(); tag.clear(); message.clear(); }

  final @property bool isDelete () const pure nothrow @safe @nogc { pragma(inline, true); return !!(actFlags&ActFlagDelete); }
  final @property bool isPurge () const pure nothrow @safe @nogc { pragma(inline, true); return !!(actFlags&ActFlagPurge); }
  final @property bool isSpam () const pure nothrow @safe @nogc { pragma(inline, true); return !!(actFlags&ActFlagSpam); }
  final @property bool isHam () const pure nothrow @safe @nogc { pragma(inline, true); return !!(actFlags&ActFlagHam); }
  final @property bool isRead () const pure nothrow @safe @nogc { pragma(inline, true); return !!(actFlags&ActFlagRead); }
  final @property bool isStop () const pure nothrow @safe @nogc { pragma(inline, true); return !!(actFlags&ActFlagStop); }

  // called if a filter was matched
  override void filterMatched () {
    matched = true;
  }

  override DynStr getAccount () {
    return account;
  }

  override DynStr getHeaderField (const(char)[] header, out bool exists) {
    auto headers = message.getData[0..findHeadersEnd(message.getData)];
    auto value = findHeaderField(headers, header);
    exists = (value !is null);
    version(debug_filter_helper) writeln("...getHeaderField(", header, "): exists=", exists, "; res=", value);
    return DynStr(value);
  }

  override DynStr getFromName () {
    auto headers = message.getData[0..findHeadersEnd(message.getData)];
    auto value = findHeaderField(headers, "From").extractName;
    version(debug_filter_helper) writeln("...getFromName: res=", value);
    return DynStr(value);
  }

  override DynStr getFromMail () {
    auto headers = message.getData[0..findHeadersEnd(message.getData)];
    auto value = findHeaderField(headers, "From").extractMail;
    if (value.length == 0) value = "nobody@nowhere";
    version(debug_filter_helper) writeln("...getFromMail: res=", value);
    return DynStr(value);
  }

  override DynStr getToName () {
    auto headers = message.getData[0..findHeadersEnd(message.getData)];
    auto value = findHeaderField(headers, "To").extractName;
    version(debug_filter_helper) writeln("...getToName: res=", value);
    return DynStr(value);
  }

  override DynStr getToMail () {
    auto headers = message.getData[0..findHeadersEnd(message.getData)];
    auto value = findHeaderField(headers, "To").extractMail;
    if (value.length == 0) value = "nobody@nowhere";
    version(debug_filter_helper) writeln("...getToMail: res=", value);
    return DynStr(value);
  }

  override DynStr getSubj (out bool exists) {
    auto headers = message.getData[0..findHeadersEnd(message.getData)];
    auto value = findHeaderField(headers, "Subject");
    exists = (value !is null);
    if (exists) value = value.decodeSubj.subjRemoveRe;
    return DynStr(value);
  }

  override DynStr exec (const(char)[] command) {
    /*version(debug_filter_helper)*/ writeln("...exec: <", command, ">");
    //return DynStr("nothing");
    import std.stdio : File;
    import std.process;
    try {
      // write article to file
      import std.uuid;
      UUID id = randomUUID();
      DynStr buf;
      void deleteTempFile () {
        if (buf.length) try { import std.file : remove; remove(buf.getData); } catch (Exception e) {}
      }
      scope(exit) deleteTempFile();
      buf.reserve(2+16*2+42);
      buf ~= "/tmp/_temp_";
      foreach (immutable ubyte b; id.data[]) {
        buf ~= "0123456789abcdef"[b>>4];
        buf ~= "0123456789abcdef"[b&0x0f];
      }
      buf ~= ".eml";
      {
        auto fo = VFile(buf.getData, "w");
        fo.rawWriteExact(message.getData);
        fo.close();
      }
      //!conwriteln("EXEC filter '", command, "'... (", buf.getData, ")");
      auto pid = pipeProcess([command, /*"-D",*/ buf.getData], Redirect.all, null, Config.none, "/tmp");
      string action = pid.stdout.readln.xstrip;
      bool doStop = (action.length && action[0] == '-');
      if (doStop) action = action[1..$].xstrip;
      version(none) {
        while (!pid.stderr.eof) conwriteln("  :", pid.stderr.readln.xstrip, "|");
      }
      pid.pid.wait();
      //!conwriteln("EXEC filter '", command, "' action: ", action, " (", doStop, ")");
      return DynStr(action);
    } catch (Exception e) {
      conwriteln("EXEC filter error: ", e.msg);
    }
    return DynStr();
  }

  override void move (const(char)[] dest) {
    version(debug_filter_helper) writeln("...move: <", dest, ">");
    tag = dest;
  }

  override void performAction (Action action) {
    version(debug_filter_helper) writeln("...performAction: ", action);
    switch (action) {
      case Action.Delete:
        actFlags &= ~(ActFlagDelete|ActFlagPurge);
        actFlags |= ActFlagPurge;
        break;
      case Action.SoftDelete:
        actFlags &= ~(ActFlagDelete|ActFlagPurge);
        actFlags |= ActFlagDelete;
        break;
      case Action.Spam:
        actFlags &= ~(ActFlagSpam|ActFlagHam);
        actFlags |= ActFlagSpam;
        break;
      case Action.Ham:
        actFlags &= ~(ActFlagSpam|ActFlagHam);
        actFlags |= ActFlagHam;
        break;
      case Action.Read:
        actFlags |= ActFlagRead;
        break;
      case Action.Stop:
        actFlags |= ActFlagStop;
        break;
      default:
        {
          import std.conv : to;
          throw new FilterSyntaxException("action "~action.to!string~" should not end up in the handler");
        }
    }
  }

  override bool match (const(char)[] pat, const(char)[] str, bool casesens) {
    version(debug_filter_helper) writeln("...match: casesens=", casesens, "; pat=<", pat, ">; str=<", str, ">");
    immutable bool bol = (pat.length && pat[0] == '^');
    if (bol) pat = pat[1..$];
    immutable bool eol = (pat.length && pat[$-1] == '$');
    if (eol) pat = pat[0..$-1];
    version(debug_filter_helper) writeln("...match:   bol=", bol, "; eol=", eol, "; pat=<", pat, ">");
    if (pat.length == 0) return (bol && eol ? str.length == 0 : false);
    if (str.length < pat.length) return false;
         if (bol && eol) { if (str.length != pat.length) return false; }
    else if (bol) str = str[0..pat.length];
    else if (eol) str = str[str.length-pat.length..$];
    if (casesens) {
      return (str.indexOf(pat) >= 0);
    } else {
      while (str.length >= pat.length) {
        if (str.startsWithCI(pat)) {
          //writeln("...match:  HIT! str=<", str, ">");
          return true;
        }
        str = str[1..$];
        //writeln("...match:  skip; str=<", str, ">; pat=<", pat, ">");
      }
      //writeln("...match:  FAIL!");
      return false;
    }
  }

  void writeResult() () const {
    if (isDelete) write(" softdelete");
    if (isPurge) write(" purge");
    if (isSpam) write(" spam");
    if (isHam) write(" ham");
    if (isRead) write(" read");
    write("; dest tag: ", tag.getData);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared bool updatesDisabled = false;
__gshared bool rcDisabled = false;
__gshared bool rcStarted = false;
__gshared Tid controlThreadId;


enum ControlReply {
  Quit,
}

struct ControlCommand {
  enum Kind {
    Ping,
    ForceUpdateAll,
    Quit,
    //
    CheckDone,
    CheckError,
    //
    DisableUpdates,
    EnableUpdates,
  }
  Kind type;
  // for CheckDone or CheckError
  uint accid;

  @disable this ();
  this (Kind atype) nothrow @safe @nogc { type = atype; accid = 0; }
  this (Kind atype, uint aid) nothrow @safe @nogc { type = atype; accid = aid; }
}


struct CheckCommand {
  uint accid;
}


static stmtAccInfo = LazyStatement!"Conf"(`
  SELECT
      accid AS accid
    , checktime AS checktime
    , nosendauth AS nosendauth
    , debuglog AS debuglog
    , nntplastindex AS nntplastindex
    , name AS name
    , recvserver AS recvserver
    , sendserver AS sendserver
    , user AS user
    , pass AS pass
    , inbox AS inbox
    , nntpgroup AS nntpgroup
    , email AS email
  FROM accounts
  WHERE accid=:accid
  LIMIT 1
;`);


static stmtSetCheckTime = LazyStatement!"Conf"(`
  INSERT INTO checktimes(accid,lastcheck) VALUES(:accid,:lastcheck)
  ON CONFLICT(accid)
  DO UPDATE SET lastcheck=:lastcheck
;`);


//==========================================================================
//
//  forEachTag
//
//  return `false` from delegate to stop
//
//==========================================================================
void forEachTag (const(char)[] tags, bool delegate (const(char)[] tag) dg) {
  if (dg is null) return;
  auto anchor = tags;
  while (tags.length) {
    auto stp = tags.indexOf('|');
    if (stp < 0) stp = cast(uint)tags.length;
    auto tag = tags[0..stp];
    tags = tags[(stp < tags.length ? stp+1 : tags.length)..$];
    if (tag.length == 0) continue;
    if (!dg(tag)) break;
  }
}


//==========================================================================
//
//  extractAccount
//
//==========================================================================
DynStr extractAccount (const(char)[] tags) {
  auto stp = tags.indexOf("account:");
  while (stp >= 0) {
    if (stp == 0 || tags[stp-1] == '|') {
      tags = tags[stp+8..$];
      stp = tags.indexOf('|');
      if (stp >= 0) tags = tags[0..stp];
      return DynStr(tags);
    }
  }
  return DynStr();
}


//==========================================================================
//
//  extractFirstFolder
//
//  can return empty string
//
//==========================================================================
DynStr extractFirstFolder (const(char)[] tags) {
  DynStr res;
  forEachTag(tags, (tag) {
    if (tag[0] != '/') return true; // go on
    res = tag;
    return false; // stop
  });
  return res;
}


//==========================================================================
//
//  removeFirstFolder
//
//  can return empty tags string
//
//==========================================================================
DynStr removeFirstFolder (const(char)[] tags) {
  DynStr res;
  bool seenFolder = false;
  forEachTag(tags, (tag) {
    if (!seenFolder && tag[0] == '/') {
      seenFolder = true;
    } else {
      if (res.length) res ~= "|";
      res ~= tag;
    }
    return true; // go on
  });
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
static struct TagInfo {
  uint tagid;
  DynStr name;
  bool wasUpdates;
}


//==========================================================================
//
//  getMessageTags
//
//==========================================================================
void getMessageTags (ref TagInfo[] tags, uint uid) {
  auto stGetTags = LazyStatement!"View"(`
    SELECT
        DISTINCT(threads.tagid) AS tagid
      , tn.tag AS name
    FROM threads
    INNER JOIN tagnames AS tn USING(tagid)
    WHERE uid=:uid
  ;`);

  tags.reserve(64);
  foreach (auto row; stGetTags.st.bind(":uid", uid).range) {
    tags ~= TagInfo(row.tagid!uint, DynStr(row.name!SQ3Text));
  }
}


//==========================================================================
//
//  updateTwittedThreadsInTag
//
//==========================================================================
void updateTwittedThreadsInTag (uint tagid, uint uid) {
  auto stTempTbl = LazyStatement!"View"(`
    INSERT INTO mutepairs
     WITH RECURSIVE children(muid, paruid, mtagid, mmute) AS (
      SELECT 0, :uid, :tagid, 666
     UNION ALL
      SELECT
        tt.uid, tt.uid, mtagid, tt.mute
      FROM children AS cc
      INNER JOIN threads AS tt ON
        tt.tagid=cc.mtagid AND
        tt.parent=cc.paruid AND
        tt.uid<>cc.muid AND
        tt.uid<>cc.paruid
    )
    SELECT
        muid AS muid
      , mtagid AS mtagid
    FROM children
    WHERE muid<>0 AND mmute=0
  ;`);

  auto stFixMutes = LazyStatement!"View"(`
    UPDATE threads
    SET
        mute=:mute
      , appearance=iif(appearance=0,1,appearance)
    FROM (SELECT muid, mtagid FROM mutepairs) AS cc
    WHERE uid=cc.muid AND tagid=cc.mtagid AND mute=0
  ;`);

  // update threads
  dbView.execute(`DELETE FROM mutepairs;`);
  stTempTbl.st
    .bind(":uid", uid)
    .bind(":tagid", tagid)
    .doAll();
  stFixMutes.st
    .bind(":mute", Mute.ThreadOther)
    .doAll();
}


//==========================================================================
//
//  createTwitByMsgid
//
//==========================================================================
public void createTwitByMsgid (uint uid, const(char)[] glob="/dmars_ng/*") {
  if (!uid) return;
  if (glob.length == 0) return;

  auto stGetMsgid = LazyStatement!"View"(`
    SELECT
      msgid AS msgid
    FROM msgids
    WHERE uid=:uid
    LIMIT 1
  ;`);

  DynStr msgid;
  foreach (auto row; stGetMsgid.st.bind(":uid", uid).range) msgid = row.msgid!SQ3Text;
  if (msgid.length == 0) return;

  auto stFindMsgidTwit = LazyStatement!"Conf"(`
    SELECT
      1
    FROM msgidtwits
    WHERE msgid=:msgid AND tagglob=:glob
    LIMIT 1
  ;`);

  // check if we already have such twit
  foreach (auto row; stFindMsgidTwit.st.bindConstText(":msgid", msgid.getData).bindConstText(":glob", glob).range) return;

  transacted!"Conf"{
    auto stAddMsgidTwit = LazyStatement!"Conf"(`
      INSERT INTO msgidtwits
              (etwitid, automatic, tagglob, msgid)
        VALUES(0, 0,:tagglob,:msgid)
    ;`);

    stAddMsgidTwit.st
      .bindConstText(":tagglob", glob)
      .bindConstText(":msgid", msgid.getData)
      .doAll();
  };

  TagInfo[] tags;
  scope(exit) delete tags;
  getMessageTags(ref tags, uid);
  if (tags.length == 0) return; // just in case

  twitPrepare();

  auto stUpdateMute = LazyStatement!"View"(`
    UPDATE threads
    SET
        mute=:mute
      , title=NULL
      , appearance=iif(appearance=0,1,appearance)
    WHERE uid=:uid AND tagid=:tagid AND mute=0
  ;`);

  // mark the message as twitted
  transacted!"View"{
    foreach (ref TagInfo ti; tags) {
      stUpdateMute.st
        .bind(":uid", uid)
        .bind(":tagid", ti.tagid)
        .bind(":mute", Mute.ThreadStart)
        .doAll();
      updateTwittedThreadsInTag(ti.tagid, uid);
    }
  };
}


//==========================================================================
//
//  twitPrepare
//
//==========================================================================
public void twitPrepare () {
  dbView.execute(`
    CREATE TEMP TABLE IF NOT EXISTS mutepairs(
        muid INTEGER
      , mtagid INTEGER
    );
  `);
}


//==========================================================================
//
//  twitMessage
//
//  set "mute" flag according to message filters
//
//==========================================================================
public void twitMessage (uint uid) {
  if (!uid) return;

  TagInfo[] tags;
  scope(exit) delete tags;
  getMessageTags(ref tags, uid);
  if (tags.length == 0) return; // just in case

  auto stUpdateMute = LazyStatement!"View"(`
    UPDATE threads
    SET
        mute=:mute
      , title=:title
      , appearance=iif(appearance=0,1,appearance)
    WHERE uid=:uid AND tagid=:tagid AND mute=0
  ;`);

  DynStr fromMail, fromName;
  /*
  foreach (auto row; dbView.statement(`
      SELECT
          from_name AS fromName
        , from_mail AS fromMail
      FROM info
      WHERE uid=:uid
      LIMIT 1
    ;`).bind(":uid", uid).range)
  {
    fromMail = row.fromMail!SQ3Text;
    fromName = row.fromName!SQ3Text;
  }
  */
  if (!chiroGetMessageFrom(uid, ref fromMail, ref fromName)) return;

  uint ttcount = 0;
  if (fromMail.length != 0 || fromName.length != 0) {
    foreach (auto trow; dbConf.statement(`
        SELECT
            tagglob AS tagglob
          , email AS email
          , name AS name
          , title AS title
        FROM emailtwits
      ;`).range)
    {
      auto email = trow.email!SQ3Text;
      auto name = trow.name!SQ3Text;
      auto glob = trow.tagglob!SQ3Text;
      if (glob.length == 0 || (!email.length && !name.length)) continue; // just in case
      // check for filter match
      if (email.length && !globmatchCI(fromMail, email)) continue;
      if (name.length && !globmatchCI(fromName, name)) continue;
      auto title = trow.title!SQ3Text;
      // for all tags
      foreach (ref TagInfo ti; tags) {
        if (ti.wasUpdates) continue;
        if (!globmatch(ti.name, glob)) continue;
        stUpdateMute.st
          .bind(":uid", uid)
          .bind(":tagid", ti.tagid)
          .bind(":mute", Mute.ThreadStart)
          .bindConstText(":title", (title.length ? title : null), allowNull:true)
          .doAll();
        ti.wasUpdates = true;
        ++ttcount;
      }
      if (ttcount == tags.length) break;
    }
  }

  // mute it if it is not muted, but should be
  static auto statFindParentFor = LazyStatement!"View"(`
    SELECT mute AS mute, parent AS parent
    FROM threads
    WHERE tagid=:tagid AND uid=:uid
    LIMIT 1
  ;`);

  foreach (TagInfo ti; tags) {
    auto mute = chiroGetMessageMute(ti.tagid, uid);
    if (mute > Mute.Normal) {
      ti.wasUpdates = true; // just in case
      if (!ttcount) ttcount = 1;
      continue;
    }
    uint puid = uid;
    while (puid) {
      statFindParentFor.st
        .bind(":tagid", ti.tagid)
        .bind(":uid", puid);
      puid = 0;
      foreach (auto prow; statFindParentFor.st.range) {
        if (prow.mute!int > Mute.Normal) {
          chiroSetMessageMute(ti.tagid, uid, Mute.ThreadOther);
          ti.wasUpdates = true; // just in case
          if (!ttcount) ttcount = 1;
        } else {
          puid = prow.parent!uint;
        }
      }
    }
  }


  if (!ttcount) return;

  // update threads
  foreach (ref TagInfo ti; tags) {
    if (!ti.wasUpdates) continue;
    updateTwittedThreadsInTag(ti.tagid, uid);
  }
}


//==========================================================================
//
//  updateViewDB
//
//  check for new messages, and update view database
//
//==========================================================================
public void updateViewDB (bool skipFilters=false) {
  uint maxViewUid = 0;
  uint maxStoreUid = 0;

  twitPrepare();

  foreach (auto row; dbView.statement(`SELECT MAX(uid) AS uid FROM info;`).range) maxViewUid = row.uid!uint;
  foreach (auto row; dbStore.statement(`SELECT MAX(uid) AS uid FROM messages;`).range) maxStoreUid = row.uid!uint;

  if (maxViewUid >= maxStoreUid) return;
  conwriteln("need to process around ", maxStoreUid-maxViewUid, " messages.");

  uint[] relinkTids;
  relinkTids.reserve(64);
  scope(exit) delete relinkTids;

  foreach (uint uid; maxViewUid+1..maxStoreUid+1) {
    DynStr msg, tags;
    foreach (auto row; dbStore.statement(`
        SELECT tags AS tags, ChiroUnpack(data) AS data FROM messages WHERE uid=:uid LIMIT 1
      ;`).bind(":uid", uid).range)
    {
      msg = row.data!SQ3Text;
      tags = row.tags!SQ3Text;
    }
    if (msg.length == 0 || tags.length == 0) continue; // not interesting

    conwriteln("============ message #", uid, " ============");

    DynStr acc = tags.extractAccount();
    DynStr origTags = tags;
    RealFilterHelper hlp;
    scope(exit) delete hlp;
    bool markSpamHam = false; //!!!

    if (!skipFilters) {
      DynStr deftag = tags.extractFirstFolder();
      tags = tags.removeFirstFolder();

      hlp = new RealFilterHelper;
      hlp.account = acc;
      hlp.tag = deftag;
      if (hlp.tag.length == 0) hlp.tag = "#hobo";
      hlp.message = msg;
      // filter
      foreach (auto row; dbConf.statement(
                `SELECT filterid AS filterid, name AS name, body AS body`~
                `  FROM filters`~
                `  WHERE valid<>0`~
                `  ORDER BY idx, filterid;`).range) {
        //conwrite("  filter '", row.name!SQ3Text, "' (", row.filterid!uint, "): ");
        bool goOn = false;
        hlp.matched = false;
        try {
          //version(debug_filter_helper) writeln("::: <", row.body!SQ3Text, ">");
          goOn = executeMailFilter(row.body!SQ3Text, hlp);
        } catch (Exception e) {
          conwriteln("ERROR IN FILTER '", row.name!SQ3Text, "': ", e.msg);
        }
        if (hlp.matched) {
          conwriteln("...filter '", row.name!SQ3Text, "' matched!");
          dbConf.statement(`UPDATE filters SET hitcount=hitcount+1 WHERE filterid=:id`)
            .bind(":id", row.filterid!uint)
            .doAll();
        }
        //hlp.writeResult(); writeln;
        //version(debug_filter_helper) writeln("::: <", row.body!SQ3Text, ">: goon=", goOn, "; isstop=", hlp.isStop);
        //assert(hlp.isStop == !goOn);
        if (hlp.isStop) break;
      }
      //write("   FINAL RESULT:"); hlp.writeResult(); writeln;
      // done filtering

      markSpamHam = false; //!!!
      if (!hlp.isSpam && !hlp.isHam) {
        auto bogo = messageBogoCheck(uid);
        if (bogo == Bogo.Spam) {
          bool exists;
          conwriteln("BOGO: SPAM message #", uid, "; from={", hlp.getFromName.getData, "}:<", hlp.getFromMail.getData, ">; to={",
            hlp.getToName.getData, "}:<", hlp.getToMail.getData, ">; subj=", hlp.getSubj(out exists).getData);
          hlp.performAction(hlp.Action.Spam);
          markSpamHam = false;
        }
      }

      if (hlp.isSpam) hlp.tag = "#spam"; // always

      if (hlp.tag.length == 0) hlp.tag = deftag; // just in case
      bool hasTag = false;
      forEachTag(tags, (xtag) {
        if (xtag == hlp.tag) {
          hasTag = true;
          return false; // stop
        }
        return true; // go on
      });

      // `tags` should contain our new tags
      if (!hasTag) {
        DynStr tt = hlp.tag;
        if (tags.length) {
          tt ~= "|";
          tt ~= tags;
        }
        tags = tt;
      }
    }

    // update tags info in the storage
    if (tags != origTags) {
      transacted!"Store"{
        dbStore.statement(`UPDATE messages SET tags=:tags WHERE uid=:uid;`)
          .bind(":uid", uid)
          .bindConstText(":tags", tags.getData)
          .doAll();
      };
    }

    // insert the message into the view db
    int appearance = (skipFilters ? Appearance.Read : Appearance.Unread);
         if (hlp !is null && hlp.isDelete) appearance = Appearance.SoftDeleteFilter;
    else if (hlp !is null && hlp.isPurge) appearance = Appearance.SoftDeletePurge;
    if (hlp !is null && appearance == Appearance.Unread && (hlp.isRead || hlp.isSpam)) appearance = Appearance.Read;
    if (hlp !is null && markSpamHam) {
      if (hlp.isSpam) messageBogoMarkSpam(uid);
      if (hlp.isHam) messageBogoMarkHam(uid);
    }

    uint msgtime = 0;
    DynStr hdr, body;
    foreach (auto trow; dbStore.statement(`
        SELECT
          ChiroExtractHeaders(:msgdata) AS headers
        , ChiroExtractBody(:msgdata) AS body
        , ChiroHdr_RecvTime(:msgdata) AS msgtime
      ;`).bindConstText(":msgdata", msg.getData).range)
    {
      msgtime = trow.msgtime!uint;
      hdr = trow.headers!SQ3Text;
      body = trow.body!SQ3Text;
    }

    conwriteln("putting msg ", uid, " (time:", msgtime, "; appr=", appearance, ") to '", tags.getData, "'; oldtags='", origTags.getData, "'");

    transacted!"View"{
      //dbView.beginTransaction();
      //scope(success) dbView.commitTransaction();
      //scope(failure) dbView.rollbackTransaction();
      chiroParseAndInsertOneMessage(uid, msgtime, appearance, hdr, body, tags);

      // custom filters, not in the repository
      static if (HasExtFilter) {
        extfilter.ExtInTrans(hdr, body, tags, uid);
      }

      DynStr msgid;
      foreach (auto mrow; dbView.statement(`SELECT msgid AS msgid FROM msgids WHERE uid=:uid LIMIT 1;`).bind(":uid", uid).range) {
        msgid = mrow.msgid!SQ3Text;
      }
      //if (msgid.length == 0) return;
      version(debug_updater) {
        {
          auto fo = VFile("zzz", "a");
          fo.writeln("MSGUID: ", uid, "; MSGID: <", msgid.getData, ">");
        }
      }
      conwriteln("MSGUID: ", uid, "; MSGID: <", msgid.getData, ">");

      // collect tags to modify
      int[] taglist;
      scope(exit) delete taglist;
      taglist.reserve(16);

      foreach (auto trow; dbView.statement(`SELECT tagid AS tagid, parent AS parent FROM threads WHERE uid=:uid;`)
          .bind(":uid", uid).range)
      {
        immutable uint tid = trow.tagid!uint;
        if (tid) {
          bool found = false;
          foreach (immutable uint tt; relinkTids) if (tt == tid) { found = true; break; }
          if (!found) relinkTids ~= tid;
        }
        if (!tid || trow.parent!uint || !chiroIsTagThreaded(tid)) continue;
        conwriteln(" tagid: ", tid, " (", chiroGetTagName(tid).getData, ")");
        version(debug_updater) {
          {
            auto fo = VFile("zzz", "a");
            fo.writeln(" tagid: ", tid, " (", chiroGetTagName(tid).getData, ")");
          }
        }
        taglist ~= tid;
      }

      foreach (immutable uint tid; taglist) {
        uint setUsAsParentFor = 0;
        bool needFullRelink = false;
        // check if there are any references to us, and fix them by full relink
        if (!msgid.length) continue;
        foreach (auto nrow; dbView.statement(`
            SELECT refids.uid AS uid, tt.parent AS parent
            FROM refids
            INNER JOIN(threads) AS tt
              ON tt.tagid=:tagid AND tt.uid=refids.uid
            WHERE idx=0 AND msgid=:msgid
            LIMIT 1
          ;`).bind(":tagid", tid).bindConstText(":msgid", msgid.getData).range)
        {
          if (nrow.parent!uint == 0) {
            setUsAsParentFor = nrow.uid!uint;
          } else {
            needFullRelink = true;
          }
        }

        if (needFullRelink) {
          //FIXME: make this faster!
          conwriteln("  tid: ", tid, " (", chiroGetTagName(tid).getData, "); performing full relink...");
          chiroSupportRelinkTagThreads(tid);
          continue;
        }

        if (setUsAsParentFor) {
          conwriteln("  tid: ", tid, " (", chiroGetTagName(tid).getData, "); settuing us (", uid, ") as a parent for ", setUsAsParentFor);
          dbView.statement(`
            UPDATE threads
            SET parent=:uid
            WHERE uid=:xuid AND tagid=:tagid
          ;`).bind(":uid", uid).bind(":xuid", setUsAsParentFor).bind(":tagid", tid).doAll();
        }

        // find parent for us
        uint paruid = 0;
        foreach (auto prow; dbView.statement(`
            SELECT msgids.uid AS paruid
            FROM msgids
            INNER JOIN(threads) AS tt
              ON tt.tagid=:tagid AND tt.uid=msgids.uid
            WHERE msgids.uid<>:uid AND msgids.msgid IN (SELECT msgid FROM refids WHERE uid=:uid ORDER BY idx)
            LIMIT 1
          ;`).bind(":uid", uid).bind(":tagid", tid).range)
        {
          paruid = prow.paruid!uint;
        }
        conwriteln("  tid: ", tid, " (", chiroGetTagName(tid).getData, "); paruid=", paruid);
        if (paruid && paruid != uid) {
          dbView.statement(`UPDATE threads SET parent=:paruid WHERE uid=:uid AND tagid=:tagid;`)
            .bind(":uid", uid)
            .bind(":tagid", tid)
            .bind(":paruid", paruid)
            .doAll();
        }
      }

      twitMessage(uid);
    };
  }

  // relink threads
  if (relinkTids.length) {
    foreach (immutable uint tid; relinkTids) {
      if (vbwin && !vbwin.closed) vbwin.postEvent(new TagThreadsUpdatedEvent(tid));
    }
  }
}


//==========================================================================
//
//  checkerThread
//
//==========================================================================
void checkerThread (Tid ownerTid) {
  uint accid = 0;
  bool isError = false;
  try {
    receive(
      (CheckCommand cmd) {
        accid = cmd.accid;
      }
    );

    if (accid == 0) {
      ownerTid.send(ControlCommand(ControlCommand.Kind.CheckError, accid));
      return;
    }

    bool found = false;
    int checktime;
    bool nosendauth;
    bool debuglog;
    uint nntplastindex;
    DynStr name;
    DynStr recvserver;
    DynStr sendserver;
    DynStr user;
    DynStr pass;
    DynStr inbox;
    DynStr nntpgroup;
    DynStr xemail;

    foreach (auto arow; stmtAccInfo.st.bind(":accid", accid).range) {
      // i found her!
      found = true;
      int upmins = arow.checktime!int;
      if (upmins < 1) upmins = 1; else if (upmins > 100000) upmins = 100000;
      checktime = upmins;
      nosendauth = (arow.nosendauth!int > 0);
      debuglog = (arow.debuglog!int > 0);
      nntplastindex = arow.nntplastindex!uint;
      name = arow.name!SQ3Text;
      recvserver = arow.recvserver!SQ3Text;
      sendserver = arow.sendserver!SQ3Text;
      user = arow.user!SQ3Text;
      pass = arow.pass!SQ3Text;
      inbox = arow.inbox!SQ3Text;
      nntpgroup = arow.nntpgroup!SQ3Text;
      xemail = arow.email!SQ3Text;
    }

    if (!found) {
      ownerTid.send(ControlCommand(ControlCommand.Kind.CheckError, accid));
      return;
    }

    struct ToSend {
      uint uid;
      dynstring from;
      dynstring to;
      dynstring data;
      bool sent;
    }
    ToSend[] sendQueue;
    scope(exit) {
      foreach (ref ToSend ss; sendQueue) { ss.from.clear; ss.to.clear; ss.data.clear; }
      sendQueue.length = 0;
    }

    //FIXME: nntp sends!
    if (sendserver.length && (nntpgroup.length != 0 || xemail.length != 0)) {
      // check if we have something to send
      foreach (auto srow; dbView.statement(`
          SELECT uid AS uid, from_pop3 AS from_pop3, to_pop3 AS to_pop3, ChiroUnpack(data) AS data
          FROM unsent
          WHERE accid=:accid AND sendtime=0
        ;`).bind(":accid", accid).range)
      {
        ToSend ss;
        ss.uid = srow.uid!uint;
        ss.from = srow.from_pop3!SQ3Text;
        ss.to = srow.to_pop3!SQ3Text;
        ss.data = srow.data!SQ3Text;
        sendQueue ~= ss;
      }
    }

    //FIXME: batch send!
    if (sendQueue.length) {
      conwriteln("sending ", sendQueue.length, " message", (sendQueue.length == 1 ? "" : "s"));
      foreach (ref ToSend ss; sendQueue) {
        try {
          if (nntpgroup.length == 0) {
            conwriteln("*** [", name, "]: connecting... (smtp)");
            SocketSMTP nsk = new SocketSMTP(sendserver.idup);
            scope(exit) { nsk.close(); delete nsk; }
            if (!nosendauth) {
              conwriteln("[", name, "]: authenticating...");
              nsk.auth(xemail.getData, user.getData, pass.getData);
            }
            conwriteln("[", name, "]: sending (uid=", ss.uid, ")...");
            nsk.sendMessage(ss.from.getData, ss.to.getData, ss.data.getData);
            nsk.close();
            conwriteln("[", name, "]: closing...");
          } else {
            conwriteln("*** [", name, "]: connecting... (nntp)");
            SocketNNTP nsk = new SocketNNTP(recvserver.idup);
            scope(exit) { nsk.close(); delete nsk; }
            conwriteln("[", name, "]: selecting group (", nntpgroup, ")");
            nsk.selectGroup(nntpgroup.getData);
            conwriteln("[", name, "]: sending (uid=", ss.uid, ")...");
            nsk.doSend("POST");
            nsk.doSendRaw(ss.data.getData);
            conwriteln("[", name, "]: getting answer...");
            auto ln = nsk.readLine;
            conwriteln("[", name, "]:   ", ln); // 340 Ok, recommended message-ID <o7dq4o$mpm$1@digitalmars.com>
            if (ln.length == 0 || ln[0] != '3') throw new Exception(ln.idup);
            conwriteln("[", name, "]: closing...");
            nsk.close();
          }
          ss.sent = true;
        } catch (Exception e) {
          conwriteln("SENDING ERROR: ", e.msg);
        }
      }

      // mark sent messages
      transacted!"View"{
        foreach (ref ToSend ss; sendQueue) {
          if (ss.sent) {
            dbView.statement(`
              UPDATE unsent
              SET sendtime=CAST(strftime('%s','now') AS INTEGER), lastsendtime=CAST(strftime('%s','now') AS INTEGER)
              WHERE uid=:uid
            ;`).bind(":uid", ss.uid).doAll();
          } else {
            dbView.statement(`
              UPDATE unsent
              SET lastsendtime=CAST(strftime('%s','now') AS INTEGER)
              WHERE uid=:uid
            ;`).bind(":uid", ss.uid).doAll();
          }
        }
      };
    }


    conwriteln("checking account '", name, "' (", accid, ")...");

    stmtSetCheckTime.st.bind(":accid", accid).bind(":lastcheck", RunningAverageExp.GetTickCount()+checktime*60).doAll();

    // ////////////////////////////////////////////////////////////////// //
    void CheckNNTP () {
      auto nsk = new SocketNNTP(recvserver.idup);
      scope(exit) { nsk.close(); delete nsk; }

      nsk.selectGroup(nntpgroup);
      if (nsk.emptyGroup) {
        conwriteln("[", name, ":", nntpgroup, "]: no new articles (empty group)");
        return;
      }

      uint stnum = nntplastindex+1;
      if (stnum > nsk.hiwater) {
        conwriteln("[", name, ":", nntpgroup, "]: no new articles");
        return;
      }

      conwriteln("[", name, ":", nntpgroup, "]: ", nsk.hiwater+1-stnum, " (possible) new articles");

      // download new articles
      foreach (immutable uint anum; stnum..nsk.hiwater+1) {
        DynStr msg;
        try {
          msg = nsk.getArticle(anum);
        } catch (Exception e) {
          conwriteln("[", name, ":", nntpgroup, "]: error downloading article #", anum);
          break;
        }
        if (msg.length == 0) continue; // this article is empty
        // insert article into the storage
        // filtering will be done later, for now, insert with the default inbox
        DynStr tags;
        if (inbox.length) tags ~= inbox;
        if (name.length) {
          if (tags.length) tags ~= "|";
          tags ~= "account:";
          tags ~= name;
        }
        if (tags.length == 0) tags = "#hobo";
        conwriteln("[", name, ":", nntpgroup, "]: storing article #", anum, " for '", tags.getData, "'...");
        transacted!"Store"{
          dbStore.statement(`INSERT INTO messages(tags, data) VALUES(:tags, ChiroPack(:data));`)
            .bindConstText(":tags", tags.getData)
            .bindConstBlob(":data", msg.getData)
            .doAll();
        };
        // update account with the new highest nntp index
        transacted!"Conf"{
          dbConf.statement(`UPDATE accounts SET nntplastindex=:anum WHERE accid=:accid;`)
            .bind(":accid", accid)
            .bind(":anum", anum)
            .doAll();
        };
      }
    }

    // ////////////////////////////////////////////////////////////////// //
    void CheckSMTP () {
      conwriteln("*** [", name, "]: connecting...");
      auto pop3 = new SocketPOP3(recvserver.idup);
      scope(exit) { pop3.close(); delete pop3; }
      if (user.length) {
        conwriteln("[", name, "]: authenticating...");
        pop3.auth(user, pass);
      }
      auto newmsg = pop3.getNewMailCount;
      if (newmsg == 0) {
        conwriteln("[", name, "]: no new messages");
        return;
      }
      conwriteln("[", name, "]: ", newmsg, " new message", (newmsg > 1 ? "s" : ""));
      foreach (immutable int popidx; 1..newmsg+1) {
        DynStr msg;
        try {
          msg = pop3.getMessage(popidx); // full message, with the ending dot
        } catch (Exception e) {
          conwriteln("[", name, "]: error downloading message #", popidx);
          break;
        }
        if (msg.length != 0) {
          DynStr tags;
          if (inbox.length) tags ~= inbox;
          if (name.length) {
            if (tags.length) tags ~= "|";
            tags ~= "account:";
            tags ~= name;
          }
          if (tags.length == 0) tags = "#hobo";
          conwriteln("[", name, ":", nntpgroup, "]: storing message #", popidx, " for '", tags.getData, "'...");
          transacted!"Store"{
            dbStore.statement(`INSERT INTO messages(tags, data) VALUES(:tags, ChiroPack(:data));`)
              .bindConstText(":tags", tags.getData)
              .bindConstBlob(":data", msg.getData)
              .doAll();
          };
        }
        //auto msg = pop3.getMessage!true(popidx); // full message, with the ending dot, and exact terminators
        // process
        pop3.deleteMessage(popidx);
      }
    }

    // ////////////////////////////////////////////////////////////////// //
    try {
      if (nntpgroup.length) CheckNNTP(); else CheckSMTP();
    } catch (Throwable e) {
      conwriteln("ERROR checking account '", name, "' (", accid, "): ", e.msg);
      isError = true;
    }

    conwriteln("done checking account '", name, "' (", accid, ")...");

    if (vbwin && !vbwin.closed) {
      //{ import core.stdc.stdio : fprintf, stderr; fprintf(stderr, "POSTING %s: accid=%u\n", (isError ? "ERROR".ptr : "DONE".ptr), accid); }
      vbwin.postEvent(new UpdatingAccountCompleteEvent(accid));
      //{ import core.stdc.stdio : fprintf, stderr; fprintf(stderr, "POSTED %s: accid=%u\n", (isError ? "ERROR".ptr : "DONE".ptr), accid); }
      //sqlite3_sleep(1000);
    }
  } catch (Throwable e) {
    // here, we are dead and fucked (the exact order doesn't matter)
    //import core.stdc.stdlib : abort;
    import core.stdc.stdio : fprintf, stderr;
    //import core.memory : GC;
    import core.thread : thread_suspendAll;
    //GC.disable(); // yeah
    //thread_suspendAll(); // stop right here, you criminal scum!
    auto s = e.toString();
    fprintf(stderr, "\n=== FATAL ===\n%.*s\n", cast(uint)s.length, s.ptr);
    //abort(); // die, you bitch!
    ownerTid.send(ControlCommand(ControlCommand.Kind.CheckError, accid));
    return;
  }
  //if (vbwin) vbwin.postEvent(evDoConCommands); }
  //{ import core.stdc.stdio : fprintf, stderr; fprintf(stderr, "SENDING %s: accid=%u\n", (isError ? "ERROR".ptr : "DONE".ptr), accid); }
  ownerTid.send(ControlCommand((isError ? ControlCommand.Kind.CheckError : ControlCommand.Kind.CheckDone), accid));
  //{ import core.stdc.stdio : fprintf, stderr; fprintf(stderr, "SENDT %s: accid=%u\n", (isError ? "ERROR".ptr : "DONE".ptr), accid); }
  {
    import core.memory : GC;
    GC.collect();
    GC.minimize();
  }
  //{ import core.stdc.stdio : fprintf, stderr; fprintf(stderr, "DONE with accid=%u\n", accid); }
}


//==========================================================================
//
//  controlThread
//
//==========================================================================
void controlThread (Tid ownerTid) {
  import core.time;
  bool doQuit = false;
  try {
    static struct AccCheck {
      uint accid;
      bool inprogress;
      Tid tid;
    }

    AccCheck[] accidCheckList;
    accidCheckList.reserve(128);

    dbConf.execute(`
      CREATE TEMP TABLE IF NOT EXISTS checktimes (
          accid INTEGER PRIMARY KEY UNIQUE      /* unique, never zero */
        , lastcheck INTEGER NOT NULL DEFAULT 0
        , checking INTEGER NOT NULL DEFAULT 0
      );
    `);

    static stmtAllAccs = LazyStatement!"Conf"(`
      SELECT
          accid AS accid
        , checktime AS checktime
      FROM accounts
      WHERE nocheck=0 AND inbox<>''
      ORDER BY accid
    ;`);

    static stmtGetCheckTime = LazyStatement!"Conf"(`
      SELECT lastcheck AS lastcheck FROM checktimes WHERE accid=:accid LIMIT 1
    ;`);

    updateViewDB();

    MonoTime lastCollect = MonoTime.currTime;
    //accidCheckList ~= AccCheck();

    bool needUpdates = false;
    bool forceAll = false;
    for (;;) {
      if (doQuit && accidCheckList.length == 0) break;
      receiveTimeout((doQuit ? 50.msecs : accidCheckList.length || needUpdates || forceAll ? 1.seconds : 60.seconds),
        (ControlCommand cmd) {
          final switch (cmd.type) {
            case ControlCommand.Kind.ForceUpdateAll: forceAll = true; break;
            case ControlCommand.Kind.Ping: break;
            case ControlCommand.Kind.Quit: doQuit = true; break;
            case ControlCommand.Kind.DisableUpdates: updatesDisabled = true; break;
            case ControlCommand.Kind.EnableUpdates: updatesDisabled = false; break;
            case ControlCommand.Kind.CheckDone:
            case ControlCommand.Kind.CheckError:
              needUpdates = true;
              if (accidCheckList.length) {
                foreach (immutable idx, const ref AccCheck nfo; accidCheckList) {
                  if (nfo.accid == cmd.accid) {
                    //if (!doQuit && vbwin && !vbwin.closed) vbwin.postEvent(new UpdatingAccountCompleteEvent(nfo.accid));
                    foreach (immutable c; idx+1..accidCheckList.length) accidCheckList[c-1] = accidCheckList[c];
                    accidCheckList.length -= 1;
                    break;
                  }
                }
                if (!doQuit && vbwin && !vbwin.closed && accidCheckList.length == 0) vbwin.postEvent(new UpdatingCompleteEvent());
              }
              break;
          }
        },
      );

      for (usize idx = 0; idx < accidCheckList.length; ) {
        if (accidCheckList[idx].accid != 0) {
          ++idx;
        } else {
          foreach (immutable c; idx+1..accidCheckList.length) accidCheckList[c-1] = accidCheckList[c];
          accidCheckList.length -= 1;
        }
      }

      if (doQuit) {
        for (usize idx = 0; idx < accidCheckList.length; ) {
          if (accidCheckList[idx].inprogress) {
            ++idx;
          } else {
            foreach (immutable c; idx+1..accidCheckList.length) accidCheckList[c-1] = accidCheckList[c];
            accidCheckList.length -= 1;
          }
        }
        continue;
      }

      if (!needUpdates && !updatesDisabled) {
        ulong ctt = RunningAverageExp.GetTickCount();
        foreach (auto arow; stmtAllAccs.st.range) {
          bool found = false;
          foreach (const ref AccCheck nfo; accidCheckList) if (nfo.accid == arow.accid!uint) { found = true; break; }
          if (found) continue;
          // forced update?
          if (forceAll) {
            accidCheckList ~= AccCheck(arow.accid!uint);
            continue;
          }
          // check timeout
          int upmins = arow.checktime!int;
          if (upmins < 1) upmins = 1; else if (upmins > 100000) upmins = 100000;
          ulong lastcheck = 0;
          foreach (auto crow; stmtGetCheckTime.st.bind(":accid", arow.accid!uint).range) lastcheck = crow.lastcheck!ulong;
          lastcheck += upmins*60; // next check time
          if (lastcheck < ctt) {
            // i found her!
            accidCheckList ~= AccCheck(arow.accid!uint);
          }
          /*
          else {
            conwriteln("check for accid ", arow.accid!uint, " in ", (lastcheck-ctt)/60, " minutes...");
          }
          */
        }
        forceAll = false;
      }

      if (!updatesDisabled) {
        foreach (ref AccCheck nfo; accidCheckList) {
          if (nfo.inprogress) break;
          if (vbwin) vbwin.postEvent(new UpdatingAccountEvent(nfo.accid));
          nfo.tid = spawn(&checkerThread, thisTid);
          nfo.inprogress = true;
          nfo.tid.send(CheckCommand(nfo.accid));
          break;
        }
      }

      bool hasProgress = false;
      foreach (ref AccCheck nfo; accidCheckList) if (nfo.inprogress) { hasProgress = true; break; }
      if (!hasProgress) {
        updateViewDB();
        needUpdates = false;
      }

      if (!doQuit) {
        immutable ctt = MonoTime.currTime;
        if ((ctt-lastCollect).total!"minutes" >= 1) {
          import core.memory : GC;
          lastCollect = ctt;
          GC.collect();
          GC.minimize();
        }
      }
    }
    ownerTid.send(ControlReply.Quit);
  } catch (Throwable e) {
    // here, we are dead and fucked (the exact order doesn't matter)
    import core.stdc.stdlib : abort;
    import core.stdc.stdio : fprintf, stderr;
    import core.memory : GC;
    import core.thread : thread_suspendAll;
    GC.disable(); // yeah
    thread_suspendAll(); // stop right here, you criminal scum!
    auto s = e.toString();
    fprintf(stderr, "\n=== FATAL ===\n%.*s\n", cast(uint)s.length, s.ptr);
    abort(); // die, you bitch!
  }
}


//==========================================================================
//
//  receiverDisable
//
//==========================================================================
public void receiverDisable () {
  rcDisabled = true;
}


//==========================================================================
//
//  disableMailboxUpdates
//
//==========================================================================
public void disableMailboxUpdates () {
  if (!rcStarted) return;
  controlThreadId.send(ControlCommand(ControlCommand.Kind.DisableUpdates));
}


//==========================================================================
//
//  enableMailboxUpdates
//
//==========================================================================
public void enableMailboxUpdates () {
  if (!rcStarted) return;
  controlThreadId.send(ControlCommand(ControlCommand.Kind.EnableUpdates));
}


//==========================================================================
//
//  receiverForceUpdateAll
//
//==========================================================================
public void receiverForceUpdateAll () {
  if (!rcStarted) return;
  controlThreadId.send(ControlCommand(ControlCommand.Kind.ForceUpdateAll));
}


//==========================================================================
//
//  receiverInit
//
//==========================================================================
public void receiverInit () {
  if (rcStarted) return;
  if (rcDisabled) return;
  controlThreadId = spawn(&controlThread, thisTid);
  rcStarted = true;
}


//==========================================================================
//
//  receiverDeinit
//
//==========================================================================
public void receiverDeinit () {
  if (!rcStarted) return;
  controlThreadId.send(ControlCommand(ControlCommand.Kind.Quit));
  bool done = false;
  while (!done) {
    receive(
      (ControlReply reply) {
        if (reply == ControlReply.Quit) done = true;
      }
    );
  }
}
