/* E-Mail Client
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module chiroptera /*is aliced*/;

//version = test_round_rect;
version = article_can_into_html;

import core.atomic;
import core.time;
import std.concurrency;

//import arsd.email;
//import arsd.htmltotext;
import arsd.simpledisplay;
import arsd.png;

version(article_can_into_html) {
  import arsd.characterencodings;
  import arsd.color;
  import arsd.dom;
  import arsd.htmltotext;
}

import iv.alice;
import iv.bclamp;
import iv.encoding;
import iv.cmdcon;
import iv.cmdcongl;
import iv.lockfile;
import iv.sdpyutil;
import iv.strex;
import iv.sq3;
import iv.timer : DurTimer = Timer;
import iv.utfutil;
import iv.vfs.io;
import iv.vfs.util;

import iv.egra;

import chibackend;
import chibackend.net;

import chievents;
import dialogs;
import receiver;


// ////////////////////////////////////////////////////////////////////////// //
static immutable string ChiroStyle = `
MainPaneWindow {
  //// group list ////
  grouplist-divline: white;
  grouplist-dots: rgb(80, 80, 80);

  grouplist-back: #222;
  // group with unread messages
  grouplist-unread-text: #0ff;
  // normal group
  grouplist-normal-text: rgb(255, 187, 0);
  grouplist-normal-child-text: rgb(225, 97, 0);
  // spam group
  grouplist-spam-text: #800;
  // main accounts group
  grouplist-accounts-text: rgb(220, 220, 0);
  grouplist-accounts-child-text: rgb(160, 160, 250);
  // account inbox group
  grouplist-inbox-text: rgb(90, 90, 180);


  grouplist-cursor-back: #088;
  // group with unread messages
  grouplist-cursor-unread-text: #0ff;
  // normal group
  grouplist-cursor-normal-text: rgb(255, 187, 0);
  grouplist-cursor-normal-child-text: rgb(225, 97, 0);
  // spam group
  grouplist-cursor-spam-text: #800;
  // main accounts group
  grouplist-cursor-accounts-text: rgb(220, 220, 0);
  grouplist-cursor-accounts-child-text: rgb(160, 160, 250);
  // account inbox group
  grouplist-cursor-inbox-text: rgb(90, 90, 180);

  grouplist-cursor-outline: black;

  //// thread list ////
  threadlist-divline: white;
  threadlist-back: #222;
  threadlist-dots: #444;

  threadlist-normal-back: transparent;
  threadlist-normal-dots: #444;
  threadlist-normal-subj-text: rgb(215, 87, 0);
  threadlist-normal-from-text: rgb(215, 87, 0);
  threadlist-normal-mail-text: rgb(155, 27, 0);
  threadlist-normal-time-text: rgb(215, 87, 0);
  threadlist-normal-strike-line: transparent;

  threadlist-unread-back: transparent;
  threadlist-unread-dots: #444;
  threadlist-unread-subj-text: white;
  threadlist-unread-from-text: white;
  threadlist-unread-mail-text: yellow;
  threadlist-unread-time-text: white;
  threadlist-unread-strike-line: transparent;

  threadlist-soft-del-back: transparent;
  threadlist-soft-del-dots: #444;
  threadlist-soft-del-subj-text: #800;
  threadlist-soft-del-from-text: #800;
  threadlist-soft-del-mail-text: #800;
  threadlist-soft-del-time-text: #800;
  threadlist-soft-del-strike-line: #800;

  threadlist-hard-del-back: transparent;
  threadlist-hard-del-dots: #444;
  threadlist-hard-del-subj-text: red;
  threadlist-hard-del-from-text: red;
  threadlist-hard-del-mail-text: red;
  threadlist-hard-del-time-text: red;
  threadlist-hard-del-strike-line: red;

  threadlist-twit-back: transparent;
  threadlist-twit-dots: #444;
  threadlist-twit-subj-text: #400;
  threadlist-twit-from-text: #400;
  threadlist-twit-mail-text: #400;
  threadlist-twit-time-text: #400;
  threadlist-twit-strike-line: transparent;

  threadlist-cursor-normal-back: #088;
  threadlist-cursor-dots: #444;
  threadlist-cursor-normal-subj-text: rgb(215, 87, 0);
  threadlist-cursor-normal-from-text: rgb(215, 87, 0);
  threadlist-cursor-normal-mail-text: rgb(155, 27, 0);
  threadlist-cursor-normal-time-text: rgb(215, 87, 0);
  threadlist-cursor-normal-strike-line: transparent;
  threadlist-cursor-normal-outline: black;

  threadlist-cursor-unread-back: #088;
  threadlist-cursor-unread-dots: #444;
  threadlist-cursor-unread-subj-text: white;
  threadlist-cursor-unread-from-text: white;
  threadlist-cursor-unread-mail-text: yellow;
  threadlist-cursor-unread-time-text: white;
  threadlist-cursor-unread-strike-line: transparent;
  threadlist-cursor-unread-outline: black;

  threadlist-cursor-soft-del-back: #066;
  threadlist-cursor-soft-del-dots: #444;
  threadlist-cursor-soft-del-subj-text: #800;
  threadlist-cursor-soft-del-from-text: #800;
  threadlist-cursor-soft-del-mail-text: #800;
  threadlist-cursor-soft-del-time-text: #800;
  threadlist-cursor-soft-del-strike-line: #800;

  threadlist-cursor-hard-del-back: #066;
  threadlist-cursor-hard-del-dots: #444;
  threadlist-cursor-hard-del-subj-text: red;
  threadlist-cursor-hard-del-from-text: red;
  threadlist-cursor-hard-del-mail-text: red;
  threadlist-cursor-hard-del-time-text: red;
  threadlist-cursor-hard-del-strike-line: red;

  threadlist-cursor-twit-back: #066;
  threadlist-cursor-twit-dots: #444;
  threadlist-cursor-twit-subj-text: #400;
  threadlist-cursor-twit-from-text: #400;
  threadlist-cursor-twit-mail-text: #400;
  threadlist-cursor-twit-time-text: #400;
  threadlist-cursor-twit-strike-line: #400;


  //// message header ////
  msg-header-back: rgb(20, 20, 20);
  msg-header-from: #088;
  msg-header-to: #088;
  msg-header-subj: #088;
  msg-header-date: #088;
  msg-header-divline: #bbb;


  //// message text ////
  msg-text-back: rgb(37, 37, 37);
  msg-text-text: rgb(174, 174, 174);
  msg-text-quote0: #880;
  msg-text-quote1: #088;
  msg-text-link: rgb(0, 200, 200);
  msg-text-html-sign: rgb(128, 0, 128);

  msg-text-link-hover: rgb(0, 255, 255);

  msg-text-link-pressed: rgb(255, 0, 255);

  //// message text twit ////
  //twit-shade: rgba(0, 0, 80, 127);
  twit-shade: rgb(0, 0, 40);
  twit-text: red;
  twit-outline: black;
}


HintWindow {
  frame: white;
  title-back: white;
  title-text: black;

  back: rgb(0, 0, 80);
  text: rgb(155, 155, 155);
}


MessageWindow {
  frame: white;
  title-back: white;
  title-text: black;

  back: rgb(0, 0, 80);
  text: rgb(255, 255, 0);
  bar-back: rgb(90, 90, 180);
  back: red;
}
`;


// ////////////////////////////////////////////////////////////////////////// //
private __gshared bool ChiroTimerExEnabled = false;


// ////////////////////////////////////////////////////////////////////////// //
public __gshared string mailRootDir = "/mnt/bigass/Mail";


shared static this () {
  import core.stdc.stdlib : getenv;
  const(char)* home = getenv("HOME");
  if (home !is null && home[0] == '/' && home[1] && home[1] != '/') {
    import std.string : fromStringz;
    mailRootDir = home.fromStringz.idup;
    if (mailRootDir.length == 0) assert(0, "wtf?!");
    if (mailRootDir[$-1] != '/') mailRootDir ~= "/";
    mailRootDir ~= "Mail";
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared NotificationAreaIcon trayicon;
__gshared Image[6] trayimages;
__gshared MemoryImage[6] icons; // 0: normal


// ////////////////////////////////////////////////////////////////////////// //
// with tagid
struct ArticleId {
  uint tagid = 0;
  uint uid = 0;

  bool valid () const nothrow @safe @nogc { pragma(inline, true); return (tagid && uid); }
  void clear () nothrow @safe @nogc { pragma(inline, true); tagid = uid = 0; }

  bool opEqual (const ref ArticleId other) const nothrow @safe @nogc {
    pragma(inline, true);
    return (valid && other.valid && tagid == other.tagid && uid == other.uid);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
static immutable ubyte[] iconsZipData = cast(immutable(ubyte)[])import("databin/icons.zip");


// ////////////////////////////////////////////////////////////////////////// //
struct MailReplacement {
  string oldmail;
  string newmail;
  string newname;
}

__gshared MailReplacement[string] repmailreps;


// ////////////////////////////////////////////////////////////////////////// //
string getBrowserCommand (bool forceOpera) {
  __gshared string browser;
  if (forceOpera) return "opera";
  if (browser.length == 0) {
    import core.stdc.stdlib : getenv;
    const(char)* evar = getenv("BROWSER");
    if (evar !is null && evar[0]) {
      import std.string : fromStringz;
      browser = evar.fromStringz.idup;
    } else {
      browser = "opera";
    }
  }
  return browser;
}


// ////////////////////////////////////////////////////////////////////////// //
/*
private void setXStr (ref char[] dest, SQ3Text src) {
  delete dest;
  if (src.length == 0) return;
  dest = new char[src.length];
  dest[] = src[];
}
*/


// ////////////////////////////////////////////////////////////////////////// //
class FolderInfo {
  uint tagid; // 0 means "ephemeral"
  DynStr name;
  const(char)[] visname; // slice of the `name`
  int depth;
  uint unreadCount;
  // used in rescanner;
  bool seen;

  ~this () nothrow @trusted @nogc { clear(); }

  void clear () nothrow @trusted @nogc {
    visname = null;
    name.clear();
  }

  // ephemeral folders doesn't exist, they are here only for visual purposes
  bool ephemeral () const nothrow @safe @nogc { pragma(inline, true); return (tagid == 0); }

  void calcDepthVisName () {
    depth = 0;
    visname = name[0..$];
    if (visname.length == 0 || visname == "/" || visname[0] == '#') return;
    visname = visname[1..$];
    foreach (immutable char ch; visname) if (ch == '/') ++depth;
    auto spos = visname.lastIndexOf('/');
    if (spos >= 0) visname = visname[spos+1..$];
  }

  static int findByFullName (const(char)[] aname) {
    if (aname.length == 0) return -1;
    foreach (immutable idx, const FolderInfo fi; folderList) {
      if (fi.name == aname) return cast(int)idx;
    }
    return -1;
  }

  bool needEphemeral () const {
    if (depth == 0) return false;
    assert(name.length > 1 && name[0] == '/');
    const(char)[] n = name[0..$];
    while (n.length) {
      if (findByFullName(n) < 0) return true;
      auto spos = n.lastIndexOf('/');
      if (spos <= 0) return false;
      n = n[0..spos];
    }
    return false;
  }

  void createEphemerals () const {
    if (depth == 0) return;
    assert(name.length > 1 && name[0] == '/');
    const(char)[] n = name[0..$];
    while (n.length) {
      auto spos = n.lastIndexOf('/');
      if (spos <= 0) break;
      n = n[0..spos];
      //conwriteln(" n=<", n, ">; spos=", spos);
      if (findByFullName(n) < 0) {
        //conwriteln("  creating: '", n, "'");
        //foreach (const FolderInfo nfi; folderList) conwriteln("    <", nfi.name, "> : <", nfi.visname, "> : ", nfi.depth);
        FolderInfo newfi = new FolderInfo;
        newfi.tagid = 0;
        newfi.name = n;
        newfi.unreadCount = 0;
        newfi.seen = true;
        newfi.calcDepthVisName();
        folderList ~= newfi;
      }
    }
  }
}

__gshared FolderInfo[] folderList;
__gshared uint folderDataVersion = uint.max;


//FIXME: make this faster
//FIXME: force-append account folders
// returns `true` if something was changed
bool rescanFolders () {
  import std.conv : to;
  bool res = false;

  if (folderList.length != 0 && dbView.getDataVersion() == folderDataVersion) return false; // nothing to do here

  bool needsort = false;
  foreach (FolderInfo fi; folderList) fi.seen = false;

  // unhide any folder tags with unread messages
  static stmtUnhide = LazyStatement!"View"(`
    UPDATE tagnames
    SET
      hidden=0
    WHERE
      hidden=1 AND
      (tag='#spam' OR (tag<>'' AND SUBSTR(tag, 1, 1)='/')) AND
      EXISTS (SELECT uid FROM threads WHERE tagid=tagnames.tagid AND appearance=`~(cast(int)Appearance.Unread).to!string~`)
  ;`);
  stmtUnhide.st.doAll();

  static auto stmtGet = LazyStatement!"View"(`
    SELECT
        tagid AS tagid
      , tag AS name
    FROM tagnames
    WHERE
      tag='#spam' OR
      (hidden=0 AND tag<>'' AND SUBSTR(tag, 1, 1)='/')
  ;`);

  static auto stmtGetUnread = LazyStatement!"View"(`
    SELECT
    COUNT(uid) AS unread
    FROM threads
    WHERE tagid=:tagid AND appearance=`~(cast(int)Appearance.Unread).to!string~`
  ;`);

  foreach (auto row; stmtGet.st.range) {
    bool append = true;
    uint tagid = row.tagid!uint;
    foreach (FolderInfo fi; folderList) {
      if (fi.tagid == tagid) {
        append = false;
        fi.seen = true;
        if (fi.name != row.name!SQ3Text) {
          fi.name = row.name!SQ3Text;
          fi.calcDepthVisName();
          needsort = true;
        }
        break;
      }
    }
    if (append) {
      needsort = true;
      FolderInfo newfi = new FolderInfo();
      newfi.tagid = tagid;
      newfi.name = row.name!SQ3Text;
      newfi.unreadCount = 0;
      newfi.seen = true;
      newfi.calcDepthVisName();
      folderList ~= newfi;
    }
  }

  // remove unseen folders
  for (usize f = 0; f < folderList.length; ) {
    if (!folderList[f].seen && !folderList[f].ephemeral) {
      needsort = true;
      folderList[f].clear();
      delete folderList[f];
      foreach (immutable c; f+1..folderList.length) folderList[c-1] = folderList[c];
      folderList[$-1] = null;
      folderList.length -= 1;
    } else {
      ++f;
    }
  }

  if (needsort) {
    // remove all epemerals
    for (usize f = 0; f < folderList.length; ) {
      if (folderList[f].ephemeral) {
        folderList[f].clear();
        delete folderList[f];
        foreach (immutable c; f+1..folderList.length) folderList[c-1] = folderList[c];
        folderList[$-1] = null;
        folderList.length -= 1;
      } else {
        ++f;
      }
    }

    // readd all ephemerals
    for (;;) {
      bool again = false;
      foreach (FolderInfo fi; folderList) {
        if (fi.needEphemeral) {
          //conwriteln("ephemeral for '", fi.name, "'");
          again = true;
          fi.createEphemerals();
          break;
        }
      }
      if (!again) break;
    }

    static bool isAccount (const(char)[] s) pure nothrow @trusted @nogc {
      if (!s.startsWith("/accounts")) return false;
      return (s.length == 9 || s[9] == '/');
    }

    import std.algorithm.sorting : sort;
    folderList.sort!((const FolderInfo a, const FolderInfo b) {
      if (a.name == b.name) return false;
      if (isAccount(a.name) && !isAccount(b.name)) return true; // a < b
      if (!isAccount(a.name) && isAccount(b.name)) return false; // a >= b
      if (a.name[0] == '#' && b.name[0] != '#') return false; // a >= b
      if (a.name[0] != '#' && b.name[0] == '#') return true; // a < b
      return (a.name < b.name);
    });
    res = true;
  }

  // check unread counts
  foreach (FolderInfo fi; folderList) {
    if (fi.ephemeral) continue;
    foreach (auto row; stmtGetUnread.st.bind(":tagid", fi.tagid).range) {
      if (fi.unreadCount != row.unread!uint) {
        res = true;
        fi.unreadCount = row.unread!uint;
      }
    }
  }

  setupTrayAnimation();
  folderDataVersion = dbView.getDataVersion();
  //conwriteln("ver=", folderDataVersion);
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared bool dbg_dump_keynames;


// ////////////////////////////////////////////////////////////////////////// //
class TrayAnimationStepEvent {}
__gshared TrayAnimationStepEvent evTrayAnimationStep;
shared static this () { evTrayAnimationStep = new TrayAnimationStepEvent(); }

__gshared int trayAnimationIndex = 0; // 0: no animation
__gshared int trayAnimationDir = 1; // direction
__gshared uint trayAnimDataVersion = uint.max;


// ////////////////////////////////////////////////////////////////////////// //
void trayPostAnimationEvent () {
  if (vbwin !is null && !vbwin.eventQueued!TrayAnimationStepEvent) vbwin.postTimeout(evTrayAnimationStep, 100);
}


void trayDoAnimationStep () {
  if (trayicon is null || trayicon.closed) return; // no tray icon
  if (vbwin is null || vbwin.closed) return;
  if (trayAnimationIndex == 0) return; // no animation
  trayPostAnimationEvent();
  if (trayAnimationDir < 0) {
    if (--trayAnimationIndex == 1) trayAnimationDir = 1;
  } else {
    if (++trayAnimationIndex == trayimages.length-1) trayAnimationDir = -1;
  }
  trayicon.icon = trayimages[trayAnimationIndex];
  //vbwin.icon = icons[trayAnimationIndex];
  //vbwin.sendDummyEvent(); // or it won't redraw itself
  //flushGui(); // or it may not redraw itself
}


// ////////////////////////////////////////////////////////////////////////// //
void trayStartAnimation () {
  if (trayicon is null) return; // no tray icon
  if (trayAnimationIndex == 0) {
    trayAnimationIndex = 1;
    trayAnimationDir = 1;
    trayicon.icon = trayimages[1];
    vbwin.icon = icons[1];
    flushGui(); // or it may not redraw itself
    trayPostAnimationEvent();
  }
}


void trayStopAnimation () {
  if (trayicon is null) return; // no tray icon
  if (trayAnimationIndex != 0) {
    trayAnimationIndex = 0;
    trayAnimationDir = 1;
    trayicon.icon = trayimages[0];
    vbwin.icon = icons[0];
    flushGui(); // or it may not redraw itself
  }
}


// check if we have to start/stop animation, and do it
void setupTrayAnimation () {
  import std.conv : to;
  static auto stmtGetUnread = LazyStatement!"View"(`
    SELECT 1
    FROM threads
    WHERE appearance=`~(cast(int)Appearance.Unread).to!string~`
    LIMIT 1
  ;`);

  if (trayicon is null) return; // no tray icon

  immutable uint dver = dbView.getDataVersion();
  //conwriteln("TRAY: dver=", dver, "; trv=", trayAnimDataVersion);
  if (dver == trayAnimDataVersion) return;
  trayAnimDataVersion = dver;

  foreach (auto row; stmtGetUnread.st.range) {
    //conwriteln("TRAY: start anim!");
    trayStartAnimation();
    return;
  }
  //conwriteln("TRAY: stop anim!");
  trayStopAnimation();
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared string[string] mainAppKeyBindings;

void clearBindings () {
  mainAppKeyBindings.clear();
}


void mainAppBind (ConString kname, ConString concmd) {
  KeyEvent evt = KeyEvent.parse(kname);
  if (concmd.length) {
    mainAppKeyBindings[evt.toStr] = concmd.idup;
  } else {
    mainAppKeyBindings.remove(evt.toStr);
  }
}


void mainAppUnbind (ConString kname) {
  KeyEvent evt = KeyEvent.parse(kname);
  mainAppKeyBindings.remove(evt.toStr);
}


void setupDefaultBindings () {
  //mainAppBind("C-L", "dbg_font_window");
  mainAppBind("C-Q", "quit_prompt");

  mainAppBind("N", "next_unread ona");
  mainAppBind("S-N", "next_unread tan");
  mainAppBind("M", "next_unread ona");
  mainAppBind("S-M", "next_unread tan");

  mainAppBind("U", "mark_unread");
  mainAppBind("R", "mark_read");

  mainAppBind("Space", "artext_page_down");
  mainAppBind("S-Space", "artext_page_up");
  mainAppBind("M-Up", "artext_line_up");
  mainAppBind("M-Down", "artext_line_down");

  mainAppBind("Up", "article_prev");
  mainAppBind("Down", "article_next");
  mainAppBind("PageUp", "article_pgup");
  mainAppBind("PageDown", "article_pgdown");
  mainAppBind("Home", "article_to_first");
  mainAppBind("End", "article_to_last");
  mainAppBind("C-Up", "article_scroll_up");
  mainAppBind("C-Down", "article_scroll_down");

  mainAppBind("C-PageUp", "folder_prev");
  mainAppBind("C-PageDown", "folder_next");

  mainAppBind("M-O", "folder_options");

  //mainAppBind("C-M-U", "folder_update");
  mainAppBind("C-S-I", "update_all");
  mainAppBind("C-H", "article_dump_headers");

  mainAppBind("C-Backslash", "find_mine");

  //mainAppBind("C-Slash", "article_to_parent");
  //mainAppBind("C-Comma", "article_to_prev_sib");
  //mainAppBind("C-Period", "article_to_next_sib");

  //mainAppBind("C-Insert", "article_copy_url_to_clipboard");
  mainAppBind("C-M-K", "article_twit_thread");
  mainAppBind("T", "article_edit_poster_title");

  mainAppBind("C-R", "article_reply");
  mainAppBind("S-R", "article_reply_to_from");

  mainAppBind("S-P", "new_post");

  //mainAppBind("S-Enter", "article_open_in_browser");
  //mainAppBind("M-Enter", "article_open_in_browser tan");

  mainAppBind("Delete", "article_softdelete_toggle");
  mainAppBind("C-Delete", "article_harddelete_toggle");
}


// ////////////////////////////////////////////////////////////////////////// //
struct ImgViewCommand {
  string filename;
}

private void imageViewThread (Tid ownerTid) {
  string fname;
  try {
    conwriteln("waiting for the message...");
    receive(
      (ImgViewCommand cmd) {
        fname = cmd.filename;
      }
    );
    conwriteln("got filename: \"", fname, "\"");

    try {
      import std.process;
      //FIXME: make regvar for image viewer
      //auto pid = execute(["keh", attfname], null, Config.detached);
      //pid.wait();
      import std.stdio : File;
      auto frd = File("/dev/null");
      auto fwr = File("/dev/null", "w");
      auto pid = spawnProcess(["keh", fname], frd, fwr, fwr, null, Config.none/*detached*/);
      pid.wait();
    } catch (Exception e) {
      conwriteln("ERROR executing image viewer: ", e.msg);
    }
  } catch (Throwable e) {
    // here, we are dead and fucked (the exact order doesn't matter)
    //import core.stdc.stdlib : abort;
    import core.stdc.stdio : fprintf, stderr;
    //import core.memory : GC;
    import core.thread : thread_suspendAll;
    //GC.disable(); // yeah
    //thread_suspendAll(); // stop right here, you criminal scum!
    auto s = e.toString();
    fprintf(stderr, "\n=== FATAL ===\n%.*s\n", cast(uint)s.length, s.ptr);
  }
  try {
    import std.file : remove;
    if (fname.length) {
      conwriteln("deleting file \"", fname, "\"");
      remove(fname);
    }
  } catch (Exception e) {}
}


// ////////////////////////////////////////////////////////////////////////// //
void initConsole () {
  import std.functional : toDelegate;

  conRegVar!scrollKeepLines("scroll_keep_lines", "number of lines to keep on page up or page down.");
  conRegVar!unreadTimeoutInterval("t_unread_timeout", "timout to mark keyboard-selected message as unread (<1: don't mark)");
  conRegVar!preferHtmlContent("prefer_html_content", "prefer html content when both html and plain are present?");
  conRegVar!detectHtmlContent("detect_html_content", "detect html content in plain text?");

  conRegFunc!clearBindings("binds_app_clear", "clear main application keybindings");
  conRegFunc!setupDefaultBindings("binds_app_default", "*append* default application bindings");
  conRegFunc!mainAppBind("bind_app", "add main application binding");
  conRegFunc!mainAppUnbind("unbind_app", "remove main application binding");


  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!((ConString filename) {
    if (filename.length == 0) return;
    import std.path : buildPath;
    auto fname = buildPath(mailRootDir, filename);
    char[] text;
    scope(exit) delete text;
    bool sayError = false;
    try {
      auto fl = VFile(fname);
      sayError = true;
      conwriteln("loading style file '", fname, "'...");
      auto sz = fl.size;
      if (sz > 1024*1024*32) { conwriteln("ERROR: style file too big!"); return; }
      if (sz == 0) return;
      text = new char[cast(usize)sz];
      fl.rawReadExact(text);
      fl.close();
    } catch (Exception) {
      if (sayError) conwriteln("ERROR reading style file!");
      return;
    }
    try {
      defaultColorStyle.parseStyle(text);
    } catch (Exception e) {
      conwriteln("ERROR parsing style: ", e.msg);
    }
  })("load_style", "load widget style");


  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!(() {
    auto qww = new YesNoWindow("Quit?", "Do you really want to quit?", true);
    qww.onYes = () { concmd("quit"); };
    //qww.addModal();
  })("quit_prompt", "quit with prompt");


  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!((ConString url, bool forceOpera=false) {
    if (url.length) {
      import std.stdio : File;
      import std.process;
      try {
        auto frd = File("/dev/null");
        auto fwr = File("/dev/null", "w");
        spawnProcess([getBrowserCommand(forceOpera), url.idup], frd, fwr, fwr, null, Config.detached);
      } catch (Exception e) {
        conwriteln("ERROR executing URL viewer (", e.msg, ")");
      }
    }
  })("open_url", "open given url in a browser");


  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!(() {
    receiverForceUpdateAll();
  })("update_all", "mark all groups for updating");


  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!(() {
    if (mainPane !is null && mainPane.folderUpOne()) {
      postScreenRebuild();
    }
  })("folder_prev", "go to previous group");

  conRegFunc!(() {
    if (mainPane !is null && mainPane.folderDownOne()) {
      postScreenRebuild();
    }
  })("folder_next", "go to next group");


  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!(() {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    if (chiroGetMessageExactRead(mainPane.lastDecodedTid, mainPane.msglistCurrUId)) {
      chiroSetMessageUnread(mainPane.lastDecodedTid, mainPane.msglistCurrUId);
      postScreenRebuild();
      setupTrayAnimation();
    }
  })("mark_unread", "mark current message as unread");

  conRegFunc!(() {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    if (chiroGetMessageUnread(mainPane.lastDecodedTid, mainPane.msglistCurrUId)) {
      chiroSetMessageRead(mainPane.lastDecodedTid, mainPane.msglistCurrUId);
      setupTrayAnimation();
      postScreenRebuild();
    }
  })("mark_read", "mark current message as read");

  conRegFunc!((bool allowNextGroup=false) {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    // try current group
    if (mainPane.lastDecodedTid != 0) {
      auto uid = chiroGetPaneNextUnread(mainPane.msglistCurrUId);
      if (uid) {
        // i found her!
        if (uid == mainPane.msglistCurrUId) return;
        mainPane.msglistCurrUId = uid;
        mainPane.threadListPositionDirty();
        chiroSetMessageRead(mainPane.lastDecodedTid, mainPane.msglistCurrUId);
        setupTrayAnimation();
        postScreenRebuild();
        return;
      }
    }
    // try other groups?
    if (!allowNextGroup) return;
    int idx = mainPane.folderCurrIndex;
    foreach (; 0..cast(int)folderList.length) {
      idx = (idx+1)%cast(int)folderList.length;
      if (idx == mainPane.folderCurrIndex) continue;
      if (folderList[idx].ephemeral) continue;
      if (folderList[idx].unreadCount == 0) continue;
      mainPane.folderSetToIndex(idx);
      auto uid = chiroGetPaneNextUnread(/*mainPane.msglistCurrUId*/0);
      if (uid) {
        // i found her!
        if (uid == mainPane.msglistCurrUId) return;
        mainPane.msglistCurrUId = uid;
        mainPane.threadListPositionDirty();
        // don't mark the message as read
        //chiroSetMessageRead(mainPane.lastDecodedTid, mainPane.msglistCurrUId);
        chiroSetMessageUnread(mainPane.lastDecodedTid, mainPane.msglistCurrUId);
        mainPane.setupUnreadTimer();
        setupTrayAnimation();
        postScreenRebuild();
        return;
      }
    }
  })("next_unread", "move to next unread message; bool arg: can skip to next group?");


  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!(() {
    if (mainPane !is null) mainPane.scrollByPageUp();
  })("artext_page_up", "do pageup on article text");

  conRegFunc!(() {
    if (mainPane !is null) mainPane.scrollByPageDown();
  })("artext_page_down", "do pagedown on article text");

  conRegFunc!(() {
    if (mainPane !is null) mainPane.scrollBy(-1);
  })("artext_line_up", "do lineup on article text");

  conRegFunc!(() {
    if (mainPane !is null) mainPane.scrollBy(1);
  })("artext_line_down", "do linedown on article text");

  // //////////////////////////////////////////////////////////////////// //
  /*FIXME
  conRegFunc!((bool forceOpera=false) {
    if (auto fldx = getActiveFolder) {
      fldx.withBaseReader((abase, cur, top, alist) {
        if (cur < alist.length) {
          abase.loadContent(alist[cur]);
          if (auto art = abase[alist[cur]]) {
            scope(exit) art.releaseContent();
            auto path = art.getHeaderValue("path:");
            //conwriteln("path: [", path, "]");
            if (path.startsWithCI("digitalmars.com!.POSTED.")) {
              import std.stdio : File;
              import std.process;
              //http://forum.dlang.org/post/hhyqqpoatbpwkprbvfpj@forum.dlang.org
              string id = art.msgid;
              if (id.length > 2 && id[0] == '<' && id[$-1] == '>') id = id[1..$-1];
              auto pid = spawnProcess(
                [getBrowserCommand(forceOpera), "http://forum.dlang.org/post/"~id],
                File("/dev/zero"), File("/dev/null"), File("/dev/null"),
              );
              pid.wait();
            }
          }
        }
      });
    }
  })("article_open_in_browser", "open the current article in browser (dlang forum)");
  */

  /*FIXME
  conRegFunc!(() {
    if (auto fldx = getActiveFolder) {
      fldx.withBaseReader((abase, cur, top, alist) {
        if (cur < alist.length) {
          if (auto art = abase[alist[cur]]) {
            auto path = art.getHeaderValue("path:");
            if (path.startsWithCI("digitalmars.com!.POSTED.")) {
              //http://forum.dlang.org/post/hhyqqpoatbpwkprbvfpj@forum.dlang.org
              string id = art.msgid;
              if (id.length > 2 && id[0] == '<' && id[$-1] == '>') id = id[1..$-1];
              id = "http://forum.dlang.org/post/"~id;
              setClipboardText(vbwin, id);
              setPrimarySelection(vbwin, id);
            }
          }
        }
      });
    }
  })("article_copy_url_to_clipboard", "copy the url of the current article to clipboard (dlang forum)");
  */


  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!((ConString oldmail, ConString newmail, ConString newname) {
    if (oldmail.length) {
      if (newmail.length == 0) {
        repmailreps.remove(oldmail.idup);
      } else {
        MailReplacement mr;
        mr.oldmail = oldmail.idup;
        mr.newmail = newmail.idup;
        mr.newname = newname.idup;
        repmailreps[mr.oldmail] = mr;
      }
    }
  })("append_replyto_mail_replacement", "append replacement for reply mails");


  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!(() {
    if (mainPane !is null && mainPane.threadListUp()) {
      postScreenRebuild();
    }
  })("article_prev", "go to previous article");

  conRegFunc!(() {
    if (mainPane !is null && mainPane.threadListDown()) {
      postScreenRebuild();
    }
  })("article_next", "go to next article");

  conRegFunc!(() {
    if (mainPane !is null && mainPane.threadListPageUp()) {
      postScreenRebuild();
    }
  })("article_pgup", "artiles list: page up");

  conRegFunc!(() {
    if (mainPane !is null && mainPane.threadListPageDown()) {
      postScreenRebuild();
    }
  })("article_pgdown", "artiles list: page down");

  conRegFunc!(() {
    if (mainPane !is null && mainPane.threadListScrollUp(movecurrent:false)) {
      postScreenRebuild();
    }
  })("article_scroll_up", "scroll article list up");

  conRegFunc!(() {
    if (mainPane !is null && mainPane.threadListScrollDown(movecurrent:false)) {
      postScreenRebuild();
    }
  })("article_scroll_down", "scroll article list up");

  conRegFunc!(() {
    if (mainPane !is null && mainPane.threadListHome()) {
      postScreenRebuild();
    }
  })("article_to_first", "go to first article");

  conRegFunc!(() {
    if (mainPane !is null && mainPane.threadListEnd()) {
      postScreenRebuild();
    }
  })("article_to_last", "go to last article");


  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!(() {
    auto pw = new PostWindow();
    //pw.setFrom("goo boo!");
    pw.caption = "WRITE NEW MESSAGE";

    uint tid = mainPane.lastDecodedTid;
    if (tid) {
      dynstring tn = chiroGetTagName(tid);
      if (tn.length) {
        foreach (auto row; dbConf.statement(`
            SELECT name AS name, realname AS realname, email AS email, nntpgroup AS nntpgroup
            FROM accounts
            WHERE inbox=:inbox
            LIMIT 1
          ;`).bindConstText(":inbox", tn.getData).range)
        {
          //pw.desttag = tn; // no need to
          pw.accname = row.name!SQ3Text;
          pw.setFrom(row.realname!SQ3Text, row.email!SQ3Text);
          pw.to.focus();
        }
      }
    }
  })("new_post", "post a new article or message");


  // //////////////////////////////////////////////////////////////////// //
  static void doReplyXX (string repfld) {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    if (!mainPane.msglistCurrUId) return;

    auto pw = new PostWindow();
    pw.ed.focus();

    dynstring accname;
    foreach (auto row; dbView.statement(`
        SELECT DISTINCT(tagid) AS tagid, tt.tag AS name
        FROM threads
        INNER JOIN tagnames AS tt USING(tagid)
        WHERE uid=:uid AND name LIKE 'account:%'
      ;`).bind(":uid", mainPane.msglistCurrUId).range)
    {
      auto name = row.name!SQ3Text;
      if (name.startsWith("account:")) {
        accname = name[8..$];
        if (accname.length) {
          pw.accname = accname;
          break;
        }
      }
    }

    if (accname.length == 0) {
      conwriteln("ERROR: source account not found!");
      vbwin.beep();
    } else {
      conwriteln("account: ", accname);
    }

    dynstring fldvalue;

    foreach (auto row; dbStore.statement(`
        SELECT ChiroHdr_Field(ChiroUnpack(data), :fldname) AS fldvalue
        FROM messages
        WHERE uid=:uid
        ORDER BY uid
        LIMIT 1
      `).bind(":uid", mainPane.msglistCurrUId).bindConstText(":fldname", repfld).range)
    {
      fldvalue = row.fldvalue!SQ3Text.decodeSubj;
    }

    if (fldvalue.length == 0 && !repfld.strEquCI("From")) {
      foreach (auto row; dbStore.statement(`
          SELECT ChiroHdr_Field(ChiroUnpack(data), :fldname) AS fldvalue
          FROM messages
          WHERE uid=:uid
          ORDER BY uid
          LIMIT 1
        `).bind(":uid", mainPane.msglistCurrUId).bindConstText(":fldname", "From").range)
      {
        fldvalue = row.fldvalue!SQ3Text.decodeSubj;
      }
    }

    pw.to.str = fldvalue;
    dynstring cpt = dynstring("Reply to: ")~fldvalue;
    cpt ~= " (field: ";
    cpt ~= repfld;
    cpt ~= ")";
    pw.caption = cpt;

    dynstring fromname;
    foreach (auto row; dbView.statement(`
        SELECT subj AS subj, from_name AS fromname
        FROM info
        WHERE uid=:uid
        LIMIT 1
      `).bind(":uid", mainPane.msglistCurrUId).range)
    {
      fromname = row.fromname!SQ3Text;
      dynstring s = "Re: ";
      s ~= row.subj!SQ3Text;
      pw.subj.str = s;
    }

    foreach (auto row; dbView.statement(`
        SELECT ChiroUnpack(content) AS content
        FROM content_text
        WHERE uid=:uid
        LIMIT 1
      `).bind(":uid", mainPane.msglistCurrUId).range)
    {
      pw.ed.addText(fromname);
      pw.ed.addText(" wrote:\n");
      pw.ed.addText("\n");
      dynstring text = row.content!SQ3Text;
      foreach (SQ3Text s; text.byLine) {
        if (s.length == 0 || s[0] == '>') pw.ed.addText(">"); else pw.ed.addText("> ");
        pw.ed.addText(s);
        pw.ed.addText("\n");
      }
      pw.ed.addText("\n");
      pw.ed.reformat();
    }

    foreach (auto row; dbConf.statement(`
        SELECT
            accid AS accid
          --, name AS name
          , recvserver AS recvserver
          , sendserver AS sendserver
          , realname AS realname
          , email AS email
          , nntpgroup AS nntpgroup
        FROM accounts
        WHERE name=:accname --AND sendserver<>'' AND email<>''
        LIMIT 1
      `).bindConstText(":accname", accname.getData).range)
    {
      /*
      conwriteln("GROUP: <", row.nntpgroup!SQ3Text, ">");
      conwriteln("NAME: <", row.realname!SQ3Text, ">");
      conwriteln("EMAIL: <", row.email!SQ3Text, ">");
      */
      dynstring frm;
      if (row.realname!SQ3Text.length) {
        frm = row.realname!SQ3Text;
        frm ~= " <";
        frm ~= row.email!SQ3Text;
        frm ~= ">";
      } else {
        frm = row.email!SQ3Text;
      }
      pw.setFrom(frm);

      if (row.nntpgroup!SQ3Text.length) {
        pw.to.str = dynstring("newsgroup: ")~row.nntpgroup!SQ3Text;
        pw.to.readonly = true;
        pw.to.disabled = true;
        pw.allowAccountChange = false;
        pw.nntp = true;
      }
    }

    writeln("getting msgid...");
    foreach (auto row; dbView.statement(`
        SELECT msgid AS msgid
        FROM msgids
        WHERE uid=:uid
        LIMIT 1
      `).bind(":uid", mainPane.msglistCurrUId).range)
    {
      conwriteln("MSGID: |", row.msgid!SQ3Text);
      if (pw.references.length) pw.references ~= " ";
      // two times, one for references field
      if (pw.references.length) pw.references ~= " ";
      pw.references ~= row.msgid!SQ3Text;
      if (pw.references.length) pw.references ~= " ";
      pw.references ~= row.msgid!SQ3Text;
    }

    writeln("select references...");
    foreach (auto row; dbView.statement(`
        SELECT msgid AS msgid
        FROM refids
        WHERE uid=:uid
        ORDER BY idx
      `).bind(":uid", mainPane.msglistCurrUId).range)
    {
      conwriteln("REF: |", row.msgid!SQ3Text);
      if (pw.references.length) pw.references ~= " ";
      pw.references ~= row.msgid!SQ3Text;
    }

    pw.desttag = chiroGetTagName(mainPane.lastDecodedTid);
  }

  static void doReply (string repfld) {
    try {
      doReplyXX(repfld);
    } catch (Exception e) {
      conwriteln("EXCEPTION: ", e.msg);
      auto s = e.toString();
      conwriteln(s);
    }
  }

  conRegFunc!(() { doReply("Reply-To"); })("article_reply", "reply to the current article with \"reply-to\" field");
  conRegFunc!(() { doReply("From"); })("article_reply_to_from", "reply to the current article with \"from\" field");


  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!(() {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    if (mainPane.msglistCurrUId) {
      //messageBogoMarkHam(mainPane.msglistCurrUId);
      foreach (auto mrow; dbStore.statement(`SELECT ChiroUnpack(data) AS data FROM messages WHERE uid=:uid LIMIT 1;`).bind(":uid", mainPane.msglistCurrUId).range) {
        try {
          import core.stdc.stdio : snprintf;
          char[128] fname = void;
          auto len = snprintf(fname.ptr, fname.length, "~/Mail/_bots/z_eml_%u.eml", cast(uint)mainPane.msglistCurrUId);
          auto fo = VFile(fname[0..len], "w");
          fo.writeln(mrow.data!SQ3Text.xstripright);
          fo.close();
          conwriteln("article exported to: ", fname[0..len]);
        } catch (Exception e) {
        }
      }
    }
  })("article_export", "export current article as raw text");


  conRegFunc!(() {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    if (mainPane.msglistCurrUId) {
      messageBogoMarkHam(mainPane.msglistCurrUId);
      //TODO: move out of spam
    }
  })("article_mark_ham", "mark current article as ham");


  conRegFunc!(() {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    if (mainPane.msglistCurrUId) {
      immutable uint uid = mainPane.msglistCurrUId;
      // move to the next message
      if (!mainPane.threadListDown()) mainPane.threadListUp();
      conwriteln("calling bogofilter...");
      messageBogoMarkSpam(uid);
      conwriteln("adding '#spam' tag");
      immutable bool updatePane = chiroMessageAddTag(uid, "#spam");
      conwriteln("removing other virtual folder tags...");
      DynStr[] tags;
      scope(exit) delete tags;
      tags.reserve(32);
      static auto stGetMsgTags = LazyStatement!"View"(`
        SELECT DISTINCT(tagid) AS tagid, tt.tag AS name
        FROM threads
        INNER JOIN tagnames AS tt USING(tagid)
        WHERE uid=:uid
      ;`);
      foreach (auto row; stGetMsgTags.st.bind(":uid", uid).range) {
        auto tag = row.name!SQ3Text;
        conwriteln("  tag: ", tag);
        if (tag.startsWith("/")) tags ~= DynStr(tag);
      }
      foreach (ref DynStr tn; tags) {
        conwriteln("removing tag '", tn.getData, "'...");
        chiroMessageRemoveTag(uid, tn.getData);
      }
      conwriteln("done marking as spam.");
      if (updatePane) {
        mainPane.switchToFolderTid(mainPane.lastDecodedTid, forced:true);
      } else {
        // rescan folder
        mainPane.updateViewsIfTid(chiroGetTagUid("#spam"));
      }
      postScreenRebuild();
    }
  })("article_mark_spam", "mark current article as spam");


  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!((ConString fldname) {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    if (!mainPane.msglistCurrUId) return;
    immutable uint uid = mainPane.msglistCurrUId;

    fldname = fldname.xstrip;
    while (fldname.length && fldname[$-1] == '/') fldname = fldname[0..$-1].xstrip;
    immutable bool isforced = (fldname.length && fldname[0] == '!');
    if (isforced) fldname = fldname[1..$];
    if (fldname.length == 0) {
      conwriteln("ERROR: cannot move to empty folder");
      return;
    }
    if (fldname[0].isalnum) {
      conwriteln("ERROR: invalid folder name '", fldname, "'");
      return;
    }

    uint tagid;
    if (!isforced) {
      tagid = chiroGetTagUid(fldname);
    } else {
      tagid = chiroAppendTag(fldname, hidden:(fldname[0] == '#' ? 1 : 0));
    }
    if (!tagid) {
      conwriteln("ERROR: invalid folder name '", fldname, "'");
      return;
    }

    immutable bool updatePane = chiroMessageAddTag(uid, fldname);
    DynStr[] tags;
    scope(exit) delete tags;
    tags.reserve(32);
    static auto stGetMsgTags = LazyStatement!"View"(`
      SELECT DISTINCT(tagid) AS tagid, tt.tag AS name
      FROM threads
      INNER JOIN tagnames AS tt USING(tagid)
      WHERE uid=:uid
    ;`);
    foreach (auto row; stGetMsgTags.st.bind(":uid", uid).range) {
      auto tag = row.name!SQ3Text;
      conwriteln("  tag: ", tag);
      if (tag.startsWith("account:")) continue;
      if (tag != fldname) tags ~= DynStr(tag);
    }
    foreach (ref DynStr tn; tags) {
      conwriteln("removing tag '", tn.getData, "'...");
      chiroMessageRemoveTag(uid, tn.getData);
    }
    if (updatePane) {
      mainPane.switchToFolderTid(mainPane.lastDecodedTid, forced:true);
    } else {
      // rescan folder
      mainPane.updateViewsIfTid(chiroGetTagUid("#spam"));
    }
    postScreenRebuild();
  })("article_move_to_folder", "move article to existing folder");


  // //////////////////////////////////////////////////////////////////// //
  version(none) {
    conRegFunc!(() {
      /*FIXME
      if (auto fld = getActiveFolder) {
        fld.moveToParent();
        postScreenRebuild();
      }
      */
    })("article_to_parent", "jump to parent article, if any");

    conRegFunc!(() {
      /*FIXME
      if (auto fld = getActiveFolder) {
        fld.moveToPrevSib();
        postScreenRebuild();
      }
      */
    })("article_to_prev_sib", "jump to previous sibling");

    conRegFunc!(() {
      /*FIXME
      if (auto fld = getActiveFolder) {
        fld.moveToNextSib();
        postScreenRebuild();
      }
      */
    })("article_to_next_sib", "jump to next sibling");
  }

  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!(() {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    if (mainPane.lastDecodedTid == 0 || mainPane.msglistCurrUId == 0) return;
    DynStr tagname = chiroGetTagName(mainPane.lastDecodedTid);
    if (!globmatch(tagname.getData, "/dmars_ng/*")) return;
    // get from
    DynStr fromMail, fromName;
    if (!chiroGetMessageFrom(mainPane.msglistCurrUId, ref fromMail, ref fromName)) return;
    if (fromMail.length == 0 && fromName.length == 0) return;
    //writeln("!!! email=", fromMail.getData, "; name=", fromName.getData, "|");

    uint twitid = 0;
    DynStr twtitle;
    DynStr twname;
    DynStr twemail;
    DynStr twnotes;
    DynStr twtagglob = "/dmars_ng/*";
    bool withName = false;

    static auto stFindTwit = LazyStatement!"Conf"(`
      SELECT
          etwitid AS twitid
        , tagglob AS tagglob
        , email AS email
        , name AS name
        , title AS title
        , notes AS notes
      FROM emailtwits
      WHERE email=:email AND name=:name
    ;`);

    static auto stAddTwit = LazyStatement!"Conf"(`
      INSERT INTO emailtwits
              ( tagglob, email, name, title, notes)
        VALUES(:tagglob,:email,:name,:title,:notes)
    ;`);

    static auto stModifyTwit = LazyStatement!"Conf"(`
      UPDATE emailtwits
        SET
            tagglob=:tagglob
          , email=:email
          , name=:name
          , title=:title
          , notes=:notes
      WHERE etwitid=:twitid
    ;`);

    static auto stRemoveTwitAuto = LazyStatement!"Conf"(`
      DELETE FROM msgidtwits
      WHERE etwitid=:twitid
    ;`);

    static auto stRemoveTwit = LazyStatement!"Conf"(`
      DELETE FROM emailtwits
      WHERE etwitid=:twitid
    ;`);

    stFindTwit.st
      .bindConstText(":email", fromMail.getData)
      .bindConstText(":name", fromName.getData);
    foreach (auto row; stFindTwit.st.range) {
      //conwriteln("!!!", row.twitid!uint, "; mail=", row.email!SQ3Text, "; name=", row.name!SQ3Text, "; title=", row.title!SQ3Text.recodeToKOI8);
      if (!globmatch(tagname.getData, row.tagglob!SQ3Text)) continue;
      twitid = row.twitid!uint;
      twtitle = row.title!SQ3Text;
      twname = row.name!SQ3Text;
      twemail = row.email!SQ3Text;
      twtagglob = row.tagglob!SQ3Text;
      twnotes = row.notes!SQ3Text;
      withName = (fromName.length != 0);
      break;
    }

    if (!twitid && fromName.length) {
      stFindTwit.st
        .bindConstText(":email", fromMail.getData)
        .bindConstText(":name", "");
      foreach (auto row; stFindTwit.st.range) {
        //conwriteln("!!!", row.twitid!uint, "; mail=", row.email!SQ3Text, "; name=", row.name!SQ3Text, "; title=", row.title!SQ3Text.recodeToKOI8);
        if (!globmatch(tagname.getData, row.tagglob!SQ3Text)) continue;
        twitid = row.twitid!uint;
        twtitle = row.title!SQ3Text;
        twname = row.name!SQ3Text;
        twemail = row.email!SQ3Text;
        twtagglob = row.tagglob!SQ3Text;
        twnotes = row.notes!SQ3Text;
        withName = false;
        break;
      }
    }

    //conwriteln("twitid: ", twitid, "; title=", twtitle.getData.recodeToKOI8);

    auto tw = new TitlerWindow((twitid ? twname : fromName), (twitid ? twemail : fromMail), twtagglob, twtitle);
    tw.onSelected = delegate (name, email, glob, title) {
      if (email.length == 0 && name.length == 0) return false;
      if (glob.length == 0) return false;
      if (email.length && email[0] == '@') return false;
      title = title.xstrip;
      if (twitid) {
        if (title.length == 0) {
          // remove twit
          conwriteln("removing twit...");
          stRemoveTwitAuto.st.bind(":twitid", twitid).doAll();
          stRemoveTwit.st.bind(":twitid", twitid).doAll();
        } else {
          // change twit
          conwriteln("changing twit...");
          stModifyTwit.st
            .bind(":twitid", twitid)
            .bindConstText(":tagglob", glob)
            .bindConstText(":email", email)
            .bindConstText(":name", name)
            .bindConstText(":title", title)
            .bindConstText(":notes", twnotes.getData, allowNull:true)
            .doAll();
        }
      } else {
        if (title.length == 0) return false;
        // new twit
        conwriteln("adding twit...");
        stAddTwit.st
            .bindConstText(":tagglob", glob)
            .bindConstText(":email", email)
            .bindConstText(":name", name)
            .bindConstText(":title", title)
            .bindConstText(":notes", null, allowNull:true)
            .doAll();
      }

      if (vbwin && !vbwin.closed) vbwin.postEvent(new RecalcAllTwitsEvent());
      return true;
    };
  })("article_edit_poster_title", "edit poster's title of the current article");


  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!(() {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    if (mainPane.lastDecodedTid == 0 || mainPane.msglistCurrUId == 0) return;
    DynStr tagname = chiroGetTagName(mainPane.lastDecodedTid);
    if (!globmatch(tagname.getData, "/dmars_ng/*")) return;
    int mute = chiroGetMessageMute(mainPane.lastDecodedTid, mainPane.msglistCurrUId);
    // check for non-automatic mutes
    if (mute != Mute.Normal && mute <= Mute.ThreadStart) return;
    createTwitByMsgid(mainPane.msglistCurrUId);
    mainPane.switchToFolderTid(mainPane.lastDecodedTid, forced:true);
    postScreenRebuild();
  })("article_twit_thread", "twit current thread");

  conRegFunc!(() {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    int app = chiroGetMessageAppearance(mainPane.lastDecodedTid, mainPane.msglistCurrUId);
    if (app >= 0) {
      //conwriteln("oldapp: ", app, " (", isSoftDeleted(app), ")");
      immutable bool wasPurged = (app == Appearance.SoftDeletePurge);
      app =
        app == Appearance.SoftDeletePurge ? Appearance.SoftDeleteUser :
        isSoftDeleted(app) ? Appearance.Read :
        Appearance.SoftDeleteUser;
      //conwriteln("newapp: ", app);
      chiroSetMessageAppearance(mainPane.lastDecodedTid, mainPane.msglistCurrUId, cast(Appearance)app);
      if (!wasPurged && isSoftDeleted(app)) mainPane.threadListDown();
      postScreenRebuild();
    }
  })("article_softdelete_toggle", "toggle \"soft deleted\" flag on current article");

  conRegFunc!(() {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    int app = chiroGetMessageAppearance(mainPane.lastDecodedTid, mainPane.msglistCurrUId);
    if (app >= 0) {
      //conwriteln("oldapp: ", app);
      app =
        app == Appearance.SoftDeletePurge ? Appearance.Read :
        isSoftDeleted(app) ? Appearance.SoftDeletePurge :
        Appearance.SoftDeletePurge;
      //conwriteln("newapp: ", app);
      chiroSetMessageAppearance(mainPane.lastDecodedTid, mainPane.msglistCurrUId, cast(Appearance)app);
      if (app == Appearance.SoftDeletePurge) mainPane.threadListDown();
      postScreenRebuild();
    }
  })("article_harddelete_toggle", "toggle \"hard deleted\" flag on current article");


  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!(() {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    DynStr hdrs = chiroMessageHeaders(mainPane.msglistCurrUId);
    if (hdrs.length == 0) return;
    conwriteln("============================");
    conwriteln("message uid: ", mainPane.msglistCurrUId);
    conwriteln("  tag tagid: ", mainPane.lastDecodedTid);
    conwriteln("============================");
    forEachHeaderLine(hdrs.getData, (const(char)[] line) {
      conwriteln("  ", line.xstripright);
      return true; // go on
    });
  })("article_dump_headers", "dump article headers");

  conRegFunc!(() {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    DynStr hdrs = chiroMessageHeaders(mainPane.msglistCurrUId);
    if (hdrs.length == 0) return;
    conwriteln("============================");
    conwriteln("message uid: ", mainPane.msglistCurrUId);
    conwriteln("  tag tagid: ", mainPane.lastDecodedTid);
    conwriteln("============================");
    auto bogo = messageBogoCheck(mainPane.msglistCurrUId);
    conwriteln("BOGO RESULT: ", bogo);
  })("article_bogo_check", "check article with bogofilter (purely informational)");

  conRegFunc!(() {
    if (mainPane !is null && mainPane.lastDecodedTid) {
      conwriteln("relinking ", mainPane.lastDecodedTid, " (", chiroGetTagName(mainPane.lastDecodedTid).getData, ")...");
      chiroSupportRelinkTagThreads(mainPane.lastDecodedTid);
      mainPane.switchToFolderTid(mainPane.lastDecodedTid, forced:true);
      postScreenRebuild();
    }
  })("folder_rebuild_index", "rebuild thread index");

  conRegFunc!(() {
    if (vbwin && !vbwin.closed && mainPane !is null /*&& mainPane.lastDecodedTid*/) {
      vbwin.postEvent(new RecalcAllTwitsEvent());
    }
  })("folder_rebuild_twits", "rebuild all twits");

  conRegFunc!(() {
    if (mainPane !is null && mainPane.folderCurrTag.length) {
      auto w = new TagOptionsWindow(mainPane.folderCurrTag.getData);
      w.onUpdated = delegate void (tagname) {
        if (vbwin && !vbwin.closed && mainPane !is null && mainPane.lastDecodedTagName == tagname) {
          mainPane.switchToFolderTid(mainPane.lastDecodedTid, forced:true);
        }
      };
      postScreenRebuild();
    }
  })("folder_options", "show options window");

  /*
  conRegFunc!(() {
    if (auto fld = getActiveFolder) {
      fld.withBase(delegate (abase) {
        uint idx = fld.curidx;
        if (idx >= fld.length) {
          idx = 0;
        } else if (auto art = abase[fld.baseidx(idx)]) {
          if (art.frommail.indexOf("ketmar@ketmar.no-ip.org") >= 0) {
            idx = (idx+1)%fld.length;
          }
        }
        foreach (immutable _; 0..fld.length) {
          auto art = abase[fld.baseidx(idx)];
          if (art !is null) {
            if (art.frommail.indexOf("ketmar@ketmar.no-ip.org") >= 0) {
              fld.curidx = cast(int)idx;
              postScreenRebuild();
              return;
            }
          }
          idx = (idx+1)%fld.length;
        }
      });
    }
  })("find_mine", "find mine article");
  */

  // //////////////////////////////////////////////////////////////////// //
  conRegVar!dbg_dump_keynames("dbg_dump_key_names", "dump key names");


  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!((uint idx) {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    if (mainPane.msglistCurrUId == 0) return;

    static auto statAttaches = LazyStatement!"View"(`
      SELECT
          idx AS idx
        , mime AS mime
        , name AS name
        , ChiroUnpack(content) AS content
      FROM attaches
      WHERE uid=:uid
      ORDER BY idx
    ;`);
    char[128] buf = void;
    uint attidx = 0;
    foreach (auto row; statAttaches.st.bind(":uid", mainPane.msglistCurrUId).range) {
      import core.stdc.stdio : snprintf;
      if (idx != 0) { --idx; ++attidx; continue; }
      if (!row.mime!SQ3Text.startsWith("image")) return;
      auto name = row.name!SQ3Text.getExtension;
      if (name.length == 0) return;
      try {
        DynStr fname;
        fname = "/tmp/_viewimage_";

        import std.uuid;
        UUID id = randomUUID();
        foreach (immutable ubyte b; id.data[]) {
          fname ~= "0123456789abcdef"[b>>4];
          fname ~= "0123456789abcdef"[b&0x0f];
        }
        fname ~= name;

        conwriteln("writing attach #", attidx, " to '", fname.getData, "'");
        VFile fo = VFile(fname.getData, "w");
        fo.rawWriteExact(row.content!SQ3Text);
        fo.close();

        auto vtid = spawn(&imageViewThread, thisTid);
        string fnamestr = fname.getData.idup;
        vtid.send(ImgViewCommand(fnamestr));
      } catch (Exception e) {
        conwriteln("ERROR writing attachment: ", e.msg);
      }
      break;
    }
  })("attach_view", "view attached image: attach_view index");


  // //////////////////////////////////////////////////////////////////// //
  conRegFunc!((uint idx, ConString userfname=null) {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    if (mainPane.msglistCurrUId == 0) return;

    static auto statAttaches = LazyStatement!"View"(`
      SELECT
          idx AS idx
        , mime AS mime
        , name AS name
        , ChiroUnpack(content) AS content
      FROM attaches
      WHERE uid=:uid
      ORDER BY idx
    ;`);
    char[128] buf = void;
    uint attidx = 0;
    foreach (auto row; statAttaches.st.bind(":uid", mainPane.msglistCurrUId).range) {
      import core.stdc.stdio : snprintf;
      if (idx != 0) { --idx; ++attidx; continue; }
      try {
        auto name = row.name!SQ3Text/*.decodeSubj*/;
        DynStr fname;

        if (userfname.length) {
          fname = userfname;
        } else {
          fname = "/tmp/";
          name = name.sanitizeFileNameStr;
          if (name.length == 0) {
            auto len = snprintf(buf.ptr, buf.sizeof, "attach_%02u.bin", attidx);
            fname ~= buf[0..cast(usize)len];
          } else {
            fname ~= name;
          }
        }

        conwriteln("writing attach #", attidx, " to '", fname.getData, "'");
        VFile fo = VFile(fname.getData, "w");
        fo.rawWriteExact(row.content!SQ3Text);
        fo.close();
      } catch (Exception e) {
        conwriteln("ERROR writing attachment: ", e.msg);
      }
      break;
    }
  })("attach_save", "save attach: attach_save index [filename]");
}


// ////////////////////////////////////////////////////////////////////////// //
//FIXME: turn into property
final class ArticleTextScrollEvent {}
__gshared ArticleTextScrollEvent evArticleScroll;
shared static this () {
  evArticleScroll = new ArticleTextScrollEvent();
}

void postArticleScroll () {
  if (vbwin !is null && !vbwin.eventQueued!ArticleTextScrollEvent) vbwin.postTimeout(evArticleScroll, 25);
}


// ////////////////////////////////////////////////////////////////////////// //
class MarkAsUnreadEvent {
  uint tagid;
  uint uid;
}

__gshared int unreadTimeoutInterval = 600;
__gshared int scrollKeepLines = 3;
__gshared bool preferHtmlContent = false;
__gshared bool detectHtmlContent = true;


void postMarkAsUnreadEvent () {
  if (unreadTimeoutInterval > 0 && unreadTimeoutInterval < 10000 && vbwin !is null && mainPane !is null) {
    if (!mainPane.msglistCurrUId) return;
    if (chiroGetMessageUnread(mainPane.lastDecodedTid, mainPane.msglistCurrUId)) {
      //conwriteln("setting new unread timer");
      auto evt = new MarkAsUnreadEvent();
      evt.tagid = mainPane.lastDecodedTid;
      evt.uid = mainPane.msglistCurrUId;
      vbwin.postTimeout(evt, unreadTimeoutInterval);
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared MainPaneWindow mainPane;


final class MainPaneWindow : SubWindow {
  dynstring[] emlines; // in utf
  uint emlinesBeforeAttaches;
  uint lastDecodedUId;
  uint lastDecodedTid; // for which folder we last build our view?
  uint lastMaxTidUid;
  DynStr lastDecodedTagName;
  DynStr arttoname;
  DynStr arttomail;
  DynStr artfromname;
  DynStr artfrommail;
  DynStr artsubj;
  DynStr arttime;
  int articleTextTopLine = 0;
  int articleDestTextTopLine = 0;
  //__gshared Timer unreadTimer; // as main pane is never destroyed, there's no need to kill the timer
  //__gshared Folder unreadFolder;
  //__gshared uint unreadIdx;
  int linesInHeader;
  // folder view
  DynStr folderTopTag = null;
  DynStr folderCurrTag = null;
  int folderTopIndex = 0;
  int folderCurrIndex = 0;
  // message list view
  uint msglistTopUId = 0;
  uint msglistCurrUId = 0;
  bool saveMsgListPositions = false;
  MonoTime lastStateSaveTime;
  uint viewDataVersion;

  this () {
    super(null, GxSize(screenWidth, screenHeight));
    allowMinimise = false;
    allowDragMove = false;
    mType = Type.OnBottom;
    add();
    loadSavedState();
    lastStateSaveTime = MonoTime.currTime;
    viewDataVersion = dbView.getDataVersion()-1u;
  }

  bool isViewChanged () const nothrow @trusted @nogc {
    pragma(inline, true);
    return (viewDataVersion != dbView.getDataVersion());
  }

  void updateViewsIfTid (uint tid) {
    if (tid && lastDecodedTid == tid) {
      switchToFolderTid(lastDecodedTid, forced:true);
    }
  }

  void checkSaveState () {
    if (lastStateSaveTime+dur!"seconds"(30) <= MonoTime.currTime) {
      //writeln("saving state...");
      saveCurrentState();
      lastStateSaveTime = MonoTime.currTime;
      if (lastDecodedTid) {
        immutable maxuid = chiroGetTreePaneTableMaxUId();
        if (maxuid && maxuid > lastMaxTidUid) {
          // something was changed in the database, rescal
          updateViewsIfTid(lastDecodedTid);
        }
      }
    }
  }

  void saveCurrentState () {
    transacted!"Conf"{
      chiroSetOption("/mainpane/folders/toptid", folderTopTag);
      chiroSetOption("/mainpane/folders/currtid", folderCurrTag);
      saveThreadListPositions(transaction:false);
    };
  }

  void loadSavedState () {
    chiroGetOption(folderTopTag, "/mainpane/folders/toptid");
    chiroGetOption(folderCurrTag, "/mainpane/folders/currtid");
    //conwriteln("curr: ", folderCurrTag);
    //conwriteln(" top: ", folderTopTag);
    rescanFolders(forced:true);
  }


  private void threadListPositionDirty () nothrow @safe @nogc {
    saveMsgListPositions = true;
  }

  private void saveThreadListPositionsInternal () {
    if (lastDecodedTagName.length == 0) return;
    import core.stdc.stdio : snprintf;
    const(char)[] tn = lastDecodedTagName.getData;
    char[128] xname = void;
    auto xlen = snprintf(xname.ptr, xname.sizeof, "/mainpane/msgview/current%.*s", cast(uint)tn.length, tn.ptr);
    chiroSetOptionUInts(xname[0..xlen], msglistTopUId, msglistCurrUId);
  }

  private void saveThreadListPositions (immutable bool transaction=true) {
    if (!saveMsgListPositions) return;
    saveMsgListPositions = false;
    if (lastDecodedTid == 0) return;
    if (transaction) {
      transacted!"Conf"(&saveThreadListPositionsInternal);
    } else {
      saveThreadListPositionsInternal();
    }
  }

  private void loadThreadListPositions () {
    saveMsgListPositions = false;
    msglistTopUId = msglistCurrUId = 0;
    if (lastDecodedTid == 0) return;
    import core.stdc.stdio : snprintf;
    char[128] xname = void;
    const(char)[] tn = lastDecodedTagName.getData;
    auto xlen = snprintf(xname.ptr, xname.sizeof, "/mainpane/msgview/current%.*s", cast(uint)tn.length, tn.ptr);
    chiroGetOptionUInts(ref msglistTopUId, ref msglistCurrUId, xname[0..xlen]);
    setupUnreadTimer();
  }


  private int getFolderMonthLimit () {
    version(none) {
      import core.stdc.stdio : snprintf;
      char[1024] xname = void;
      if (lastDecodedTid == 0) return chiroGetOption!int("/mainpane/msgview/monthlimit", 6);
      bool exists;
      const(char)[] tn = lastDecodedTagName.getData;
      for (;;) {
        auto len = snprintf(xname.ptr, xname.sizeof, "/mainpane/msgview/monthlimit%.*s", cast(uint)tn.length, tn.ptr);
        int v = chiroGetOptionEx!int(xname[0..len], out exists);
        //writeln(tn, " :: ", v, " :: ", exists);
        if (exists) return v;
        auto slp = tn.lastIndexOf('/', 1);
        if (slp <= 0) break;
        tn = tn[0..slp];
      }
      return chiroGetOption!int("/mainpane/msgview/monthlimit", 6);
    } else {
      if (lastDecodedTid == 0) return chiroGetOption!int("/mainpane/msgview/monthlimit", 6);
      return chiroGetTagMonthLimit(lastDecodedTagName.getData, 6);
    }
  }


  void rescanFolders (bool forced=false) {
    if (!.rescanFolders()) { if (!forced) return; }
    folderTopIndex = folderCurrIndex = 0;
    bool foundTop = false, foundCurr = false;
    foreach (immutable idx, const FolderInfo fi; folderList) {
      if (!foundTop && fi.name == folderTopTag) { foundTop = true; folderTopIndex = cast(int)idx; }
      if (!foundCurr && fi.name == folderCurrTag) { foundCurr = true; folderCurrIndex = cast(int)idx; }
    }
    if (!foundTop) folderTopTag.clear();
    if (!foundCurr) folderCurrTag.clear();
  }

  void normTopIndex () {
    if (folderList.length) {
      if (folderTopIndex >= folderList.length) folderTopIndex = cast(uint)folderList.length-1;
      int hgt = screenHeight/(gxTextHeightUtf+2)-1;
      if (hgt < 1) hgt = 1;
      if (folderList.length > hgt && folderTopIndex > folderList.length-hgt-1) {
        folderTopIndex = folderList.length-hgt-1;
      }
      if (folderTopTag != folderList[folderTopIndex].name) folderTopTag = folderList[folderTopIndex].name;
    }
  }

  bool folderScrollUp () {
    int oldidx = folderTopIndex;
    if (folderTopIndex > 0) {
      --folderTopIndex;
      normTopIndex();
    }
    return (folderTopIndex != oldidx);
  }

  bool folderScrollDown () {
    int oldidx = folderTopIndex;
    if (folderList.length > 1) {
      ++folderTopIndex;
      normTopIndex();
    }
    return (folderTopIndex != oldidx);
  }

  void folderMakeCurVisible () {
    rescanFolders();
    if (folderList.length) {
      if (folderCurrIndex >= folderList.length) folderCurrIndex = cast(uint)folderList.length-1;
      if (folderTopIndex >= folderList.length) folderTopIndex = cast(uint)folderList.length-1;
      if (folderCurrIndex-3 < folderTopIndex) {
        folderTopIndex = folderCurrIndex-3;
        if (folderTopIndex < 0) folderTopIndex = 0;
      }
      int hgt = screenHeight/(gxTextHeightUtf+2)-1;
      if (hgt < 1) hgt = 1;
      if (folderCurrIndex+2 > folderTopIndex+hgt) {
        folderTopIndex = (folderCurrIndex+2 > hgt ? folderCurrIndex+2-hgt : 0);
        if (folderTopIndex < 0) folderTopIndex = 0;
      }
      if (folderTopTag != folderList[folderTopIndex].name) folderTopTag = folderList[folderTopIndex].name;
      if (folderCurrTag != folderList[folderCurrIndex].name) folderCurrTag = folderList[folderCurrIndex].name;
    } else {
      folderCurrIndex = folderTopIndex = 0;
    }
  }

  private void purgeMessages (const uint tid) {
    import core.stdc.stdio : snprintf;
    char[128] xname = void;
    int xlen;
    if (tid != 0) {
      auto tname = chiroGetTagName(tid);
      const(char)[] tn = tname.getData;
      xlen = cast(int)snprintf(xname.ptr, xname.sizeof, "/mainpane/msgview/current%.*s",
                               cast(uint)tn.length, tn.ptr);
      // also, move down if deleted
      uint topUid = 0, currUid = 0;
      // get positions
      if (tid == lastDecodedTid) {
        topUid = msglistTopUId;
        currUid = msglistCurrUId;
      } else {
        chiroGetOptionUInts(ref topUid, ref currUid, xname[0..xlen]);
      }
      // move
      immutable uint origmsgid = currUid;
      int pass = 0;
      do {
        immutable int app = chiroGetMessageAppearance(tid, currUid);
        if (app == Appearance.SoftDeletePurge) {
          immutable uint muid =
            (pass ? chiroGetTreePaneTableNextUid(currUid)   // pass 1: down
                  : chiroGetTreePaneTablePrevUid(currUid)); // pass 0: up
          if (!muid || muid == currUid) { // oops
            // restore position
            currUid = origmsgid;
            // change direction
            pass += 1;
          } else {
            currUid = muid;
          }
        } else {
          pass = 3;
        }
      } while (pass < 2);
      //conwriteln("orig: ", origmsgid, "; new: ", currUid, "; xname:", xname[0..xlen]);
      // purge
      chiroDeletePurgedWithTag(tid);
      // save positions
      if (tid == lastDecodedTid) {
        msglistTopUId = topUid;
        msglistCurrUId = currUid;
      }
      // save new position
      if (tid == lastDecodedTid) {
        threadListPositionDirty();
      } else if (currUid != origmsgid) {
        transacted!"Conf"(() {
          chiroSetOptionUInts(xname[0..xlen], topUid, currUid);
        });
      }
    }
  }

  private void switchToFolderTid (const uint tid, bool forced=false) {
    //setupTrayAnimation();
    if (!forced && tid == lastDecodedTid) return;
    if (!forced) {
      purgeMessages(lastDecodedTid);
      if (tid != lastDecodedTid) purgeMessages(tid);
    }
    saveThreadListPositions();
    // rescan
    lastDecodedTid = tid;
    if (tid != 0) {
      lastDecodedTagName = chiroGetTagName(tid);
      immutable int monthlimit = getFolderMonthLimit();
      chiroCreateTreePaneTable(tid, lastmonthes:monthlimit);
      // load position
      loadThreadListPositions();
      // top
      if (!chiroIsTreePaneTableUidValid(msglistTopUId)) {
        msglistTopUId = chiroGetTreePaneTableIndex2Uid(0);
        threadListPositionDirty();
      }
      // current
      if (!chiroIsTreePaneTableUidValid(msglistCurrUId)) {
        msglistCurrUId = chiroGetTreePaneTableIndex2Uid(0);
        // find first unread, or position to the last
        if (!chiroGetMessageUnread(lastDecodedTid, msglistCurrUId)) {
          uint xid = chiroGetPaneNextUnread(msglistCurrUId);
          if (xid != 0) {
            msglistCurrUId = chiroGetTreePaneTablePrevUid(xid);
          } else {
            threadListEnd();
          }
        }
        threadListPositionDirty();
      }
      lastMaxTidUid = chiroGetTreePaneTableMaxUId();
      setupUnreadTimer();
    } else {
      lastDecodedTagName.clear();
      msglistTopUId = 0;
      msglistCurrUId = 0;
      chiroClearTreePaneTable();
    }
  }

  void folderSetToIndex (int idx) {
    if (idx < 0 || idx >= folderList.length) return;
    if (idx == folderCurrIndex) return;
    folderCurrIndex = idx;
    folderCurrTag = folderList[folderCurrIndex].name;
    switchToFolderTid(folderList[folderCurrIndex].tagid);
  }

  bool folderUpOne () {
    if (folderList.length == 0) return false;
    if (folderCurrIndex <= 0) return false;
    --folderCurrIndex;
    folderCurrTag = folderList[folderCurrIndex].name;
    setupUnreadTimer();
    return true;
  }

  bool folderDownOne () {
    if (folderList.length == 0) return false;
    if (folderCurrIndex+1 >= cast(int)folderList.length) return false;
    ++folderCurrIndex;
    folderCurrTag = folderList[folderCurrIndex].name;
    setupUnreadTimer();
    return true;
  }

  bool threadListHome () {
    if (lastDecodedTid == 0) return false;
    immutable uint firstUid = chiroGetTreePaneTableFirstUid();
    if (!firstUid || firstUid == msglistCurrUId) return false;
    msglistCurrUId = firstUid;
    threadListPositionDirty();
    setupUnreadTimer();
    return true;
  }

  bool threadListEnd () {
    if (lastDecodedTid == 0) return false;
    immutable uint lastUid = chiroGetTreePaneTableLastUid();
    if (!lastUid || lastUid == msglistCurrUId) return false;
    msglistCurrUId = lastUid;
    threadListPositionDirty();
    setupUnreadTimer();
    return true;
  }

  bool threadListUp () {
    import iv.timer;
    if (lastDecodedTid == 0) return false;
    auto ctm = Timer(true);
    immutable uint prevUid = chiroGetTreePaneTablePrevUid(msglistCurrUId);
    ctm.stop;
    if (ChiroTimerExEnabled) writeln("threadListUp time: ", ctm);
    if (!prevUid || prevUid == msglistCurrUId) return false;
    msglistCurrUId = prevUid;
    threadListPositionDirty();
    setupUnreadTimer();
    return true;
  }

  bool threadListDown () {
    import iv.timer;
    if (lastDecodedTid == 0) return false;
    auto ctm = Timer(true);
    immutable uint nextUid = chiroGetTreePaneTableNextUid(msglistCurrUId);
    ctm.stop;
    if (ChiroTimerExEnabled) writeln("threadListDown time: ", ctm);
    if (!nextUid || nextUid == msglistCurrUId) return false;
    msglistCurrUId = nextUid;
    threadListPositionDirty();
    setupUnreadTimer();
    return true;
  }

  bool threadListScrollUp (bool movecurrent) {
    import iv.timer;
    if (lastDecodedTid == 0) return false;
    auto ctm = Timer(true);
    immutable uint topPrevUid = chiroGetTreePaneTablePrevUid(msglistTopUId);
    ctm.stop;
    if (ChiroTimerExEnabled) conwriteln("threadListScrollUp: prevtop time: ", ctm);
    if (!topPrevUid || topPrevUid == msglistTopUId) return false;
    ctm.restart;
    immutable uint currPrevUid = (movecurrent ? chiroGetTreePaneTablePrevUid(msglistCurrUId) : 0);
    ctm.stop;
    if (movecurrent && ChiroTimerExEnabled) conwriteln("threadListScrollUp: prevcurr time: ", ctm);
    if (movecurrent && !currPrevUid) return false;
    msglistTopUId = topPrevUid;
    if (movecurrent) {
      msglistCurrUId = currPrevUid;
      setupUnreadTimer();
    }
    threadListPositionDirty();
    return true;
  }

  bool threadListScrollDown (bool movecurrent) {
    import iv.timer;
    if (lastDecodedTid == 0) return false;
    auto ctm = Timer(true);
    immutable uint currNextUid = (movecurrent ? chiroGetTreePaneTableNextUid(msglistCurrUId) : 0);
    ctm.stop;
    if (movecurrent && ChiroTimerExEnabled) writeln("threadListScrollDown: nextcurr time: ", ctm);
    if (movecurrent && (!currNextUid || currNextUid == msglistCurrUId)) return false;
    ctm.restart;
    immutable uint topNextUid = chiroGetTreePaneTableNextUid(msglistTopUId);
    ctm.stop;
    if (ChiroTimerExEnabled) writeln("threadListScrollDown: nexttop time: ", ctm);
    if (!topNextUid) return false;
    msglistTopUId = topNextUid;
    if (movecurrent) {
      msglistCurrUId = currNextUid;
      setupUnreadTimer();
    }
    threadListPositionDirty();
    return true;
  }

  bool threadListPageUp () {
    import iv.timer;
    bool res = false;
    int hgt = guiThreadListHeight/gxTextHeightUtf-1;
    if (hgt < 1) hgt = 1;
    auto ctm = Timer(true);
    foreach (; 0..hgt) {
      if (!threadListScrollUp(movecurrent:true)) break;
      res = true;
    }
    ctm.stop;
    if (ChiroTimerExEnabled) writeln("threadListPageUp time: ", ctm);
    if (res) setupUnreadTimer();
    return res;
  }

  bool threadListPageDown () {
    import iv.timer;
    bool res = false;
    int hgt = guiThreadListHeight/gxTextHeightUtf-1;
    if (hgt < 1) hgt = 1;
    auto ctm = Timer(true);
    foreach (; 0..hgt) {
      if (!threadListScrollDown(movecurrent:true)) break;
      res = true;
    }
    ctm.stop;
    if (ChiroTimerExEnabled) writeln("threadListPageDown time: ", ctm);
    if (res) setupUnreadTimer();
    return res;
  }


  // //////////////////////////////////////////////////////////////////// //
  static struct WebLink {
    int ly; // in lines
    int x, len; // in pixels
    dynstring url;
    dynstring text; // visual text
    int attachnum = -1;
    bool nofirst = false;

    @property bool isAttach () const pure nothrow @safe @nogc { return (attachnum >= 0); }
  }

  WebLink[] emurls;
  int lastUrlIndex = -1;

  void appendUrl (in ref WebLink wl) {
    immutable oldcap = emurls.capacity;
    if (oldcap && emurls.length < oldcap) {
      import core.stdc.string : memset;
      memset(emurls.ptr+emurls.length, 0, (oldcap-emurls.length)*emurls[0].sizeof);
    }
    emurls ~= WebLink();
    if (emurls.capacity > oldcap) {
      import core.stdc.string : memset;
      memset(emurls.ptr+oldcap, 0, (emurls.capacity-oldcap)*emurls[0].sizeof);
    }
    emurls[$-1] = wl;
  }

  void appendEmLine () {
    immutable oldcap = emlines.capacity;
    if (oldcap && emlines.length < oldcap) {
      import core.stdc.string : memset;
      memset(emlines.ptr+emlines.length, 0, (oldcap-emlines.length)*emlines[0].sizeof);
    }
    emlines ~= dynstring();
    if (emlines.capacity > oldcap) {
      import core.stdc.string : memset;
      memset(emlines.ptr+oldcap, 0, (emlines.capacity-oldcap)*emlines[0].sizeof);
    }
  }

  void appendEmLine (in ref dynstring s) {
    appendEmLine();
    emlines[$-1] = s;
  }

  void appendEmLine (const(char)[] buf) {
    appendEmLine();
    emlines[$-1] = dynstring(buf);
  }

  void clearDecodedText () {
    import core.stdc.string : memset;
    if (emlines.length) {
      //{ import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "000: *** clearing %u emlines (%u) ***\n", cast(uint)emlines.length, cast(uint)emlines.capacity); }
      foreach (ref dynstring s; emlines) s.clear();
      //{ import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "001: *** clearing %u emlines (%u) ***\n", cast(uint)emlines.length, cast(uint)emlines.capacity); }
      immutable maxsz = (emlines.capacity > emlines.length ? emlines.capacity : emlines.length);
      memset(emlines.ptr, 0, maxsz*emlines[0].sizeof);
      //{ import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "002: *** clearing %u emlines (%u) ***\n", cast(uint)emlines.length, cast(uint)emlines.capacity); }
      emlines.length = 0;
      //{ import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "003: *** clearing %u emlines (%u) ***\n", cast(uint)emlines.length, cast(uint)emlines.capacity); }
      //!if (emlines.capacity) emlines.assumeSafeAppend;
      //{ import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "004: *** clearing %u emlines (%u) ***\n", cast(uint)emlines.length, cast(uint)emlines.capacity); }
      if (emlines.capacity) memset(emlines.ptr, 0, emlines.capacity*emlines[0].sizeof);
      //{ import core.stdc.stdio : stderr, fprintf; fprintf(stderr, "005: *** clearing %u emlines (%u) ***\n", cast(uint)emlines.length, cast(uint)emlines.capacity); }
    }
    if (emurls.capacity || emurls.length) {
      foreach (ref WebLink l; emurls) { l.url.clear(); l.text.clear(); }
      immutable maxsz = (emurls.capacity > emurls.length ? emurls.capacity : emurls.length);
      memset(emurls.ptr, 0, maxsz*emurls[0].sizeof);
      emurls.length = 0;
      //!if (emurls.capacity) emurls.assumeSafeAppend;
      if (emurls.capacity) memset(emurls.ptr, 0, emurls.capacity*emurls[0].sizeof);
    }
    lastDecodedUId = 0;
    articleTextTopLine = 0;
    articleDestTextTopLine = 0;
    lastUrlIndex = -1;
    arttoname.clear();
    arttomail.clear();
    artfromname.clear();
    artfrommail.clear();
    artsubj.clear();
    arttime.clear();
  }

  // <0: not on url
  int findUrlIndexAt (int mx, int my) {
    int tpX0 = guiGroupListWidth+2+1;
    int tpX1 = screenWidth-1-guiMessageTextLPad*2-guiScrollbarWidth;
    int tpY0 = guiThreadListHeight+1;
    int tpY1 = screenHeight-1;

    int y = tpY0+linesInHeader*gxTextHeightUtf+2+1+guiMessageTextVPad;

    if (mx < tpX0 || mx > tpX1) return -1;
    if (my < y || my > tpY1) return -1;

    mx -= tpX0;

    // yeah, i can easily calculate this, i know
    uint idx = articleTextTopLine;
    while (idx < emlines.length && y < screenHeight) {
      if (my >= y && my < y+gxTextHeightUtf) {
        foreach (immutable uidx, const ref WebLink wl; emurls) {
          //conwriteln("checking url [#", uidx, "]; idx=", idx, "; ly=", wl.ly);
          if (wl.ly == idx) {
            if (mx >= wl.x && mx < wl.x+wl.len) return cast(int)uidx;
          }
        }
      }
      ++idx;
      y += gxTextHeightUtf+guiMessageTextInterline;
    }

    return -1;
  }

  WebLink* findUrlAt (int mx, int my) {
    auto uidx = findUrlIndexAt(mx, my);
    return (uidx >= 0 ? &emurls[uidx] : null);
  }

  private void emlDetectUrls (uint textlines) {
    lastUrlIndex = -1;


    if (textlines > emlines.length) textlines = cast(uint)emlines.length; // just in case
    foreach (immutable cy, dynstring s; emlines[0..textlines]) {
      if (s.length == 0) continue;
      detectUrl(s.getData(), 0, (const(char)[] url, usize spos, usize epos) {
        WebLink wl;
        wl.nofirst = (spos > 0);
        wl.ly = cast(int)cy;
        auto kr = GxKerning(4, 0);
        s[0..epos].utfByDCharSPos(delegate (dchar ch, usize stpos) {
          int w = kr.fixWidthPre(ch);
          if (stpos == spos) wl.x = w;
        });
        wl.len = kr.finalWidth-wl.x;
        wl.url = wl.text = s[spos..epos];
        for (;;) {
          auto pp = wl.url.indexOf("..");
          if (pp < 0) break;
          wl.url = wl.url[0..pp]~wl.url[pp+1..$];
        }
        appendUrl(wl); //emurls ~= wl;
        return true; // go on
      });
    }

    int attachcount = 0;
    foreach (immutable uint cy; textlines..cast(uint)emlines.length) {
      dynstring s = emlines[cy];
      if (s.length == 0) continue;
      auto spos = s.indexOf("attach:");
      if (spos < 0) continue;
      auto epos = spos+7;
      while (epos < s.length && s.ptr[epos] > ' ') ++epos;
      //if (attachcount >= parts.length) break;
      WebLink wl;
      wl.nofirst = (spos > 0);
      wl.ly = cast(int)cy;
      //wl.x = gxTextWidthUtf(s[0..spos]);
      //wl.len = gxTextWidthUtf(s[spos..epos]);
      auto kr = GxKerning(4, 0);
      s[0..epos].utfByDCharSPos(delegate (dchar ch, usize stpos) {
        int w = kr.fixWidthPre(ch);
        if (stpos == spos) wl.x = w;
      });
      wl.len = kr.finalWidth-wl.x;
      wl.url = wl.text = s[spos..epos];
      //if (spos > 0) ++wl.x; // this makes text bolder, no need to
      wl.attachnum = attachcount;
      //wl.attachfname = s[spos+7..epos];
      //wl.part = parts[attachcount];
      ++attachcount;
      appendUrl(wl); //emurls ~= wl;
    }
  }

  bool needToDecodeArticleTextNL (uint uid) const nothrow @trusted @nogc {
    return (lastDecodedUId != uid);
  }

  // fld is locked here
  void decodeArticleTextNL (uint uid) {
    static auto stmtGet = LazyStatement!"View"(`
      SELECT
          from_name AS fromName
        , from_mail AS fromMail
        ,  to_name AS toName
        , to_mail AS toMail
        , subj AS subj
        , datetime(threads.time, 'unixepoch') AS time
        , ChiroUnpack(content_text.content) AS text
        , ChiroUnpack(content_html.content) AS html
      FROM info
      INNER JOIN threads USING(uid)
      INNER JOIN content_text USING(uid)
      INNER JOIN content_html USING(uid)
      WHERE uid=:uid
      LIMIT 1
    ;`);

    if (!needToDecodeArticleTextNL(uid)) return;

    //if (uid == 0) { clearDecodedText(); return; }
    //conwriteln("200: em.len=", emlines.length);
    clearDecodedText();
    //conwriteln("201: em.len=", emlines.length);
    lastDecodedUId = uid;

    // get article content
    //FIXME: see `art.getTextContent()` for GPG decryption!
    DynStr artcontent;
    bool ishtml = false;
    bool htmlheader = false;
    foreach (auto row; stmtGet.st.bind(":uid", uid).range) {
      auto text = row.text!SQ3Text;
      auto html = row.html!SQ3Text;
      bool isHtml = ((text.xstrip.length == 0 || preferHtmlContent) && html.xstrip.length != 0);
      bool forceHtml = false;
      if (detectHtmlContent && !isHtml && html.xstrip.length == 0 && text.xstrip.length != 0) {
        auto tmp = text.xstrip;
        if (tmp.startsWithCI("<!DOCTYPE")) forceHtml = true;
      }
      //conwriteln("! ", text.xstrip.length, " ! ", html.xstrip.length);
      if (isHtml || forceHtml) {
        version(article_can_into_html) {
          try {
            string s = htmlToText((forceHtml ? text.idup : html.idup), false);
            //conwriteln("000: ac.len=", artcontent.length);
            artcontent ~= s;
            //conwriteln("001: ac.len=", artcontent.length);
            artcontent.removeASCIICtrls();
            //conwriteln("002: ac.len=", artcontent.length);
            htmlheader = true;
          } catch (Exception e) {
            artcontent ~= (forceHtml ? text : html);
          }
        } else {
          artcontent ~= (forceHtml ? text : html);
        }
        ishtml = true;
      } else if (text.length != 0) {
        artcontent ~= text;
      } else {
        artcontent ~= "no text content";
      }
      arttoname = row.toName!SQ3Text;
      arttomail = row.toMail!SQ3Text;
      artfromname = row.fromName!SQ3Text;
      artfrommail = row.fromMail!SQ3Text;
      artsubj = row.subj!SQ3Text;
      arttime = row.time!SQ3Text;
      if (artsubj.length == 0) artsubj = "no subject";
    }

    if (artcontent.length == 0) return; // no text
    //conwriteln("100: ac.len=", artcontent.length, "; em.len=", emlines.length);
    appendEmLine(); //emlines ~= dynstring(); // hack; this dummy line will be removed
    //conwriteln("101: ac.len=", artcontent.length, "; em.len=", emlines.length);
    bool skipEmptyLines = true;

    if (htmlheader) {
      appendEmLine("\x01==============================================");
      appendEmLine("\x01--- HTML CONTENT ---");
      appendEmLine("\x01==============================================");
      //emlines ~= null;
    }

    bool lastEndsWithSpace () { return (emlines[$-1].length ? emlines[$-1][$-1] == ' ' : false); }

    int lastQLevel = 0;

    static dynstring addQuotes (dynstring s, int qlevel) {
      enum QuoteStr = ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ";
      if (qlevel <= 0) return s;
      if (qlevel > QuoteStr.length-1) qlevel = cast(int)QuoteStr.length-1;
      return dynstring(QuoteStr[$-qlevel-1..$])~s;
    }

    // returns quote level
    static int removeQuoting (ref ConString s) {
      // calculate quote level
      int qlevel = 0;
      if (s.length && s[0] == '>') {
        usize lastqpos = 0, pos = 0;
        while (pos < s.length) {
          if (s.ptr[pos] != '>') {
            if (s.ptr[pos] != ' ') break;
          } else {
            lastqpos = pos;
            ++qlevel;
          }
          ++pos;
        }
        if (s.length-lastqpos > 1 && s.ptr[lastqpos+1] == ' ') ++lastqpos;
        ++lastqpos;
        s = s[lastqpos..$];
      }
      return qlevel;
    }

    bool inCode = false;

    void putLine (ConString s) {
      int qlevel = (ishtml ? 0 : removeQuoting(s));
      //conwriteln("qlevel=", qlevel, "; s=[", s, "]");
      // empty line: just insert it
      if (s.length == 0) {
        if (skipEmptyLines) return;
        if (qlevel == 0 && emlines[$-1].xstripright.length == 0) return;
        appendEmLine(addQuotes(dynstring(), qlevel).xstripright);
      } else {
        if (s.xstrip.length == 0) {
          if (skipEmptyLines) return;
          if (qlevel == 0 && emlines[$-1].xstripright.length == 0) return;
        }
        skipEmptyLines = false;
        // join code lines if it is possible
        if (inCode && qlevel == lastQLevel && lastEndsWithSpace) {
          //conwriteln("<", s, ">");
          emlines[$-1] ~= addQuotes(dynstring(s.xstrip), qlevel);
          return;
        }
        // two spaces at the beginning usually means "this is code"; don't wrap it
        if (s.length >= 1 && s[0] == '\t') {
          appendEmLine(addQuotes(dynstring(s), qlevel));
          // join next lines if it is possible
          inCode = true;
          //conwriteln("[", s, "]");
          lastQLevel = qlevel;
          return;
        }
        inCode = false;
        // can we append?
        bool newline = false;
        if (lastQLevel != qlevel || !lastEndsWithSpace) {
          newline = true;
        } else {
          // append words
          //while (s.length > 1 && s.ptr[0] <= ' ') s = s[1..$];
        }
        while (s.length) {
          usize epos = 0;
          if (newline) {
            while (epos < s.length && s.ptr[epos] <= ' ') ++epos;
          } else {
            //assert(s[0] > ' ');
            while (epos < s.length && s.ptr[epos] <= ' ') ++epos;
          }
          while (epos < s.length && s.ptr[epos] > ' ') ++epos;
          auto xlen = epos;
          while (epos < s.length && s.ptr[epos] <= ' ') ++epos;
          if (!newline && emlines[$-1].length+xlen <= 80) {
            // no wrapping, continue last line
            emlines[$-1] ~= s[0..epos];
          } else {
            newline = false;
            // wrapping; new line
            appendEmLine(addQuotes(dynstring(s[0..epos]), qlevel));
          }
          s = s[epos..$];
        }
        if (newline) appendEmLine(addQuotes(dynstring(), qlevel));
      }
      lastQLevel = qlevel;
    }

    try {
      //foreach (ConString s; LineIterator!false(art.getTextContent)) putLine(s);
      const(char)[] buf = artcontent;
      while (buf.length) {
        usize epos = skipOneLine(buf, 0);
        usize eend = epos;
             if (eend >= 2 && buf[eend-2] == '\r' && buf[eend-1] == '\n') eend -= 2;
        else if (eend >= 1 && buf[eend-1] == '\n') eend -= 1;
        putLine(buf[0..eend]);
        buf = buf[epos..$];
      }
      // remove first dummy line
      if (emlines.length) emlines = emlines[1..$];
      // remove trailing empty lines
      while (emlines.length && emlines[$-1].xstrip.length == 0) emlines.length -= 1;
    } catch (Exception e) {
      conwriteln("================================= ERROR: ", e.msg, " =================================");
      conwriteln(e.toString);
    }


    // attaches
    auto lcount = cast(uint)emlines.length;
    emlinesBeforeAttaches = lcount;

    uint attcount = 0;
    static auto statAttaches = LazyStatement!"View"(`
      SELECT
          idx AS idx
        , mime AS mime
        , name AS name
      FROM attaches
      WHERE uid=:uid
      ORDER BY idx
    ;`);
    foreach (auto row; statAttaches.st.bind(":uid", uid).range) {
      import core.stdc.stdio : snprintf;
      if (attcount == 0) { appendEmLine(); appendEmLine(); }
      DynStr att;
      char[128] buf = void;
      //if (type.length == 0) type = "unknown/unknown";
      //string s = " [%02s] attach:%s -- %s".format(attcount, Article.fixAttachmentName(filename), type);
      auto mime = row.mime!SQ3Text;
      auto name = row.name!SQ3Text/*.decodeSubj*/;
      auto len = snprintf(buf.ptr, buf.sizeof, " [%2u] attach:%.*s -- %.*s",
        attcount, cast(uint)name.length, name.ptr, cast(uint)mime.length, mime.ptr);
      assert(len > 0);
      if (len > buf.sizeof) len = cast(int)buf.sizeof;
      appendEmLine(buf[0..len]);
      ++attcount;
    }
    /*FIXME
    art.forEachAttachment(delegate(ConString type, ConString filename) {
      if (attcount == 0) { emlines ~= null; emlines ~= null; }
      import std.format : format;
      if (type.length == 0) type = "unknown/unknown";
      string s = " [%02s] attach:%s -- %s".format(attcount, Article.fixAttachmentName(filename), type);
      emlines ~= s;
      ++attcount;
      return false;
    });
    */
    emlDetectUrls(lcount);
  }

  @property int visibleArticleLines () {
    int y = guiThreadListHeight+1+linesInHeader*gxTextHeightUtf+2+guiMessageTextVPad;
    return (screenHeight-y)/(gxTextHeightUtf+guiMessageTextInterline);
  }

  void normalizeArticleTopLine () {
    int lines = visibleArticleLines;
    if (lines < 1 || emlines.length <= lines) {
      articleTextTopLine = 0;
      articleDestTextTopLine = 0;
    } else {
      if (articleTextTopLine < 0) articleTextTopLine = 0;
      if (articleTextTopLine+lines > emlines.length) {
        articleTextTopLine = cast(int)emlines.length-lines;
        if (articleTextTopLine < 0) articleTextTopLine = 0;
      }
    }
  }

  void doScrollStep () {
    auto oldtop = articleTextTopLine;
    foreach (immutable _; 0..6) {
      normalizeArticleTopLine();
      if (articleDestTextTopLine < articleTextTopLine) {
        --articleTextTopLine;
      } else if (articleDestTextTopLine > articleTextTopLine) {
        ++articleTextTopLine;
      } else {
        break;
      }
      normalizeArticleTopLine();
    }
    if (articleTextTopLine == oldtop) {
      // can't scroll anymore
      articleDestTextTopLine = articleTextTopLine;
      return;
    }
    postScreenRebuild();
    postArticleScroll();
  }

  void scrollBy (int delta) {
    articleDestTextTopLine += delta;
    doScrollStep();
  }

  void scrollByPageUp () {
    int lines = visibleArticleLines-scrollKeepLines;
    if (lines < 1) lines = 1;
    scrollBy(-lines);
  }

  void scrollByPageDown () {
    int lines = visibleArticleLines-scrollKeepLines;
    if (lines < 1) lines = 1;
    scrollBy(lines);
  }

  // ////////////////////////////////////////////////////////////////// //
  // this also fixes current/top uids
  void getAndFixThreadListIndicies (out int topidx, out int curridx) {
    int hgt = guiThreadListHeight/gxTextHeightUtf-1;
    if (hgt < 1) hgt = 1;

    topidx = chiroGetTreePaneTableUid2Index(msglistTopUId);
    immutable origTopIdx = topidx;
    if (topidx < 0) topidx = 0;
    curridx = chiroGetTreePaneTableUid2Index(msglistCurrUId);
    immutable origCurrIdx = curridx;
    if (curridx < 0) curridx = 0;

    //conwriteln("topuid: ", msglistTopUId, "; topidx=", topidx);
    //conwriteln("curruid: ", msglistCurrUId, "; curridx=", curridx);

    if (curridx-3 < topidx) {
      topidx = curridx-3;
      if (topidx < 0) topidx = 0;
    }
    if (curridx+3 > topidx+hgt) {
      topidx = (curridx+3 > hgt ? curridx+3-hgt : 0);
      if (topidx < 0) topidx = 0;
    }

    if (origCurrIdx != curridx || msglistCurrUId == 0) {
      immutable ocurr = msglistCurrUId;
      msglistCurrUId = chiroGetTreePaneTableIndex2Uid(curridx);
      if (ocurr != msglistCurrUId) {
        threadListPositionDirty();
        setupUnreadTimer();
      }
    }

    if (origTopIdx != topidx || msglistTopUId == 0) {
      immutable otop = msglistTopUId;
      msglistTopUId = chiroGetTreePaneTableIndex2Uid(topidx);
      if (otop != msglistTopUId) threadListPositionDirty();
    }
  }

  void setupUnreadTimer () {
    postMarkAsUnreadEvent();
  }

  // //////////////////////////////////////////////////////////////////// //
  //TODO: move parts to widgets
  override void onPaint () {
    viewDataVersion = dbView.getDataVersion();
    gxClipReset();

    gxFillRect(0, 0, guiGroupListWidth, screenHeight, getColor("grouplist-back"));
    gxVLine(guiGroupListWidth, 0, screenHeight, getColor("grouplist-divline"));

    gxFillRect(guiGroupListWidth+1, 0, screenWidth, guiThreadListHeight, getColor("threadlist-back"));
    gxHLine(guiGroupListWidth+1, guiThreadListHeight, screenWidth, getColor("threadlist-divline"));

    // ////////////////////////////////////////////////////////////////// //
    void drawArticle (uint uid) {
      import core.stdc.stdio : snprintf;
      import std.format : format;
      import std.datetime;
      char[128] tbuf;
      const(char)[] tbufs;

      void xfmt (string s, const(char)[][] strs...) {
        int dpos = 0;
        void puts (const(char)[] s...) {
          foreach (char ch; s) {
            if (dpos >= tbuf.length) break;
            tbuf[dpos++] = ch;
          }
        }
        while (s.length) {
          if (strs.length && s.length > 1 && s[0] == '%' && s[1] == 's') {
            puts(strs[0]);
            strs = strs[1..$];
            s = s[2..$];
          } else {
            puts(s[0]);
            s = s[1..$];
          }
        }
        tbufs = tbuf[0..dpos];
      }

      if (needToDecodeArticleTextNL(uid)) {
        decodeArticleTextNL(uid);
      }

      /*
      gxClipX0 = guiGroupListWidth+2;
      gxClipX1 = screenWidth-1;
      gxClipY0 = guiThreadListHeight+1;
      gxClipY1 = screenHeight-1;
      */
      gxClipRect = GxRect(GxPoint(guiGroupListWidth+2, guiThreadListHeight+1), GxPoint(screenWidth-1, screenHeight-1));

      int msx = lastMouseX;
      int msy = lastMouseY;

      int curDrawYMul = 1;

      // header
      immutable int hdrHeight = (3+(arttoname.length || arttomail.length ? 1 : 0))*gxTextHeightUtf+2;
      gxFillRect(gxClipRect.x0, gxClipRect.y0, gxClipRect.x1-gxClipRect.x0+1, hdrHeight, getColor("msg-header-back"));

      if (artfromname.length) {
        xfmt("From: %s <%s>", artfromname, artfrommail);
      } else {
        xfmt("From: %s", artfrommail);
      }
      gxDrawTextUtf(gxClipRect.x0+guiMessageTextLPad, gxClipRect.y0+0*gxTextHeightUtf+1, tbufs, getColor("msg-header-from"));
      if (arttoname.length || arttomail.length) {
        if (arttoname.length) {
          xfmt("To: %s <%s>", arttoname, arttomail);
        } else {
          xfmt("To: %s", arttomail);
        }
        gxDrawTextUtf(gxClipRect.x0+guiMessageTextLPad, gxClipRect.y0+curDrawYMul*gxTextHeightUtf+1, tbufs, getColor("msg-header-to"));
        ++curDrawYMul;
      }
      xfmt("Subject: %s", (artsubj.length ? artsubj : "no subject"));
      gxDrawTextUtf(gxClipRect.x0+guiMessageTextLPad, gxClipRect.y0+curDrawYMul*gxTextHeightUtf+1, tbufs, getColor("msg-header-subj"));
      ++curDrawYMul;
      {
        //auto t = SysTime.fromUnixTime(arttime);
        //auto tlen = snprintf(tbuf.ptr, tbuf.length, "Date: %04d/%02d/%02d %02d:%02d:%02d", t.year, t.month, t.day, t.hour, t.minute, t.second);
        auto tlen = snprintf(tbuf.ptr, tbuf.length, "Date: %.*s", cast(uint)arttime.length, arttime.ptr);
        gxDrawTextUtf(gxClipRect.x0+guiMessageTextLPad, gxClipRect.y0+curDrawYMul*gxTextHeightUtf+1, tbuf[0..tlen], getColor("msg-header-date"));
        ++curDrawYMul;
      }


      // text
      linesInHeader = curDrawYMul;
      int y = gxClipRect.y0+curDrawYMul*gxTextHeightUtf+2;

      gxHLine(gxClipRect.x0, y, gxClipRect.x1-gxClipRect.x0+1, getColor("msg-header-divline"));
      ++y;
      gxFillRect(gxClipRect.x0, y, gxClipRect.x1-gxClipRect.x0+1, screenHeight-y, getColor("msg-text-back"));
      y += guiMessageTextVPad;

      immutable sty = y;

      normalizeArticleTopLine();

      bool drawUpMark = (articleTextTopLine > 0);
      bool drawDownMark = false;

      immutable uint messageTextNormalColor = getColor("msg-text-text");
      immutable uint messageTextQuote0Color = getColor("msg-text-quote0");
      immutable uint messageTextQuote1Color = getColor("msg-text-quote1");
      immutable uint messageTextHtmlHeaderColor = getColor("msg-text-html-sign");
      immutable uint messageTextLinkColor = getColor("msg-text-link");
      immutable uint messageTextLinkHoverColor = getColor("msg-text-link-hover");
      immutable uint messageTextLinkPressedColor = getColor("msg-text-link-pressed");

      uint idx = articleTextTopLine;
      bool msvisible = isMouseVisible;
      bool checkQuotes = true;
      if (emlines.length && emlines[0].length && emlines[0][0] == 1) {
        // html content
        checkQuotes = false;
      }
      while (idx < emlines.length && y < screenHeight) {
        int qlevel = 0;
        dynstring s = emlines[idx];

        if (checkQuotes) {
          foreach (immutable char ch; s) {
            if (ch <= ' ') continue;
            if (ch != '>') break;
            ++qlevel;
          }
        }

        uint clr = messageTextNormalColor;
        if (qlevel) {
          final switch (qlevel%2) {
            case 0: clr = messageTextQuote0Color; break;
            case 1: clr = messageTextQuote1Color; break;
          }
        }

        if (!checkQuotes && s.length && s[0] == 1) {
          clr = messageTextHtmlHeaderColor;
          s = s[1..$];
        }

        gxDrawTextUtfOpt(GxDrawTextOptions.TabColor(4, clr), gxClipRect.x0+guiMessageTextLPad, y, s);

        foreach (const ref WebLink wl; emurls) {
          if (wl.ly == idx) {
            uint lclr = messageTextLinkColor;
            if (msvisible && msy >= y && msy < y+gxTextHeightUtf &&
                msx >= gxClipRect.x0+1+guiMessageTextLPad+wl.x &&
                msx < gxClipRect.x0+1+guiMessageTextLPad+wl.x+wl.len)
            {
              lclr = (lastMouseLeft ? messageTextLinkPressedColor : messageTextLinkHoverColor);
              //lclr = getColor("msg-text-link", (lastMouseLeft ? "pressed" : "hover"));
            }
            gxDrawTextUtfOpt(GxDrawTextOptions.TabColorFirstFull(4, lclr, wl.nofirst), gxClipRect.x0+guiMessageTextLPad+wl.x, y, wl.text);
          }
        }

        if (gxClipRect.y1-y < gxTextHeightUtf && emlines.length-idx > 0) drawDownMark = true;

        ++idx;
        y += gxTextHeightUtf+guiMessageTextInterline;
      }

      //gxDrawTextOutScaledUtf(1, gxClipRect.x1-gxTextWidthUtf(triangleDownStr)-3, sty, triangleDownStr, (drawUpMark ? gxRGB!(255, 255, 255) : gxRGB!(85, 85, 85)), gxRGB!(0, 69, 69));
      //gxDrawTextOutScaledUtf(1, gxClipRect.x1-gxTextWidthUtf(triangleUpStr)-3, gxClipRect.y1-7, triangleUpStr, (drawDownMark ? gxRGB!(255, 255, 255) : gxRGB!(85, 85, 85)), gxRGB!(0, 69, 69));

      gxDrawScrollBar(GxRect(gxClipRect.x1-10, sty+10, 5, gxClipRect.y1-sty-17), cast(int)emlines.length-1, idx-1);

      bool twited = false;
      DynStr twittext = chiroGetMessageTwit(lastDecodedTid, uid, out twited);
      if (twited) {
        immutable uint clr = getColor("twit-shade");
        /*
        foreach (immutable dy; gxClipRect.y0+3*gxTextHeightUtf+2..gxClipRect.y1+1) {
          foreach (immutable dx; gxClipRect.x0..gxClipRect.x1+1) {
            if ((dx^dy)&1) gxPutPixel(dx, dy, clr);
          }
        }
        */
        GxRect rc = gxClipRect;
        rc.y0 = rc.y0+3*gxTextHeightUtf+2;
        gxDashRect(rc, clr);

        if (twittext.length) {
          immutable ofdz = egraFontSize;
          scope(exit) egraFontSize = ofdz;
          egraFontSize = 150;
          int tx = gxClipRect.x0+(gxClipRect.width-gxTextWidthUtf(twittext))/2-1;
          int ty = gxClipRect.y0+(gxClipRect.height-gxTextHeightUtf)/2-1;
          gxDrawTextOutUtf(tx, ty, twittext, getColor("twit-text"), getColor("twit-outline"));
        }
      }
    }

    // ////////////////////////////////////////////////////////////////// //
    void drawThreadList () {
      uint tid = 0;
      if (folderCurrIndex >= 0 && folderCurrIndex < folderList.length) {
        tid = folderList[folderCurrIndex].tagid;
      }
      switchToFolderTid(tid);

      // find indicies
      int topidx, curridx;
      getAndFixThreadListIndicies(out topidx, out curridx);

      int hgt = (guiThreadListHeight+gxTextHeightUtf-1)/gxTextHeightUtf;
      if (hgt < 1) hgt = 1;

      gxClipRect.x0 = guiGroupListWidth+2;
      gxClipRect.x1 = screenWidth-1-5;
      gxClipRect.y0 = 0;
      gxClipRect.y1 = guiThreadListHeight-1;
      immutable uint origX0 = gxClipRect.x0;
      immutable uint origX1 = gxClipRect.x1;
      immutable uint origY0 = gxClipRect.y0;
      immutable uint origY1 = gxClipRect.y1;
      int y = 0;

      static uint darkenBy (in uint clr, in int val) pure nothrow @safe @nogc {
        pragma(inline, true);
        return
          val ?
            (clr&0xff_00_00_00)|
            (cast(uint)clampToByte(cast(int)(cast(ubyte)(clr>>16))-val)<<16)|
            (cast(uint)clampToByte(cast(int)(cast(ubyte)(clr>>8))-val)<<8)|
            (cast(uint)clampToByte(cast(int)(cast(ubyte)clr)-val)) :
          clr;
      }

      chiroGetPaneTablePage(topidx, hgt,
        delegate (int pgofs, /* offset from the page start, from zero and up to `limit` */
                  int iid, /* item id, never zero */
                  uint uid, /* msguid, never zero */
                  uint parentuid, /* parent msguid, may be zero */
                  uint level, /* threading level, from zero */
                  Appearance appearance, /* see above */
                  Mute mute, /* see above */
                  SQ3Text date, /* string representation of receiving date and time */
                  SQ3Text subj, /* message subject, can be empty string */
                  SQ3Text fromName, /* message sender name, can be empty string */
                  SQ3Text fromMail, /* message sender email, can be empty string */
                  SQ3Text title) /* title from twiting */
        {
          import std.format : format;
          import std.datetime;

          if (y >= guiThreadListHeight) return;
          if (subj.length == 0) subj = "no subject";

          gxClipRect.x0 = origX0;
          gxClipRect.x1 = origX1;

          int darken = (level != 0 && appearance != Appearance.Unread ? 40 : 0);

          string style = "normal";
          if (appearance == Appearance.Unread) {
            style = "unread";
            darken = 0;
          } else {
            if (mute > Mute.Normal) { style = "twit"; darken = 0; }
          }
          if (isSoftDeleted(appearance)) {
            darken = 0;
            style = (appearance == Appearance.SoftDeletePurge ? "hard-del" : "soft-del");
          }

          char[64] stfull = void;
          usize stpos = 0;
          if (uid == msglistCurrUId) {
            static immutable string sc = "cursor-";
            stpos = sc.length;
            stfull[0..stpos] = sc;
          }
          stfull[stpos..stpos+style.length] = style;
          const(char)[] stname = stfull[0..stpos+style.length];

          char[128] stx = void;
          const(char)[] buildStyle (const(char)[] stt) {
            static immutable string sc = "threadlist-";
            stx[0..sc.length] = sc;
            usize xpos = sc.length;
            stx[xpos..xpos+stname.length] = stname;
            xpos += stname.length;
            stx[xpos++] = '-';
            stx[xpos..xpos+stt.length] = stt;
            xpos += stt.length;
            return stx[0..xpos];
          }

          //conwriteln("STNAME: <", stname, ">; darken=", darken);

          immutable uint clrBack = getColor(buildStyle("back"));
          immutable uint clrOut = getColor(buildStyle("outline"));
          immutable uint clrFrom = darkenBy(getColor(buildStyle("from-text")), darken);
          immutable uint clrMail = darkenBy(getColor(buildStyle("mail-text")), darken);
          immutable uint clrSubj = darkenBy(getColor(buildStyle("subj-text")), darken);
          immutable uint clrTime = darkenBy(getColor(buildStyle("time-text")), darken);

          // background
          if (!gxIsTransparent(clrBack)) gxFillRect(gxClipRect.x0, y, gxClipRect.width-1, gxTextHeightUtf, clrBack);
          gxClipRect.x0 = gxClipRect.x0+1;
          gxClipRect.x1 = gxClipRect.x1-1;

          // time
          int timewdt = gxDrawTextOutUtf(gxClipRect.x1-gxTextWidthUtf(date), y, date, clrTime, clrOut);
          if (timewdt%8) timewdt = (timewdt|7)+1;

          SQ3Text from = fromName;
          {
            auto vp = from.indexOf(" via Digitalmars-");
            if (vp > 0) {
              from = from[0..vp].xstrip;
              if (from.length == 0) from = "anonymous";
            }
          }

          // from/mail
          gxClipRect.x1 = gxClipRect.x1-/*(13*6+4)*2+33*/timewdt;
          enum FromWidth = 22*6*2+88;
          gxDrawTextOutUtf(gxClipRect.x1-FromWidth, y, from, clrFrom, clrOut);
          gxDrawTextOutUtf(gxClipRect.x1-FromWidth+gxTextWidthUtf(from)+4, y, "<", clrMail, clrOut);
          gxDrawTextOutUtf(gxClipRect.x1-FromWidth+gxTextWidthUtf(from)+4+gxTextWidthUtf("<")+1, y, fromMail, clrMail, clrOut);
          gxDrawTextOutUtf(gxClipRect.x1-FromWidth+gxTextWidthUtf(from)+4+gxTextWidthUtf("<")+1+gxTextWidthUtf(fromMail)+1, y, ">", clrMail, clrOut);
          gxClipRect.x1 = gxClipRect.x1-FromWidth-6;

          // subj
          gxDrawTextOutUtf(gxClipRect.x0+level*3, y, subj, clrSubj, clrOut);

          // nesting dots
          if (level) {
            immutable uint clrDot = getColor(buildStyle("dots"));
            foreach (immutable dx; 0..level) gxPutPixel(gxClipRect.x0+1+dx*3, y+gxTextHeightUtf/2, clrDot);
          }

          // deleted strike line
          if (isSoftDeleted(appearance)) {
            immutable uint clrLine = getColor(buildStyle("strike-line"));
            if (!gxIsTransparent(clrLine)) {
              gxClipRect.x0 = origX0;
              gxClipRect.x1 = origX1;
              gxHLine(gxClipRect.x0, y+gxTextHeightUtf/2, gxClipRect.x1-gxClipRect.x0+1, clrLine);
              if (appearance == Appearance.SoftDeletePurge) {
                gxHLine(gxClipRect.x0, y+gxTextHeightUtf/2+1, gxClipRect.x1-gxClipRect.x0+1, clrLine);
              }
            }
          }

          y += gxTextHeightUtf;
        }
      );

      // draw progressbar
      if (msglistCurrUId) {
        gxClipRect.x0 = origX0;
        gxClipRect.x1 = origX1+5;
        gxClipRect.y0 = origY0;
        gxClipRect.y1 = origY1;
        gxDrawScrollBar(GxRect(gxClipRect.x1-5, gxClipRect.y0, 4, gxClipRect.height-1),
          cast(int)chiroGetTreePaneTableCount()-1, curridx);
      }

      drawArticle(msglistCurrUId);
    }

    // ////////////////////////////////////////////////////////////////// //
    void drawFolders () {
      immutable uint clrNormal = getColor("grouplist-normal-text");
      immutable uint clrNormalCursor = getColor("grouplist-cursor-normal-text");
      immutable uint clrNormalChild = getColor("grouplist-normal-child-text");
      immutable uint clrNormalChildCursor = getColor("grouplist-cursor-normal-child-text");
      immutable uint clrDots = getColor("grouplist-dots");
      immutable uint clrNormOutline = getColor("grouplist-outline");
      immutable uint clrCurOutline = getColor("grouplist-cursor-outline");

      folderMakeCurVisible();
      int ofsx = 2;
      int ofsy = 1;
      foreach (immutable idx, const FolderInfo fi; folderList) {
        if (idx < folderTopIndex) continue;
        if (ofsy >= screenHeight) break;
        gxClipReset();

        immutable int depth = fi.depth;
        uint clr = (depth ? clrNormalChild : clrNormal);
        uint clrOut = clrNormOutline;

        immutable bool isCursor = (idx == folderCurrIndex);
        if (isCursor) {
          gxFillRect(0, ofsy-1, guiGroupListWidth, (gxTextHeightUtf+2), getColor("grouplist-cursor-back"));
          clr = (depth ? clrNormalChildCursor : clrNormalCursor);
          clrOut = clrCurOutline;
        }
        gxClipRect.x0 = ofsx-1;
        gxClipRect.y0 = ofsy;
        gxClipRect.x1 = guiGroupListWidth-3;

        if (fi.unreadCount) {
          clr = getColor(isCursor ? "grouplist-cursor-unread-text" : "grouplist-unread-text");
        } else if (depth == 0) {
               if (fi.name == "#spam") clr = getColor(isCursor ? "grouplist-cursor-spam-text" : "grouplist-spam-text");
          else if (fi.name == "/accounts") clr = getColor(isCursor ? "grouplist-cursor-accounts-text" : "grouplist-accounts-text");
        } else if (depth == 1 && fi.name.startsWith("/accounts/")) {
          clr = getColor(isCursor ? "grouplist-cursor-accounts-child-text" : "grouplist-accounts-child-text");
        } else if (fi.name.startsWith("/accounts/") && fi.name.endsWith("/inbox")) {
          clr = getColor(isCursor ? "grouplist-cursor-inbox-text" : "grouplist-inbox-text");
        }
        foreach (immutable dd; 0..depth) gxPutPixel(ofsx+dd*6+2, ofsy+gxTextHeightUtf/2, clrDots);
        gxDrawTextOutUtf(ofsx+depth*6, ofsy, fi.visname, clr, clrOut);
        ofsy += gxTextHeightUtf+2;
      }
    }

    drawFolders();
    drawThreadList();
    //setupTrayAnimation();

    version(test_round_rect) {
      gxClipReset();
      gxFillRoundedRect(lastMouseX-16, lastMouseY-16, 128, 96, rrad, /*gxSolidWhite*/gxRGBA!(0, 255, 0, 127));
      //gxDrawRoundedRect(lastMouseX-16, lastMouseY-16, 128, 96, rrad, gxRGBA!(0, 255, 0, 127));
    }
  }

  version(test_round_rect) {
    int rrad = 16;
  }

  override bool onKeySink (KeyEvent event) {
    if (event.pressed) {
      version(test_round_rect) {
        if (event == "Plus") { ++rrad; postScreenRebuild(); return true; }
        if (event == "Minus") { --rrad; postScreenRebuild(); return true; }
      }
      if (dbg_dump_keynames) conwriteln("key: ", event.toStr, ": ", event.modifierState&ModifierState.windows);
      //foreach (const ref kv; mainAppKeyBindings.byKeyValue) if (event == kv.key) concmd(kv.value);
      char[64] kname;
      if (auto cmdp = event.toStrBuf(kname[]) in mainAppKeyBindings) {
        concmd(*cmdp);
        return true;
      }
      // debug
      /*
      if (event == "S-Up") {
        if (folderTop > 0) --folderTop;
        postScreenRebuild();
        return true;
      }
      if (event == "S-Down") {
        if (folderTop+1 < folders.length) ++folderTop;
        postScreenRebuild();
        return true;
      }
      */
      //if (event == "Tab") { new PostWindow(); return true; }
      //if (event == "Tab") { new SelectPopBoxWindow(null); return true; }
    }
    return super.onKeySink(event);
  }

  // returning `false` to avoid screen rebuilding by dispatcher
  override bool onMouseSink (MouseEvent event) {
    int mx = event.x;
    int my = event.y;
    // button press
    if (event.type == MouseEventType.buttonPressed && event.button == MouseButton.left) {
      // select folder
      if (mx >= 0 && mx < guiGroupListWidth && my >= 0 && my < screenHeight) {
        uint fnum = my/(gxTextHeightUtf+2)+folderTopIndex;
        if (fnum >= 0 && fnum != folderCurrIndex && folderCurrIndex < folderList.length) {
          folderCurrIndex = fnum;
          postScreenRebuild();
        }
        return false;
      }
      // select post
      if (mx > guiGroupListWidth && mx < screenWidth && my >= 0 && my < guiThreadListHeight) {
        if (lastDecodedTid != 0) {
          my /= gxTextHeightUtf;
          // find indicies
          int topidx, curridx;
          getAndFixThreadListIndicies(out topidx, out curridx);
          int newidx = topidx+my;
          if (curridx != newidx) {
            uint newuid = chiroGetTreePaneTableIndex2Uid(newidx);
            if (newuid && newuid != msglistCurrUId) {
              chiroSetMessageRead(lastDecodedTid, newuid);
              msglistCurrUId = newuid;
              setupTrayAnimation();
              postScreenRebuild();
            }
          }
          return false;
        }
      } else {
        auto uidx = findUrlIndexAt(mx, my);
        if (uidx != lastUrlIndex || lastUrlIndex >= 0) { lastUrlIndex = uidx; postScreenRebuild(); }
      }
    }
    // wheel
    if (event.type == MouseEventType.buttonPressed &&
        (event.button == MouseButton.wheelUp || event.button == MouseButton.wheelDown))
    {
      // folder
      if (mx >= 0 && mx < guiGroupListWidth && my >= 0 && my < screenHeight) {
        if (event.button == MouseButton.wheelUp) {
          if (folderScrollUp()) postScreenRebuild();
          /*
          if (folderCurrIndex > 0) {
            --folderCurrIndex;
            postScreenRebuild();
          }
          */
        } else {
          if (folderScrollDown()) postScreenRebuild();
          /*
          if (folderCurrIndex+1 < folderList.length) {
            ++folderCurrIndex;
            postScreenRebuild();
          }
          */
        }
        return false;
      }
      // post
      if (mx > guiGroupListWidth && mx < screenWidth && my >= 0 && my < guiThreadListHeight) {
        if (event.button == MouseButton.wheelUp) {
          //if (threadListUp()) postScreenRebuild();
          if (threadListScrollUp(movecurrent:false)) postScreenRebuild();
        } else {
          //if (threadListDown()) postScreenRebuild();
          if (threadListScrollDown(movecurrent:false)) postScreenRebuild();
        }
        return false;
      }
      // text
      if (mx > guiGroupListWidth && mx < screenWidth && my > guiThreadListHeight && my < screenHeight) {
        enum ScrollLines = 2;
        if (event.button == MouseButton.wheelUp) scrollBy(-ScrollLines); else scrollBy(ScrollLines);
        postScreenRebuild();
        return false;
      }
    }
    // button release
    if (event.type == MouseEventType.buttonReleased && event.button == MouseButton.left) {
      // try url
      auto uidx = findUrlIndexAt(mx, my);
      auto url = findUrlAt(mx, my);
      if (url !is null) {
        if (url.isAttach) {
          if (event.modifierState&(ModifierState.alt|ModifierState.shift|ModifierState.ctrl)) {
            concmdf!"attach_save %s"(url.attachnum);
          } else {
            concmdf!"attach_view %s"(url.attachnum);
          }
        } else {
          if (event.modifierState&(ModifierState.shift|ModifierState.ctrl)) {
            //conwriteln("link-to-clipboard: <", url.url, ">");
            setClipboardText(vbwin, url.url.getData.idup); // it is safe to cast here
            setPrimarySelection(vbwin, url.url.getData.idup); // it is safe to cast here
            conwriteln("url copied to the clipboard.");
          } else {
            //conwriteln("link-open: <", url.url, ">");
            concmdf!"open_url \"%s\" %s"(url.url, ((event.modifierState&ModifierState.alt) != 0));
          }
        }
        postScreenRebuild();
      } else {
        if (lastUrlIndex >= 0) postScreenRebuild();
      }
      lastUrlIndex = uidx;
    }
    if (event.type == MouseEventType.motion) {
      auto uidx = findUrlIndexAt(mx, my);
      if (uidx != lastUrlIndex) { lastUrlIndex = uidx; postScreenRebuild(); return false; }
    }

    /*
    if (event.type == MouseEventType.buttonPressed || event.type == MouseEventType.buttonReleased) {
      postScreenRebuild();
    } else {
      // for OpenGL, this rebuilds the whole screen anyway
      postScreenRepaint();
    }
    */

    return false;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared LockFile mainLockFile;


void checkMainLockFile () {
  import std.path : buildPath;
  mainLockFile = LockFile(buildPath(mailRootDir, ".chiroptera2.lock"));
  if (!mainLockFile.tryLock) {
    mainLockFile.close();
    //assert(0, "already running");
    conwriteln("another copy of Chiroptera is running, disabling updater.");
    receiverDisable();
  }
}


void main (string[] args) {
  {
    import etc.linux.memoryerror;
    bool setMH = true;
    int idx = 1;
    while (idx < args.length) {
      string a = args[idx++];
      if (a == "--") break;
      if (a == "--gdb") {
        setMH = false;
        --idx;
        foreach (immutable c; idx+1..args.length) args[c-1] = args[c];
      }
    }
    if (setMH) registerMemoryErrorHandler();
  }

  defaultColorStyle.parseStyle(ChiroStyle);

  glconAllowOpenGLRender = false;

  checkMainLockFile();
  scope(exit) mainLockFile.close();

  sdpyWindowClass = "Chiroptera";
  //glconShowKey = "M-Grave";

  initConsole();
  //FIXME
  //hitwitInitConsole();

  clearBindings();
  setupDefaultBindings();

  concmd("exec chiroptera.rc tan");
  concmd("load_style userstyle.rc");

  //FIXME
  //scanFolders();

  //FIXME:concmdf!"exec %s/accounts.rc tan"(mailRootDir);
  //FIXME:concmdf!"exec %s/addressbook.rc tan"(mailRootDir);
  //FIXME:concmdf!"exec %s/filters.rc tan"(mailRootDir);
  //FIXME:concmdf!"exec %s/highlights.rc tan"(mailRootDir);
  //FIXME:concmdf!"exec %s/twits.rc tan"(mailRootDir);
  //FIXME:concmdf!"exec %s/twit_threads.rc tan"(mailRootDir);
  //FIXME:concmdf!"exec %s/auto_twits.rc tan"(mailRootDir);
  //FIXME:concmdf!"exec %s/auto_twit_threads.rc tan"(mailRootDir);
  //FIXME:concmdf!"exec %s/repreps.rc tan"(mailRootDir);
  conProcessQueue(); // load config
  conProcessArgs!true(args);

  chiroOpenStorageDB();
  chiroOpenViewDB();
  chiroOpenConfDB();
  //ChiroTimerEnabled = true;
  //ChiroTimerExEnabled = true;

  rescanFolders();

  egraCreateSystemWindow("Chiroptera", allowResize:true);

  static if (is(typeof(&vbwin.closeQuery))) {
    vbwin.closeQuery = delegate () { concmd("quit"); egraPostDoConCommands(); };
  }


  vbwin.windowResized = delegate (int wdt, int hgt) {
    egraSdpyOnWindowResized(wdt, hgt);

    // TODO: fix gui sizes
    if (vbwin.closed) return;

    double glwFrac = cast(double)guiGroupListWidth/screenWidth;
    double tlhFrac = cast(double)guiThreadListHeight/screenHeight;

    if (wdt < screenEffScale*32) wdt = screenEffScale;
    if (hgt < screenEffScale*32) hgt = screenEffScale;
    int newwdt = (wdt+screenEffScale-1)/screenEffScale;
    int newhgt = (hgt+screenEffScale-1)/screenEffScale;

    guiGroupListWidth = cast(int)(glwFrac*newwdt+0.5);
    guiThreadListHeight = cast(int)(tlhFrac*newhgt+0.5);

    if (guiGroupListWidth < 12) guiGroupListWidth = 12;
    if (guiThreadListHeight < 16) guiThreadListHeight = 16;
  };


  vbwin.addEventListener((QuitEvent evt) {
    if (vbwin.closed) return;
    if (isQuitRequested) { vbwin.close(); return; }
    vbwin.close();
  });


  vbwin.addEventListener((TrayAnimationStepEvent evt) {
    if (vbwin.closed) return;
    if (isQuitRequested) { vbwin.close(); return; }
    trayDoAnimationStep();
  });


  HintWindow uphintWindow;

  vbwin.addEventListener((UpdatingAccountEvent evt) {
    DynStr accName = chiroGetAccountName(evt.accid);
    if (accName.length) {
      DynStr msg = "updating: ";
      msg ~= accName;
      if (uphintWindow !is null) {
        uphintWindow.message = msg;
      } else {
        uphintWindow = new HintWindow(msg);
        uphintWindow.y0 = guiThreadListHeight+1+(3*gxTextHeightUtf+2-uphintWindow.height)/2;
      }
      postScreenRebuild();
    }
  });

  vbwin.addEventListener((UpdatingAccountCompleteEvent evt) {
    if (uphintWindow is null) return;
    DynStr accName = chiroGetAccountName(evt.accid);
    if (accName.length) {
      DynStr msg = "done: ";
      msg ~= accName;
      uphintWindow.message = msg;
      postScreenRebuild();
    }
  });

  vbwin.addEventListener((UpdatingCompleteEvent evt) {
    if (uphintWindow) {
      uphintWindow.close();
      uphintWindow = null;
    }
    if (vbwin is null || vbwin.closed) return;
    setupTrayAnimation(); // check if we have to start/stop animation, and do it
    postScreenRebuild();
  });

  vbwin.addEventListener((TagThreadsUpdatedEvent evt) {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    if (evt.tagid && mainPane.lastDecodedTid == evt.tagid) {
      // force view pane rebuild
      mainPane.switchToFolderTid(evt.tagid, forced:true);
      postScreenRebuild();
    }
  });


  ProgressWindow recalcHintWindow;

  vbwin.addEventListener((RecalcAllTwitsEvent evt) {
    if (vbwin !is null && !vbwin.closed) {
      glconHide();
      if (recalcHintWindow !is null) recalcHintWindow.close();
      recalcHintWindow = new ProgressWindow("recalculating twits");
      egraRebuildScreen();
    } else {
      recalcHintWindow = null;
    }
    {
      disableMailboxUpdates();
      scope(exit) enableMailboxUpdates();
      chiroRecalcAllTwits((msg, curr, total) {
        if (recalcHintWindow is null) return;
        if (recalcHintWindow.setProgress(msg, curr, total)) {
          egraRebuildScreen();
        }
      });
    }
    if (vbwin !is null && !vbwin.closed) {
      if (recalcHintWindow !is null) recalcHintWindow.close();
      if (mainPane !is null) mainPane.switchToFolderTid(mainPane.lastDecodedTid, forced:true);
      postScreenRebuild();
    }
  });

  vbwin.addEventListener((ArticleTextScrollEvent evt) {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    mainPane.doScrollStep();
  });

  vbwin.addEventListener((MarkAsUnreadEvent evt) {
    if (vbwin is null || vbwin.closed) return;
    if (mainPane is null) return;
    //conwriteln("unread timer fired");
    if (mainPane.lastDecodedTid == evt.tagid && evt.uid == mainPane.msglistCurrUId) {
      //conwriteln("*** unread timer hit!");
      chiroSetMessageRead(evt.tagid, evt.uid);
      setupTrayAnimation();
      postScreenRebuild();
    }
  });

  void firstTimeInit () {
    // create notification icon
    if (trayicon is null) {
      auto drv = vfsAddPak(wrapMemoryRO(iconsZipData[]), "", "databinz/icons.zip:");
      scope(exit) vfsRemovePak(drv);
      try {
        foreach (immutable idx; 0..6) {
          string fname = "databinz/icons.zip:icons";
          if (idx == 0) {
            fname ~= "/main.png";
          } else {
            import std.format : format;
            fname = "%s/bat%s.png".format(fname, idx-1);
          }
          auto fl = VFile(fname);
          if (fl.size == 0 || fl.size > 1024*1024) throw new Exception("fucked icon");
          auto pngraw = new ubyte[](cast(uint)fl.size);
          fl.rawReadExact(pngraw);
          auto img = readPng(pngraw);
          if (img is null) throw new Exception("fucked icon");
          icons[idx] = imageFromPng(img);
        }
        foreach (immutable idx, MemoryImage img; icons[]) {
          trayimages[idx] = Image.fromMemoryImage(img);
        }
        vbwin.icon = icons[0];
        trayicon = new NotificationAreaIcon("Chiroptera", trayimages[0], (MouseButton button) {
          scope(exit) if (!conQueueEmpty()) egraPostDoConCommands();
          if (button == MouseButton.left) vbwin.switchToWindow();
          if (button == MouseButton.middle) concmd("quit");
        });
        setupTrayAnimation();
        flushGui(); // or it may not redraw itself
      } catch (Exception e) {
        conwriteln("ERROR loading icons: ", e.msg);
      }
    }
  }

  vbwin.visibleForTheFirstTime = delegate () {
    egraFirstTimeInit();
    firstTimeInit();
  };

  mainPane = new MainPaneWindow();
  egraSkipScreenClear = true; // main pane is fullscreen

  postScreenRebuild();
  repostHideMouse();

  receiverInit();

  MonoTime lastCollect = MonoTime.currTime;
  vbwin.eventLoop(1000*10,
    delegate () {
      egraProcessConsole();
      if (mainPane !is null) {
        if (mainPane.isViewChanged) postScreenRebuild();
        mainPane.checkSaveState();
        setupTrayAnimation();
      }
      {
        immutable ctt = MonoTime.currTime;
        if ((ctt-lastCollect).total!"minutes" >= 1) {
          import core.memory : GC;
          lastCollect = ctt;
          GC.collect();
          GC.minimize();
        }
      }
    },
    delegate (KeyEvent event) {
      egraOnKey(event);
      if (mainPane !is null && mainPane.isViewChanged) postScreenRebuild();
    },
    delegate (MouseEvent event) {
      egraOnMouse(event);
      if (mainPane !is null && mainPane.isViewChanged) postScreenRebuild();
    },
    delegate (dchar ch) {
      egraOnChar(ch);
      if (mainPane !is null && mainPane.isViewChanged) postScreenRebuild();
    },
  );

  mainPane.saveCurrentState();

  trayimages[] = null;
  if (trayicon !is null && !trayicon.closed) { trayicon.close(); trayicon = null; }
  flushGui();
  receiverDeinit();
  conProcessQueue(int.max/4);
}
